'use strict'

module.exports = {
    BUCKET_USERS_IMAGES         : 'images-users',
    BUCKET_EMPLOYER_IMAGES         : 'images-users',
    BUCKET_USERS_CV         : 'cv-users',
    
    SIZE_USERS_IMAGES         : 5,
    URL_CLOUD_PUBLIC: 'https://storage.googleapis.com/',
    PAGINATION_DEFAULT: 50,
    CV_TEMPLATE: 'pdf/cv_template.html',
    SYSTEM_NAME: 'Total Empleos',

    CV_FILES: "./files/test",
}