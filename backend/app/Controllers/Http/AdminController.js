'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Ms = use('App/Constants')
const Database = use('Database')
const User = use('App/Models/User')
const UsersReason = use('App/Models/UsersReason')
const { validate } = use('Validator')
class AdminController {

    async index ({ response,request }) {
        let _users = await User.admins(request);
        return response.json(Payload.success(_users));
    }

    async show ({ params, response }) {
        let { page,search,order,actives,limit } = request.all();

        page = page && page>0 ? page : 1;
        search = search ? search : '';
        order = order ? order : 'asc';
        limit = limit ? limit : Ms.PAGINATION_DEFAULT;

        let _users = Database.table('_users')
        if(actives)
          _users.where('active',actives)

        _users =await _users.whereRaw(`title LIKE  ?`, [`%${search}%`])
            .orderBy('id', order)
            .paginate(page, limit)
        return response.json(Payload.success(_users))
    }


    async store ({ request, response, auth }) {
        const message  = Antl.formatMessage('messages.store');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          ])


        let _user = new User();
        _user.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        

        await _user.save();
         return response.json(Payload.success(_user,message))
    }

    async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          'active',
        ])
    
        const _user = await User.findOrFail(params.id)
    
        _user.merge(data);
        _user.updated_by = user.id;
        await _user.save()
        return response.json(_user)
      }
    
      async destroy ({ params,response }) {
        const message  = Antl.formatMessage('messages.delete');
        const _user = await User.findOrFail(params.id)
        await _user.delete()
        return response.json(Payload.success(_user,message))
      }

      async active ({ response,request,auth }) {
        const message  = Antl.formatMessage('messages.inactive_self_not');
        const params = request.only(['id','reason',]);
        const user = await auth.getUser();
        const trx = await Database.beginTransaction();
        try {
        const rules = {
          reason: 'required|min:8',
          user_id: 'required'
        }
      

        // Validar si el usuario es el mismo
        if(user.id==params.id)
          return response.forbidden(Payload.error(message))
        let _user = await User.findOrFail(params.id);
        const modelUsersReason = 
        {
          user_id: params.id,
          reason: params.reason,
          created_by: user.id,
          actual: !_user.active
        }
        const validation = await validate(modelUsersReason, rules)

        if (validation.fails()) 
          throw validation.messages()

         
          const usersReason = new UsersReason();
  
          usersReason.fill(modelUsersReason);
          
          _user.changeActive()
          await _user.save();
          await usersReason.save();
          trx.commit();
          return response.json(Payload.success(_user))
        } catch (error) {
          console.log(error);
          trx.rollback();
          response.forbidden(Payload.error(
              error,
              error
          ))
        }
      }
}

module.exports = AdminController
