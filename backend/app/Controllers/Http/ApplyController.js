'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database')
const JobsPostulation = use('App/Models/JobsPostulation')
const User = use('App/Models/User')
const Job = use('App/Models/Job')
const Service = use('App/Helpers/Service')
const Env = use('Env')
const Mail = use('Mail')
const Ms = use('App/Constants')
const BaseController = use('App/Controllers/Http/BaseController')
const Roles = use('App/Models/Rol/Roles')
/**
 * SOlo pueden ver los usuario candidatos y los administradores
 * @class ApplyController
 * @extends {BaseController}
 */
class ApplyController extends BaseController {
  /**
   * Muestra las postulaciones por usuario
   *
   * @param {*} {
   *         auth,
   *         request,
   *         response,
   *     }
   * @memberof ApplyController
   */
  async postulations({
    auth,
    request,
    response,
  }) {
    let {
      page,
      search,
      order,
      limit
    } = request.all();
    const session = await auth.getUser();
    const isAdmin = this.getRoleAdmin().includes(session.role);

    const fields = ['title'];
    page = page && page > 0 ? page : 1;
    search = search ? search : '';
    order = order ? order : 'asc';
    limit = limit ? limit : Ms.PAGINATION_DEFAULT;
    const clauseWhere = this.search(fields, search);
    const values = this.value(fields, search);

    let postulations = await JobsPostulation.query()
    .select('*','jobs_postulations.seen AS has_seen',
    'jobs_postulations.id as id_main','jobs_postulations.user_id  as seeker_id')
    .innerJoin('jobs', function () {
      this
        .on('jobs.id', 'jobs_postulations.job_id')
        
    })
    .innerJoin('user_employers', function () {
      this
        .on('user_employers.id', 'jobs.user_employer_id')
    })
    .with('job').setHidden(['seen'])
    .whereRaw(clauseWhere,[`${values}`])
    .where(function () {
      if(!isAdmin)
        this.where('jobs_postulations.user_id',session.id)
    })
    .orderBy('jobs_postulations.id', order)
    .paginate(page, limit);


    let data = [];
      for (let index = 0; index < postulations.rows.length; index++) {
        const postulation = postulations.rows[index];
        data.push(await postulation.search(isAdmin));
      }

    return response.json(Payload.success(this.setRows(postulations,data)))
  }

  async seen({
    response,
    request
  }) {
    const param = request.only(['id', ]);

    const trx = await Database.beginTransaction();
    try {
     const firstRecord = 0;//el primer registro de la consulta
     const jobsPostulation =  await Database.table('jobs_postulations')
      .where('id',param.id)//
      const objectJob = Object.assign({}, jobsPostulation)[firstRecord];

      const user = await User.find(objectJob.user_id);
      let job = await Job.find(objectJob.job_id);
      job = await job.search();
      const baseURL = Env.get('FRONTEND_URL', 'http://localhost:4200/');
     await Mail.send('emails.postulation', {
        email: user.email,
        fullname: user.fullname(),
        baseURL:baseURL,
        job: job
      }, (message) => {
        message
          .to(user.email)
          .from(Antl.formatMessage('messages.no_reply'))
          .subject(Antl.formatMessage('messages.email_postulation'))
      })
      await Database.table('jobs_postulations')
      .where('id',param.id).update({seen:1});
      trx.commit();
      return response.json(Payload.success())
    } catch (error) {
      console.log(error);
      trx.rollback();
      response.forbidden(Payload.error())
    }
  }


  getRoleAdmin() {
    const _roles = Roles.all();
    return [_roles.ADMIN, _roles.GOD,_roles.EMPLOYER];
  }
  /**
   * Se postula el candidato registrado
   * @param {*} {
   *         auth,
   *         request,
   *         response,
   *     }
   * @memberof ApplyController
   */
  async apply({
    auth,
    request,
    response,
  }) {
    const message = Antl.formatMessage('messages.store');
    const session = await auth.getUser();
    const params = request.all();
    const trx = await Database.beginTransaction();
    try {
      let jobsPostulation = new JobsPostulation();
      
      const findPostulation = await JobsPostulation.query()
      .where('user_id',session.id)
      .where('job_id',params.job_id)
      .getCount();
      if(findPostulation>0)
      {
        trx.rollback();
        return response.forbidden(Payload.error(Antl.formatMessage('messages.apply_again')))
      }

      jobsPostulation.fill({
        ...params,
        user_id: session.id
      });
      await jobsPostulation.save();
      trx.commit();
      return response.json(Payload.success([], message))
    } catch (error) {
      console.log(error);
      trx.rollback();
      return response.forbidden(Payload.error())
    }
  }

}

module.exports = ApplyController
