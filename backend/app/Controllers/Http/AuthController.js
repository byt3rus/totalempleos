'use strict'

const { first } = require("../../Models/Job")

const User = use('App/Models/User')
const TokenReset = use('App/Models/TokenReset')
const UserRegister = use('App/Models/UserRegister')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database');
const Mail = use('Mail')
const Env = use('Env')
const Service = use('App/Helpers/Service')
const Cloud = use('App/Helpers/Cloud')
const Hash = use('Hash')
const Ms = use('App/Constants')
const Roles = use('App/Models/Rol/Roles')
const BaseController = use('App/Controllers/Http/BaseController')
class AuthController extends BaseController {



  async login({
    request,
    response,
    auth
  }) {
    const {
      email,
      password
    } = request.all()
    try {
      if (await auth.attempt(email, password)) {
        let user = await User.query()
          .where('email', email)
          .where('active', true)
          .first();
        let accessToken = await auth.generate(user)
        const payload = await this.payload(user, accessToken);
        return response.json(Payload.success(payload))
      }
    } catch (error) {
      console.log(error)
      response.forbidden(
        Payload.error(Antl.formatMessage('messages.auth_error')),
        error
      )
    }
  }



  async settings({
    request,
    response,
    auth
  }) {
    const {
      id
    } = request.all();
    try {
      const user = await User.find(id)
      const isProtected = await this.protectedAdmin(user, auth);
      if (user && isProtected) {
        return response.json(Payload.success(user.editProfile()))
      }
      return response.forbidden(
        Payload.error()
      )
    } catch (error) {
      console.log(error)
      response.forbidden(
        Payload.error(),
        error
      )
    }
  }

  getRoleAdmin() {
    const _roles = Roles.all();
    return [_roles.ADMIN, _roles.GOD];
  }

  async protectedAdmin(user, auth) {
    const session = await auth.getUser();
    return session.id === user.id || this.getRoleAdmin().includes(session.role);
  }

  async profile({
    request,
    response,
    auth
  }) {
    const {
      id
    } = request.all();
    const user = await User.find(id);
    const isProtected = await this.protectedAdmin(user, auth);
    if (user && isProtected)
      return response.json(Payload.success(await user.profile()))
    return response.forbidden(
      Payload.error()
    )
  }

  async newAdmin({
    request,
    response
  }) {
    const _roles = Roles.all();
    const role = _roles.ADMIN;
    const trx = await Database.beginTransaction();

    try {
      const fileName = 'avatar_file';
      let changeAvatar = false;
      const profilePic = request.file(fileName, {
        types: ['image'],
        size: '2mb'
      });
      let params = request.except([fileName, 'currentSession', 'id']);

      const file = await this.uploadFile(fileName, request);
      if (file) {
        params.avatar = file.name;
        changeAvatar = true;
      } else
        delete params.avatar;

      if (params.password === null)
        delete params.password;
      else {
        params.password = await Hash.make(params.password)
      }

      params.role = role;
      const user = new User();
      user.fill(params);


      if (!await user.save())
        throw user.errors;
      trx.commit();
      return response.json(Payload.success(
        params,
        Antl.formatMessage('messages.profile_success')
      ));
    } catch (error) {
      trx.rollback();
      response.forbidden(Payload.error(
        error,
        error
      ))
    }
  }

  async setProfile({
    request,
    response,
    auth
  }) {
    const {
      currentSession,
      id
    } = request.all();
    const trx = await Database.beginTransaction();
    try {
      /**
       * Validar de dónde viene la edición
       * por el momento solo admite puros administradores o 
       * sesión
       */
      let session;
      console.log(request.all());
      const _roles = Roles.all();
      const exclude = [_roles.ADMIN];
      if (currentSession === "true")
        session = await auth.getUser();
      else {
        session = await User.query()
          .where('id', id)
          .whereIn('role', exclude)
          .first();
        if (!session)
          throw Antl.formatMessage('messages.user_invalid');
      }

      const fileName = 'avatar_file';
      let changeAvatar = false;
      const file = await this.uploadFile(fileName, request);
      let params = request.except([fileName, 'id', 'currentSession']);
      if (file) {
        
        params.avatar = file.name;
        changeAvatar = true;
      } else
        delete params.avatar;

      if (params.password === null)
        delete params.password;
      else {
        params.password = await Hash.make(params.password)
      }
      let user = await User.query()
        .where('id', session.id)
        .update(params);
      delete params.password;
      if (changeAvatar)
        params.avatar = await this.getAvatar(params.avatar);

      if (!user)
        throw user.errors;
      trx.commit();

      const success = currentSession ? Antl.formatMessage('messages.profile_success') : Antl.formatMessage('messages.profile_edit_success')

      return response.json(Payload.success(
        {
          ...params,
        avatar: User.buildAllavatar(session.id,changeAvatar ? params.avatar : session.avatar)
        },
        success
      ));
    } catch (error) {
      trx.rollback();
      response.forbidden(Payload.error(
        error,
        error
      ))
    }
  }

  /**
   *Mostrar avatar
   *
   * @param {*} {params,response}
   * @returns
   * @memberof ProfileController
   */
   async avatar({
    params,
    response
  }) {

    try {
      let user = await User.find(params.id);
      return await this.showFile(response, user);
    } catch (error) {
      await this.responseError(response, error);
    }
  }

  

  async resetPassword({
    request,
    response
  }) {
    const {
      email
    } = request.all();
    try {
      const baseURL = Env.get('FRONTEND_URL', 'http://localhost:4200/');
      const user = await User.findBy('email', email)
      if (user) {
        const token = await user.generateToken();
        if (token !== false) {
          await Mail.send('emails.reset', {
            email: email,
            token: token,
            baseURL: baseURL
          }, (message) => {
            message
              .to(email)
              .from(Antl.formatMessage('messages.no_reply'))
              .subject(Antl.formatMessage('messages.email_reset_title'))
          })
        }

      }
      return response.json(Payload.success(
        email,
        Antl.formatMessage('messages.reset_success')
      ))

    } catch (error) {
      console.log(error);
      response.forbidden(
        Payload.error(Antl.formatMessage('messages.reset_error')),
        error
      )
    }
  }

  async confirmToken({
    request,
    response,
    auth
  }) {
    const token_data = request.input('token');
    let errorMessage = Antl.formatMessage('messages.try_again');
    try {
      const userRegister = await UserRegister.query()
        .where('token_data', token_data)
        .where('confirmation', false).first();
      if (userRegister) {
        userRegister.confirmation = true;
        if (!userRegister.save())
          throw userRegister.errors;
        let user = await User.query()
          .where('id', userRegister.user_id).first();
        if (!user)
          throw Antl.formatMessage('messages.record_not_found');
        let accessToken = await auth.generate(user)
        const payload = await this.payload(user, accessToken);
        return response.json(Payload.success(payload))
      } else {
        errorMessage = Antl.formatMessage('messages.record_not_found');
        throw Antl.formatMessage('messages.record_not_found');
      }


    } catch (error) {
      response.forbidden(Payload.error(
        errorMessage,
        error
      ))
    }
  }

  async checkToken({
    request,
    response
  }) {
    const token_data = request.input('token')
    const tokenReset = await TokenReset.findBy('token_data', token_data)
    if (tokenReset)
      return response.json(Payload.success())
    return response.json(Payload.error(
      Antl.formatMessage('messages.common_error'),
      error
    ))
  }

  async changePassword({
    request,
    response
  }) {
    const trx = await Database.beginTransaction();
    try {
      const {
        password,
        token
      } = request.all()
      const tokenReset = await TokenReset.findBy('token_data', token)
      tokenReset.active = 0;

      const user = await User.find(tokenReset.user_id);
      user.password = password;
      if (!user.save())
        throw user.errors;
      if (!tokenReset.save())
        throw tokenReset.errors;
      trx.commit();
      return response.json(Payload.success(
        Antl.formatMessage('messages.password_success')
      ));
    } catch (error) {
      trx.rollback();
      response.forbidden(Payload.error(
        Antl.formatMessage('messages.password_error'),
        error
      ))
    }
  }

  async employeerValidation({request,
    response,
    auth})
    {
      const employeer = request.all();
      response.json(Payload.success(employeer, Antl.formatMessage('messages.register_complete')));
    }

  async employeer({
    request,
    response,
    auth
  }) {
    const trx = await Database.beginTransaction();
    const rol = Roles.all().EMPLOYER;
   
    try {
     const user = new User();
     let params = request.except(['rePassword']);
      user.fill({
        ...params
      });
      user.generateUsername();
      user.role = rol;
      await user.save(trx);
      trx.commit();
      response.json(Payload.success(user, Antl.formatMessage('messages.register_complete')));
    } catch (error) {
      trx.rollback();
      response.forbidden(Payload.error(
        Antl.formatMessage('messages.register_error'),
        error
      ))
    }
  }

  async testCloud({response})
  {
    response.ok({clientCloud:await Cloud.upload("xdddd")})
  }

  async freeRegister({
    request,
    response,
    auth
  }) {
    const trx = await Database.beginTransaction();
    const rol = Roles.all().SEEKER;
    const baseURL = Env.get('FRONTEND_URL', 'http://localhost:4200/');
    try {
      const user = new User();
      let params = request.except(['rePassword']);
      user.fill({
        ...params
      });
      user.generateUsername();
      user.role = rol;
      //Enviar correo de confirmación
      const token = await user.confirmToken();
      if (token !== false) {
        await Mail.send('emails.confirm', {
          email: user.email,
          token: token,
          baseURL: baseURL
        }, (message) => {
          message
            .to(user.email)
            .from(Antl.formatMessage('messages.no_reply'))
            .subject(Antl.formatMessage('messages.email_free_register'))
        })
      }


      await user.save(trx);
      trx.commit();
      response.json(Payload.success(user, Antl.formatMessage('messages.register_complete')));
    } catch (error) {
      trx.rollback();
      response.forbidden(Payload.error(
        Antl.formatMessage('messages.register_error'),
        error
      ))
    }
  }

  async skip({
    response,
    auth
  }) {
    
    let session =  await auth.getUser();
    const userRegister = await UserRegister.query()
    .where('user_id',session.id)
    .first();
    userRegister.skipChange()
    await userRegister.save();
    return response.json(Payload.success())
  }

  async payload(user, accessToken) {
    const avatar = await user.avatar;
    return {
      "user": user.editProfile(),
      "access_token": accessToken,
      "avatar": user.buildAvatar(user.id)
    };
  }

  async getAvatar(avatar) {
    if (!avatar) return false;
    const service = new Service();
    const result = await service.getFile(avatar, Ms.BUCKET_USERS_IMAGES);
    return result;
  }
}

module.exports = AuthController
