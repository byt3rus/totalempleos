'use strict'
const FileErrorException = use('App/Exceptions/FileErrorException')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database');
const Mail = use('Mail')
const Ms = use('App/Constants')
const Env = use('Env')

const pdf = use("pdf-creator-node");
const path = require('path');
const Service = require('../../Helpers/Service');
const fs = use('fs');
const Helpers = use('Helpers')
const readFile = Helpers.promisify(fs.readFile)
class BaseController {

  /**
   * 
   * @param {*} request 
   * @param {*} response 
   * @param {string} table  Tabla a consultar
   * @param {string} fields Campos a buscar
   */
  async index(request, response, table, fields) {
    let {
      page,
      search,
      order,
      actives,
      limit
    } = request.all();

    page = page && page > 0 ? page : 1;
    search = search ? search : '';
    order = order ? order : 'asc';
    limit = limit ? limit : Ms.PAGINATION_DEFAULT;

    let model = Database.table(table)
    if (actives)
      model.where('active', actives)

    model = await model.whereRaw(this.search(fields, search), this.value(fields, search))
      .orderBy('id', order)
      .paginate(page, limit)
    return response.json(Payload.success(model))
  }

  search(fields, search) {
    let where = "";
    for (let i in fields) {
      where += `${fields[i]} LIKE ? `
      if (fields.length - 1 > i)
        where += ` OR `;
    }
    return where;
  }
  value(fields, search) {
    let values = [];
    for (let i in fields) {
      values.push(`%${search}%`)
    }
    return values;
  }

  /**
   *Modifica el data de cada uno de los registros
   *
   * @param {*} collection
   * @param {*} data
   * @returns
   * @memberof BaseController
   */
  setRows(collection, data) {
    return {
      total: collection.pages.total,
      perPage: collection.pages.perPage,
      page: collection.pages.page,
      lastPage: collection.pages.lastPage,
      data: data
    };
  }
  async showFile(response, user) 
  {
    if(!user)
      response.download(`${process.cwd()}/images/anon.webp`)
    return await response.download(Helpers.tmpPath(user.avatar))
  }

  async showCompany(response, user) 
  {
    if(!user)
      response.download(`${process.cwd()}/images/anon.webp`)
    return await response.download(Helpers.tmpPath(user.company_logo))
  }

  async showCv(response, seeker) 
  {
    if(!seeker)
      response.download(`${process.cwd()}/images/anon.webp`)
    return await response.download(Helpers.tmpPath(seeker.cv))
  }

  exceptionsToShow(name) {
    return ['FileErrorException'].includes(name);
  }

async responseError(response, error) {
  let trackError = error;

  if (this.exceptionsToShow(trackError.name))
    return response.badRequest(Payload.error(error.message))


  try {
    trackError = {
      fileName: error.fileName,
      message: error.message,
      lineNumber: error.lineNumber,
      _type: error.name,
      stack: error.stack,
    };
    trackError = JSON.stringify(trackError);
  } catch (er) {
    console.log(er);
  }

  const code = Payload.uuidv4();

  this.sendError();
  console.group();
  this.sendError(`code: ${code}`);
  console.groupEnd();
  return response.internalServerError(Payload.error(code, error));
}
sendError(message = 'Error 5000') {
  console.log('%c ' + message, 'background: #222; color: #bada55');
}



  async uploadFile(fileName, request, config = {
    path: 'users',
    types: ['image'],
    size: `5`,
  }) {

    const size = `${config.size}mb`;
    const file = request.file(fileName, {
      types: config.types,
      size: size
    });
    const path = `uploads/${config.path}`;
    if (!file)
      return false;

    await file.move(Helpers.tmpPath(path), {
      name: `${Payload.uuidv4()}.${file.extname}`,
      overwrite: true
    })

    if (!file.moved()) {
      const response = Service.mapErrorFile(file.error(), size);
      throw new FileErrorException(response)
    }
    return {
      meta: {extname:file.subtype,size:file.size,original:file.clientName},
      valid: true,
      name: `${path}/${file.fileName}`
    };
  }


  async makePdfCv(response, user, info) {
    const _path = path.join(__dirname, Ms.CV_TEMPLATE);
    const html = fs.readFileSync(_path, 'utf8');

    const salary = Service.currency(info.academic.expected_salary);
    const options = {
      format: "A3",
      orientation: "portrait",
      border: "5mm",
      header: {
        height: "20mm",
        contents: `<div style="text-align: center;">${user.fullname}<br>
                ${user.email}<br></div>
                <sup>Expectativa Salarial ${salary} Mensual</sup>
                `
      },
      "footer": {
        "height": "10mm",
        "contents": {
          first: Ms.SYSTEM_NAME,
          2: Ms.SYSTEM_NAME,
          default: `<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>`, // fallback value
          last: Ms.SYSTEM_NAME,
        }
      }
    };


    const scholarship = await info.academic._scholarship()

    const stateCity = await info.location._city();
    const location = `${stateCity.state}, ${stateCity.city} ${info.location.zip_code}`
    const document = {
      html: html,
      data: {
        title_basic: info.basic.title,
        sex_basic: info.basic.sex,
        nationality_basic: info.basic.nationality,
        birthday_basic: Service.getAge(info.basic.birthday),
        //Location
        location: location,
        contact: `${info.location.phone} | ${info.location.mobile}`,
        academic: `${scholarship.scholarship} ${info.academic.school_name}`,
      },
      path: "./files/out.pdf"
    };
    response.implicitEnd = false
    pdf.create(document, options)
      .then(res => {
        response.header('Content-type', 'application/pdf')
        response.implicitEnd = false

        fs.readFile(res.filename, (error, contents) => {
          response.send(contents)
        })
      })
      .catch(error => {
        console.error(error)
        return response.forbidden(
          Payload.error(),
          error
        )
      });
  }
}

module.exports = BaseController
