'use strict'
const Category = use('App/Models/Category')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Ms = use('App/Constants')
const Database = use('Database')
class CategoryController {


    async index ({ response,request }) {
        let { page,search,order,actives,limit } = request.all();

        page = page && page>0 ? page : 1;
        search = search ? search : '';
        order = order ? order : 'asc';
        limit = limit ? limit : Ms.PAGINATION_DEFAULT;

        let categories = Database.table('categories')
        if(actives)
          categories.where('active',actives)

        categories =await categories
        .whereRaw(`title LIKE  ?`, [`%${search}%`])
            .orderBy('id', order)
            .paginate(page, limit)
        return response.json(Payload.success(categories))
    }

    async show ({ params, response }) {
        const category = await Category.query()
        .with('updated')
        .with('created')
        .where('id',params.id)
        .first()
        if(!category)
          return response.forbidden(Payload.error())
        return response.json(category)
    }


    async store ({ request, response, auth }) {
        const message  = Antl.formatMessage('messages.store');
        const user = await auth.getUser();
        const data = request.only([
            'title',
          ])


        let category = new Category();
        category.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        

        await category.save();
         return response.json(Payload.success(category,message))
    }

    async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'active',
        ])
    
        const category = await Category.findOrFail(params.id)
    
        category.merge(data);
        category.updated_by = user.id;
        await category.save()
        return response.json(category)
      }
    
      async destroy ({ params,response }) {
        const message  = Antl.formatMessage('messages.delete');
        const category = await Category.findOrFail(params.id)
        await category.delete()
        return response.json(Payload.success(category,message))
      }

      async active ({ response,request }) {
        const param = request.only(['id',])
        const category = await Category.findOrFail(param.id);
        category.changeActive()
        await category.save();
        return response.json(Payload.success(category))
      }

}

module.exports = CategoryController
