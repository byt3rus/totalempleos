'use strict'

const User = use('App/Models/User')
const TokenReset = use('App/Models/TokenReset')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database');
const Mail = use('Mail')
const Env = use('Env')
const Service = use('App/Helpers/Service')
const Hash = use('Hash')
const Ms = use('App/Constants')

class DashboardController {

    async index({request,response, auth})
    {
        const user = await auth.user;
        if(user)
            return response.json(Payload.success(await user.dashboard()))
        return response.forbidden(
            Payload.error()
        )
    }

}

module.exports = DashboardController