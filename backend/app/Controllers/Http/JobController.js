'use strict'

const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database')
const User = use('App/Models/User')
const Job = use('App/Models/Job')
const JobsCategory = use('App/Models/JobsCategory')
const Ms = use('App/Constants')
const BaseController = use('App/Controllers/Http/BaseController')

class JobController extends BaseController {
  async index({
    request,
    response
  }) {
    
    let {
      page,
      search,
      order,
      actives,
      limit
    } = request.all();

    const fields = ['title'];
    page = page && page > 0 ? page : 1;
    search = search ? search : '';
    order = order ? order : 'asc';
    limit = limit ? limit : Ms.PAGINATION_DEFAULT;
    const clauseWhere = this.search(fields, search);
    const values = this.value(fields, search);

    
    const jobs = await Job.query()
      .with('city')
      .with('state')
      .with('job_type')
      .with('job_work_space')
      .with('user_employer')
      .where(function () {
        if(actives)
          this.where('active',actives)
      })
      .where(function () {
        this.whereRaw(clauseWhere,values)
      })
      .orderBy('id', order)
      .paginate(page, limit)

    return response.json(Payload.success(jobs))
  }
    
      async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
          const trx = await Database.beginTransaction();
          try {
          const user = await auth.getUser();
          const data = request.except(['category_id']);
          const param = request.only(['category_id']);
          const job = await Job.findOrFail(params.id);

          job.merge({...data,
            updated_by: user.id
          });
          
          await job.save(trx);
          // convierte el string de ids en array
          const categories = param.category_id.split(',');

          // Itera para verificar nuevas categorias
          const jobsCategories = await job.jobsCategories().fetch();
          const ids = jobsCategories.rows.map((j)=> j.category_id);
          // Obtiene los ids que no vienen y están registrados para eliminarlos
          let deleteIds = ids.filter(x => !categories.includes(x.toString()));
        
          

          for (const category in categories) {
            
            const _category  = parseInt(categories[category]);
            //agrega las nuevas
            if(!ids.includes(_category))
            {
             
              const jobsCategory = new JobsCategory();
              jobsCategory.job_id = job.id;
              jobsCategory.category_id = categories[category];
              jobsCategory.save();
            }else
            {
              
            }
          }
          //Elimina los que ya no están
          if(deleteIds.length>0)
          {
              const toDelete = await JobsCategory.query()
              .byCategoryIds(deleteIds)
              .fetch();
              for (let index = 0; index < toDelete.rows.length; index++) {
                const j = toDelete.rows[index];
                await j.delete();
              }
          }
          
            trx.commit();
            return response.json(Payload.success([], message))
          } catch (error) {
            console.log(error);
            trx.rollback();
            response.forbidden(Payload.error())
          }
      }
    
      async store({
        request,
        response,
        auth
      }) {
        const message = Antl.formatMessage('messages.store');
        const trx = await Database.beginTransaction();
        try {
          const user = await auth.getUser();
          const data = request.except(['category_id']);
          const param = request.only(['category_id']);
        let job = new Job();
        job.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        await job.save(trx);

        const categories = param.category_id.split(',');
        for (const category in categories) {
          const jobsCategory = new JobsCategory();
          jobsCategory.job_id = job.id;
          jobsCategory.category_id = categories[category];
          jobsCategory.save();
        }

          trx.commit();
          return response.json(Payload.success([], message))
        } catch (error) {
          console.log(error);
          trx.rollback();
          response.forbidden(Payload.error())
        }
    
      }
    
      async show ({ params, response }) {
        const job = await Job.query()
        .where('id',params.id)
        .first()
        if(!job)
          return response.forbidden(Payload.error())
        return response.json(await job.show())
    }

    /**
     *Muestra el  total de vacantes creadas por el usuario actual
     *
     * @param {*} {
     *       response,
     *       auth
     *     }
     * @returns
     * @memberof JobController
     */
    async myJobs({
      response,
      auth
    })
    {
      try {
        const session = await auth.getUser();
        const jobs = await session.jobs().getCount();
        return response.json({total:jobs});
      } catch (error) {
        console.log(error)
        return  response.forbidden(Payload.error(error.message));
      }
    }

  async edit({request,response})
  {
    const param = request.only(['id', ]);
    const job = await Database.table('jobs')
        .where('id',param.id)
        .first();
        return response.json(Payload.success(job))
  }


  async categories({
    request,
    response,
    auth
  }) {
    const params = request.all();
    const trx = await Database.beginTransaction();
    try {
      const categories = params.category_id.split(',');
      const session = await auth.getUser();
      let seekerCategories = await session.seekerCategories().fetch();
      let isEdit = false;
      if (seekerCategories.rows.length > 0) {
        const ids = seekerCategories.rows.map((c) => c.category_id);
        isEdit = true;
        for (const index in categories) {
          const category = categories[index];
          console.log(typeof category)
          if (!ids.includes(parseInt(category))) {
            const seekerCategory = new SeekerCategory();
            seekerCategory.user_id = session.id;
            seekerCategory.category_id = category;
            seekerCategory.save();
          }
        }
      } else {

        for (const category in categories) {
          const seekerCategory = new SeekerCategory();
          seekerCategory.user_id = session.id;
          seekerCategory.category_id = categories[category];
          seekerCategory.save();
        }
      }
      trx.commit();

      // Se renueva la sessión para obtener los nuevos datos una ves commit
      let categoryResources = await this.getNewData(auth);
      return response.json(Payload.success(categoryResources))
    } catch (error) {
      trx.rollback();
      console.log(error)
      response.forbidden(
        Payload.error(),
        error
      )
    }
  }
  
    
  
  async active({
    response,
    request
  }) {
    const message = Antl.formatMessage('messages.active');
    const trx = await Database.beginTransaction();
    try {
      const param = request.only(['id',])
      const job = await Job.findOrFail(param.id);
      job.changeActive()
      await job.save();
      trx.commit();
      return response.json(Payload.success(message))
    } catch (error) {
      console.log(error);
      trx.rollback();
      response.forbidden(Payload.error())
    }
  }

}

module.exports = JobController
