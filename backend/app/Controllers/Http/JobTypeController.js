'use strict'

const JobType = use('App/Models/JobType')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Ms = use('App/Constants')
const Database = use('Database')

const BaseController = use('App/Controllers/Http/BaseController')

class JobTypeController extends BaseController {

    async index ({ response,request }) {
        return super.index(request,response,'job_types',['title']);
    }

    async show ({ params, response }) {
        const jobType = await JobType.query()
        .with('updated')
        .with('created')
        .where('id',params.id)
        .first()
        if(!jobType)
          return response.forbidden(Payload.error())
        return response.json(jobType)
    }


    async store ({ request, response, auth }) {
        const message  = Antl.formatMessage('messages.store');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          ])


        let jobType = new JobType();
        jobType.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        

        await jobType.save();
         return response.json(Payload.success(jobType,message))
    }

    async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          'active',
        ])
    
        const jobType = await JobType.findOrFail(params.id)
    
        jobType.merge(data);
        jobType.updated_by = user.id;
        await jobType.save()
        return response.json(jobType)
      }
    
      async destroy ({ params,response }) {
        const message  = Antl.formatMessage('messages.delete');
        const jobType = await JobType.findOrFail(params.id)
        await jobType.delete()
        return response.json(Payload.success(jobType,message))
      }

      async active ({ response,request }) {
        const param = request.only(['id',])
        const jobType = await JobType.findOrFail(param.id);
        jobType.changeActive()
        await jobType.save();
        return response.json(Payload.success(jobType))
      }
}

module.exports = JobTypeController
