'use strict'
const LanguageProficiency = use('App/Models/LanguageProficiency')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Ms = use('App/Constants')
const Database = use('Database')
const BaseController = use('App/Controllers/Http/BaseController')
class LanguageProficiencyController  extends BaseController {

    async index ({ response,request }) {
        return super.index(request,response,'language_proficiencies',['title']);
    }

    async show ({ params, response }) {
        const languageProficiency = await LanguageProficiency.query()
        .with('updated')
        .with('created')
        .where('id',params.id)
        .first()
        if(!languageProficiency)
          return response.forbidden(Payload.error())
        return response.json(languageProficiency)
    }


    async store ({ request, response, auth }) {
        const message  = Antl.formatMessage('messages.store');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          ])


        let languageProficiency = new LanguageProficiency();
        languageProficiency.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        

        await languageProficiency.save();
         return response.json(Payload.success(languageProficiency,message))
    }

    async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          'active',
        ])
    
        const languageProficiency = await LanguageProficiency.findOrFail(params.id)
    
        languageProficiency.merge(data);
        languageProficiency.updated_by = user.id;
        await languageProficiency.save()
        return response.json(languageProficiency)
      }
    
      async destroy ({ params,response }) {
        const message  = Antl.formatMessage('messages.delete');
        const languageProficiency = await LanguageProficiency.findOrFail(params.id)
        await languageProficiency.delete()
        return response.json(Payload.success(languageProficiency,message))
      }

      async active ({ response,request }) {
        const param = request.only(['id',])
        const languageProficiency = await LanguageProficiency.findOrFail(param.id);
        languageProficiency.changeActive()
        await languageProficiency.save();
        return response.json(Payload.success(languageProficiency))
      }
}

module.exports = LanguageProficiencyController
