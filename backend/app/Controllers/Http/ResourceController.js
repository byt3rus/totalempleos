'use strict'

const Payload = use('App/Models/Response/Payload')
const State = use('App/Models/State')
const Scholarship = use('App/Models/Scholarship')
const City = use('App/Models/City')
const JobsCategory = use('App/Models/JobsCategory')


const UserEmployer = use('App/Models/UserEmployer')
const SeekerAcademic = use('App/Models/SeekerAcademic')
const JobType = use('App/Models/JobType')
const WorkSpace = use('App/Models/WorkSpace')

const Category = use('App/Models/Category')
const ResumeLanguage = use('App/Models/ResumeLanguage')
const LanguageProficiency = use('App/Models/LanguageProficiency')

const Ms = use('App/Constants')
const Sex = use('App/Models/Enums/Sex')
const Nationality = use('App/Models/Enums/Nationality')
const Antl = use('Antl');

const pdf = use("pdf-creator-node");
const path = require('path')
const fs = use('fs');
const Helpers = use('Helpers')
const readFile = Helpers.promisify(fs.readFile)
const request = require('request');
class ResourceController {

    async listCities({request,response})
    {
        let { id } = request.all();
        try {
            const cities = await City.query()
            .where("state_id",id)
            .fetch();
            
            response.json(cities);
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }


    async categoriesList({auth,response})
    {
        const session = await auth.getUser();
        try {
            let seekerCategories = await session.seekerCategories().fetch();
            const ids = seekerCategories.rows.map((c) => c.category_id);
            const categories = await Category.query()
            .userActives(ids)
            .fetch();
            
            response.json(Payload.success(categories))
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage()),
                error
            )
        }
    }

    async resumeLanguagesList({auth,response})
    {
        const session = await auth.getUser();
        try {
            let seekerLanguages = await session.seekerLanguages().fetch();
            const resumesIds = seekerLanguages.rows.map((c) => c.resume_language_id);
            const resumeLanguages = await ResumeLanguage.query()
            .actives(resumesIds)
            .fetch();

            const languagesIds = seekerLanguages.rows.map((c) => c.language_proficiency_id);
            const languageProficiencies = await LanguageProficiency.query()
            .actives(languagesIds)
            .fetch();


            response.json(Payload.success({
                resumeLanguages:resumeLanguages,
                languageProficiencies:languageProficiencies,
            }))
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage()),
                error
            )
        }
    }

    async cityState({request,response})
    {
        let { id } = request.all();
        try {
            const cities = await City.query()
            .where("state_id",id)
            .fetch();

            const states = await State.query()
            .fetch();
            
            response.json({
                'cities': cities,
                'states': states,
                
            });
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage()),
                error
            )
        }
    }


    async employers({request,response})
    {
        let { q } = request.all();
        try {
            q = q ? q  : '';
            const userEmployer = await UserEmployer.query()
            //.whereRaw(`company_name LIKE ?`,[`${q}`])
            .active()
            .fetch()
            response.json(userEmployer.rows.map((e)=> {
                return {id:e.id,title:e.company_name};
            }));
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }

    

    async jobType({request,response})
    {
        let { q } = request.all();
        try {
            q = q ? q  : '';
            const jobType = await JobType.query()
            .whereRaw(`title LIKE '%${q}%'`)
            .actives()
            .fetch()
            response.json(jobType.rows.map((e)=> {
                return {id:e.id,title:e.title};
            }));
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }

    async workSpace({request,response})
    {
        let { q } = request.all();
        try {
            q = q ? q  : '';
            const workSpace = await WorkSpace.query()
            .whereRaw(`title LIKE '%${q}%'`)
            .actives()
            .fetch()
            response.json(workSpace.rows.map((e)=> {
                return {id:e.id,title:e.title};
            }));
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }


    async cities({request,response})
    {
        let { q } = request.all();
        try {
            q = q ? q  : '';
            const cities = await City.query()
            .whereRaw(`name LIKE '%${q}%'`)
            .with('state')
            .fetch()

            const states = await State.query()
            .whereRaw(`name LIKE '%${q}%'`)
            .fetch()
            
            response.json(City.search({cities:cities,states:states}));
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }

    nationalities({response,request})
    {
        let { q } = request.all();
        
        const nationalities = Nationality.search(q);
        response.json(nationalities);
    }

    sex({response,request})
    {
        let { q } = request.all();
        const sex = Sex.getData();
        response.json(sex);
    }

    async scholarships({response,request})
    {
        try {
            const scholarships = await Scholarship.query()
            .fetch()
            
            response.json(scholarships);
        } catch (error) {
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }
    /**
     *
     *
     * @param {*} {response,request}
     * @memberof ResourceController
     */
    async jobs({response,request})
    {
        try {
            const params = request.only(['id']);
            const states = await State.query()
            .fetch()
            const userEmployer = await UserEmployer.query()
            .active()
            .fetch()
            const jobType = await JobType.query()
            .actives()
            .fetch()
            const workSpace = await WorkSpace.query()
            .actives()
            .fetch()

            let categories = [];
            
            //Obtiene las categorias del usuario
            if(params.id)
            {
             const jobsCategories = await JobsCategory.query()
                .where('job_id',params.id)
                .fetch()
             const ids = jobsCategories.rows.map((c) => c.category_id);
             categories = await Category.query()
             .onlyActives(ids)
             .fetch();
            }
           
            response.json(Payload.collections({
                states:states,
                userEmployers:userEmployer.rows.map((e)=> {
                    return {id:e.id,title:e.company_name};
                }),
                jobTypes:jobType.rows.map((e)=> {
                    return {id:e.id,title:e.title};
                }),
                workSpaces:workSpace.rows.map((e)=> {
                    return {id:e.id,title:e.title};
                }),
                categories: categories,
            }));
        } catch (error) {
            console.log(error)
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }

    async states({response,request})
    {
        let { q } = request.all();
        try {
            q = q ? q  : '';
            const states = await State.query()
            .whereRaw(`name LIKE '${q}%'`)
            .fetch()
            
            response.json(states);
        } catch (error) {
            response.forbidden(
                Payload.error(Antl.formatMessage('messages.auth_error')),
                error
            )
        }
    }

}

module.exports = ResourceController