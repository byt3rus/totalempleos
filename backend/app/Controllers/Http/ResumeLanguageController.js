'use strict'
const ResumeLanguage = use('App/Models/ResumeLanguage')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Ms = use('App/Constants')
const Database = use('Database')
const BaseController = use('App/Controllers/Http/BaseController')
class ResumeLanguageController  extends BaseController {

    async index ({ response,request }) {
        return super.index(request,response,'resume_languages',['title']);
    }

    async show ({ params, response }) {
        const resumeLanguage = await ResumeLanguage.query()
        .with('updated')
        .with('created')
        .where('id',params.id)
        .first()
        if(!resumeLanguage)
          return response.forbidden(Payload.error())
        return response.json(resumeLanguage)
    }


    async store ({ request, response, auth }) {
        const message  = Antl.formatMessage('messages.store');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          ])


        let resumeLanguage = new ResumeLanguage();
        resumeLanguage.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        

        await resumeLanguage.save();
         return response.json(Payload.success(resumeLanguage,message))
    }

    async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          'active',
        ])
    
        const resumeLanguage = await ResumeLanguage.findOrFail(params.id)
    
        resumeLanguage.merge(data);
        resumeLanguage.updated_by = user.id;
        await resumeLanguage.save()
        return response.json(resumeLanguage)
      }
    
      async destroy ({ params,response }) {
        const message  = Antl.formatMessage('messages.delete');
        const resumeLanguage = await ResumeLanguage.findOrFail(params.id)
        await resumeLanguage.delete()
        return response.json(Payload.success(resumeLanguage,message))
      }

      async active ({ response,request }) {
        const param = request.only(['id',])
        const resumeLanguage = await ResumeLanguage.findOrFail(param.id);
        resumeLanguage.changeActive()
        await resumeLanguage.save();
        return response.json(Payload.success(resumeLanguage))
      }
}

module.exports = ResumeLanguageController
