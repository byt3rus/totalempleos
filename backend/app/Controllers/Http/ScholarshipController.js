'use strict'
const Scholarship = use('App/Models/Scholarship')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Ms = use('App/Constants')
const Database = use('Database')
const BaseController = use('App/Controllers/Http/BaseController')
class ScholarshipController  extends BaseController {

    async index ({ response,request }) {
        return super.index(request,response,'scholarships',['title']);
    }

    async show ({ params, response }) {
        const scholarship = await Scholarship.query()
        .with('updated')
        .with('created')
        .where('id',params.id)
        .first()
        if(!scholarship)
          return response.forbidden(Payload.error())
        return response.json(scholarship)
    }


    async store ({ request, response, auth }) {
        const message  = Antl.formatMessage('messages.store');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          ])


        let scholarship = new Scholarship();
        scholarship.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        

        await scholarship.save();
         return response.json(Payload.success(scholarship,message))
    }

    async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          'active',
        ])
    
        const scholarship = await Scholarship.findOrFail(params.id)
    
        scholarship.merge(data);
        scholarship.updated_by = user.id;
        await scholarship.save()
        return response.json(scholarship)
      }
    
      async destroy ({ params,response }) {
        const message  = Antl.formatMessage('messages.delete');
        const scholarship = await Scholarship.findOrFail(params.id)
        await scholarship.delete()
        return response.json(Payload.success(scholarship,message))
      }

      async active ({ response,request }) {
        const param = request.only(['id',])
        const scholarship = await Scholarship.findOrFail(param.id);
        scholarship.changeActive()
        await scholarship.save();
        return response.json(Payload.success(scholarship))
      }
}

module.exports = ScholarshipController
