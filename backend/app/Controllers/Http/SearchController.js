'use strict'

const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database')
const Job = use('App/Models/Job')
const Ms = use('App/Constants')
const BaseController = use('App/Controllers/Http/BaseController')
const Roles = use('App/Models/Rol/Roles')
class SearchController extends BaseController {

  async index({
    request,
    response,
  }) {
    return await this.searchJobs(request, response, true);
  }

  async admin({
    request,
    response,
  }) {
    return await this.searchJobs(request, response);
  }

  async searchJobs(request, response, isGuest = false) {
    try {

      let {
        page,
        q,
        order,
        city,
        state,
        limit
      } = request.all();

      page = page && page > 0 ? page : 1;
      q = q ? q : '';
      city = city ? city : '';
      state = state ? state : '';
      order = order ? order : 'asc';
      limit = limit ? limit : Ms.PAGINATION_DEFAULT;

      const categories = await Database.table('jobs_categories as jc')
        .select('jc.id', 'c.title', 'jc.job_id')
        .where('jc.deleted_at', null)
        .leftJoin('categories as c', 'c.id', 'jc.category_id');

      const findMatch = Job.findMatch(q, categories);
      const ids = !findMatch ? [] : findMatch.map((c) => c.job_id);

      const _fields = ['j.title', 'j.description'];
      const clause = this.search(_fields, q)
      const value = this.value(_fields, q)
      const jobs = await Database.table('jobs as j')
        .select(
          'j.id', 'j.title', 'j.description', 'ue.company_logo as logo',
          'j.active', 'ue.company_name as employer', 'ue.id as employer_id',
          'ws.title as work_space', 'jt.title as job_type', 'c.name as city',
          's.name as state', 'j.confidencial', 'j.show_salary', 'j.salary',
          'j.start_date as ago'
        )
        .leftJoin('job_types as jt', 'jt.id', 'j.job_type_id')
        .leftJoin('work_spaces as ws', 'ws.id', 'j.work_space_id')
        .leftJoin('user_employers as ue', 'ue.id', 'j.user_employer_id')
        .leftJoin('cities as c', 'c.id', 'j.city_id')
        .leftJoin('states as s', 's.id', 'j.state_id')
        
        .where(function () {
          if (city)
            this.where('j.city_id', city)
          if (state)
            this.where('j.state_id', state);
        })
        .where(function () {
          const todayDate = new Date().toISOString().slice(0, 10);
          if (isGuest) {
            this.where('j.start_date', '<=', todayDate)
            this.where('j.end_time', '>=', todayDate)
            this.where('j.active', 1)
          }
        })
        .where(function () {
          if(q)
           this.whereRaw(clause, value)
        })
        .orWhere(function () {
          if(findMatch)
            this.whereIn('j.id', ids);
        })
        .paginate(page, limit);

      return response.json({
        ...jobs,
        data: Job.mapping(jobs.data, categories)
      })
    } catch (error) {
      console.log(error);
      response.forbidden(Payload.error())
    }
  }

  /**
   *Obtiene el recurso de la vacante para la vista públic y privada
   *
   * @param {*} {
   *     params,
   *     request,
   *     response
   *   }
   * @returns
   * @memberof SearchController
   */
  async job({
    params,
    request,
    auth,
    response
  }) {
    try {
      const job = await Job.query()
        .where('id', params.id)
        .first()
      if (!job)
        return response.forbidden(Payload.error(Antl.formatMessage('messages.404_error')))

      const user = await auth.getUser();
      const role = Roles.all().SEEKER;
      const allowed = user.role === role;

      // Permite incrementar las visitas a la vacante
      if (allowed) {
        job.seen = job.seen + 1;
        await job.save();
      }


      return response.json(await job.search())
    } catch (error) {
      console.log(error);
      response.forbidden(Payload.error())
    }
  }
}

module.exports = SearchController
