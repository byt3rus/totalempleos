'use strict'

const FileErrorException = use('App/Exceptions/FileErrorException')
const User = use('App/Models/User')
const UserRegister = use('App/Models/UserRegister')
const SeekerBasic = use('App/Models/SeekerBasic')
const SeekerLocation = use('App/Models/SeekerLocation')
const SeekerAcademic = use('App/Models/SeekerAcademic')
const SeekerCategory = use('App/Models/SeekerCategory')
const SeekerSkill = use('App/Models/SeekerSkill')
const SeekerLanguage = use('App/Models/SeekerLanguage')
const SeekerProfessionalExperience = use('App/Models/SeekerProfessionalExperience')

const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database');
const Env = use('Env')
const Service = use('App/Helpers/Service')
const Ms = use('App/Constants')
const Roles = use('App/Models/Rol/Roles')
const BaseController = use('App/Controllers/Http/BaseController')

class SeekerController extends BaseController {


  /**
   * Genera un pdf para el cv del candidato
   *
   * @param {*} {response,params}
   * @returns
   * @memberof SeekerController
   */
  async cvPdf({response,params})
    {
        const id = params.id;
       try {
        let user = await User.find(id);
        user = await user.profile();
        return await this.makePdfCv(response,user,user.timeline)
       } catch (error) {
         console.log(error)
        return response.json(Payload.error(Antl.formatMessage('messages.profile_incomplete')))
       }
    }

  /**
   * Solo los usuarios candidatos
   *
   * @param {*} {
   *     request,
   *     response
   *   }
   * @returns
   * @memberof SeekerController
   */
  async list({
    request,
    response
  }) {
    const _roles = Roles.all();
    const exclude = _roles.SEEKER;
    let {
      page,
      search,
      order,
      actives,
      limit
    } = request.all();

    const fields = ['username', 'email', 'first_name', 'last_name', 'middle_name'];
    page = page && page > 0 ? page : 1;
    search = search ? search : '';
    order = order ? order : 'asc';
    limit = limit ? limit : Ms.PAGINATION_DEFAULT;

    const clauseWhere = this.search(fields, search);
    const values = this.value(fields, search);
    const users = await User.query().where(function () {
        this.whereRaw(clauseWhere,values)
      }).andWhere({
        role: exclude
      })
      .orderBy('id', order)
      .paginate(page, limit)

    return response.json(Payload.success(users))
  }


  async documentCv({
    params,
    response
  }) {

    try {
      const {id,school_name} = SeekerAcademic.decode(params.hash);
      const seekerAcademic = await SeekerAcademic.find(id);
      if(!seekerAcademic)
        throw new FileErrorException(Antl.formatMessage('messages.not_found'))
      const schoolName =  SeekerAcademic.reverse(seekerAcademic.school_name);
      if(schoolName!==school_name)
        throw new FileErrorException(Antl.formatMessage('messages.not_found'))
      return await this.showCv(response, seekerAcademic);
    } catch (error) {
      console.log(error);
      await this.responseError(response, error);
    }
  }

 
  async seeker_active ({ response,request }) {
    const param = request.only(['id',])
    const user = await User.findOrFail(param.id);
    user.changeActive()
    await user.save();
    return response.json(Payload.success(user))
  }

  async basic({
    request,
    response,
    auth
  }) {
    const params = request.all();
    try {
      const session = await auth.getUser();
      let seekerBasic = await session.seekerBasic().fetch();
      if (!seekerBasic)
        seekerBasic = new SeekerBasic();
      seekerBasic.merge(params);
      seekerBasic.user_id = session.id;

      await seekerBasic.save();
      return response.json(Payload.success(params))
    } catch (error) {
      console.log(error)
      response.forbidden(
        Payload.error(),
        error
      )
    }
  }

  async location({
    request,
    response,
    auth
  }) {
    const params = request.all();
    try {
      const session = await auth.getUser();
      let seekerLocation = await session.seekerLocation().fetch();
      if (!seekerLocation)
        seekerLocation = new SeekerLocation();
      seekerLocation.merge(params);
      seekerLocation.user_id = session.id;
      await seekerLocation.save();

      let locationResources = {};
      if (seekerLocation)
        locationResources = await seekerLocation._city() || {};


      return response.json(Payload.success(Object.assign(seekerLocation, locationResources)))
    } catch (error) {
      console.log(error)
      response.forbidden(
        Payload.error(),
        error
      )
    }
  }

  async skills({
    request,
    response,
    auth
  }) {
    const params = request.all();
    const session = await auth.getUser();
    const trx = await Database.beginTransaction();
    try {

      const skill = new SeekerSkill();
      skill.skills = params.skills;
      skill.user_id = session.id;

      await skill.save();
      trx.commit();
      const newSession = await auth.getUser();
      const skills = await newSession.seekerSkills().fetch() || [];
      return response.json(Payload.success(skills))
    } catch (error) {
      trx.rollback();
      console.log(error)
      response.forbidden(
        Payload.error(),
        error
      )
    }
  }

  /**
   * Obtiene un array en strng separado por commas lo 
   * trasnforma en array y lo inserta no se deben repetir.
   *
   * @param {*} {
   *     request,
   *     response,
   *     auth
   *   }
   * @returns
   * @memberof SeekerController
   */
  async categories({
    request,
    response,
    auth
  }) {
    const params = request.all();
    const trx = await Database.beginTransaction();
    try {
      const categories = params.category_id.split(',');
      const session = await auth.getUser();
      let seekerCategories = await session.seekerCategories().fetch();
      let isEdit = false;
      if (seekerCategories.rows.length > 0) {
        const ids = seekerCategories.rows.map((c) => c.category_id);
        isEdit = true;
        for (const index in categories) {
          const category = categories[index];
          if (!ids.includes(parseInt(category))) {
            const seekerCategory = new SeekerCategory();
            seekerCategory.user_id = session.id;
            seekerCategory.category_id = category;
            seekerCategory.save();
          }
        }
      } else {

        for (const category in categories) {
          const seekerCategory = new SeekerCategory();
          seekerCategory.user_id = session.id;
          seekerCategory.category_id = categories[category];
          seekerCategory.save();
        }
      }
      trx.commit();

      // Se renueva la sessión para obtener los nuevos datos una ves commit
      let categoryResources = await this.getNewData(auth);
      return response.json(Payload.success(categoryResources))
    } catch (error) {
      trx.rollback();
      console.log(error)
      response.forbidden(
        Payload.error(),
        error
      )
    }
  }

  async languages({
    request,
    response,
    auth
  }) {
    const params = request.all();
    const trx = await Database.beginTransaction();
    const session = await auth.getUser();
    try {
      const seekerLanguage = new SeekerLanguage();
      seekerLanguage.fill({
        ...params,
        user_id: session.id,
      });
      await seekerLanguage.save();
      trx.commit();

      let languageResources = await this.getNewLanguage(auth);
      return response.json(Payload.success(languageResources))
    } catch (error) {
      trx.rollback();
      console.log(error)
      response.forbidden(
        Payload.error(),
        error
      )
    }
  }


  async getNewData(auth) {
    const newSession = await auth.getUser();
    const resources = await newSession.seekerCategories().fetch();
    let categoryResources = [];
    for (let index = 0; index < resources.rows.length; index++) {
      const category = resources.rows[index];
      let categoryName = await category._category();

      categoryResources.push(Object.assign(category, categoryName));
    }
    return categoryResources;
  }

  async getNewLanguage(auth) {
    const newSession = await auth.getUser();
    let languagesResources = [];
    const resources = await newSession.seekerLanguages().fetch();
    for (let index = 0; index < resources.rows.length; index++) {
      const language = resources.rows[index];
      let resumeLanguageName = await language._resume_language();
      let languageProficiencyName = await language._language_proficiency();
      const resume = Object.assign(language, resumeLanguageName);
      const proficiency = Object.assign(resume, languageProficiencyName);
      languagesResources.push(proficiency);
    }
    return languagesResources;
  }



  /**
   * Permite subir el cv del candidato
   *
   * @param {*} { request, response, auth }
   * @returns
   * @memberof SeekerController
   */
  async academic({
    request,
    response,
    auth
  }) {
    const fileName = 'cv';
    const params = request.except([fileName]);

    try {
      // Bloque para subir archivos
      const cvFile = await this.uploadFile(fileName, request,{
        path: 'cvs',
        types: ['pdf', 'docx', 'doc'],
        size: '10'
      });
      if (cvFile) 
        params.cv = cvFile.name;
      else
        delete params.cv;
// Bloque para subir archivos
      const session = await auth.getUser();
      let seekerAcademic = await session.seekerAcademic().fetch();
      if (!seekerAcademic)
        seekerAcademic = new SeekerAcademic();
      seekerAcademic.merge(params);
      seekerAcademic.user_id = session.id;
      await seekerAcademic.save();

      let locationResources = {};
      if (seekerAcademic)
        locationResources = await seekerAcademic._scholarship() || {};


      return response.json(Payload.success(Object.assign(seekerAcademic, locationResources)))
    } catch (error) {
      response.forbidden(
        Payload.error(error),
        error
      )
    }
  }

  async getSkillData(auth) {
    const newSession = await auth.getUser();
    return await newSession.seekerSkills().fetch();
  }

  async deleteSkill({
    request,
    response,
    auth
  }) {
    const params = request.all();
    try {

      const message = Antl.formatMessage('messages.delete');

      const seekerSkill = await SeekerSkill.findOrFail(params.id);
      if (!seekerSkill)
        return response.forbidden(Payload.error())
      await seekerSkill.delete();
      let skillResources = await this.getSkillData(auth);

      return response.json(Payload.success(skillResources, message))
    } catch (error) {
      console.log(error)
      return response.forbidden(
        Payload.error([]),
        error
      )
    }
  }

  async deleteCategory({
    request,
    response,
    auth
  }) {
    const params = request.all();
    try {

      const message = Antl.formatMessage('messages.delete');

      const seekerCategory = await SeekerCategory.findOrFail(params.id);
      if (!seekerCategory)
        return response.forbidden(Payload.error())
      await seekerCategory.delete();

      let categoryResources = await this.getNewData(auth);

      return response.json(Payload.success(categoryResources, message))
    } catch (error) {
      console.log(error)
      return response.forbidden(
        Payload.error([]),
        error
      )
    }
  }



  async deleteLanguage({
    request,
    response,
    auth
  }) {
    const params = request.all();
    try {

      const message = Antl.formatMessage('messages.delete');

      const seekerLanguage = await SeekerLanguage.findOrFail(params.id);
      if (!seekerLanguage)
        return response.forbidden(Payload.error())
      await seekerLanguage.delete();

      let languageResources = await this.getNewLanguage(auth);

      return response.json(Payload.success(languageResources, message))
    } catch (error) {
      console.log(error)
      return response.forbidden(
        Payload.error([]),
        error
      )
    }
  }


  async professionalExperiencies({
    request,
    response,
    auth
  }) {

    const params = request.all();
    const trx = await Database.beginTransaction();
    const session = await auth.getUser();
    try {
      const seekerProfessionalExperience = new SeekerProfessionalExperience();
      seekerProfessionalExperience.fill({
        ...params,
        user_id: session.id,
      });

      await seekerProfessionalExperience.save();
      trx.commit();
      let seekerProfessionalExperienceResource = await this.seekerProfessionalExperienceResource(auth);
      return response.json(Payload.success(seekerProfessionalExperienceResource))
    } catch (error) {
      trx.rollback();
      console.log(error)
      response.forbidden(
        Payload.error(error),
        error
      )
    }
  }

  async deleteProfessionalExperiency({
    request,
    response,
    auth
  }) {
    const params = request.all();
    try {

      const message = Antl.formatMessage('messages.delete');

      const seekerProfessionalExperience = await SeekerProfessionalExperience.findOrFail(params.id);
      if (!seekerProfessionalExperience)
        return response.forbidden(Payload.error())
      await seekerProfessionalExperience.delete();

      let seekerProfessionalExperienceResource = await this.seekerProfessionalExperienceResource(auth);

      return response.json(Payload.success(seekerProfessionalExperienceResource, message))
    } catch (error) {
      console.log(error)
      return response.forbidden(
        Payload.error([]),
        error
      )
    }
  }

  async seekerProfessionalExperienceResource(auth) {
    const newSession = await auth.getUser();
    return await newSession.seekerProfessionalExperiences().fetch();
  }
}


module.exports = SeekerController
