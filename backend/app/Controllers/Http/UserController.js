'use strict'
const User = use('App/Models/User')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database');

class UserController {

    async index ({ response }) {
        
        let users = await User.all()
        return response.json(Payload
            .success(users))
    }
    async edit ({ params, response }) {
        let user = await User.findOrFail(params.id);
        return response.json(Payload
            .success(user))
    }
    async store ({ request, response }) {
    }
    async show ({ params, request, response, view }) {
    }
    async update ({ params, request, response }) {
    }
    async destroy ({ params, request, response }) {
    }

   
}

module.exports = UserController
