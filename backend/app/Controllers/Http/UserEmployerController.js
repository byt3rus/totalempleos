'use strict'


const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const Database = use('Database')
const User = use('App/Models/User')
const UserEmployer = use('App/Models/UserEmployer')
const UsersReason = use('App/Models/UsersReason')
const {
  validate
} = use('Validator')
const Service = use('App/Helpers/Service')
const Ms = use('App/Constants')
const BaseController = use('App/Controllers/Http/BaseController')
const Roles = use('App/Models/Rol/Roles')
class UserEmployerController extends BaseController {

  async index({
    request,
    response
  }) {
    const _roles = Roles.all();
    const exclude = _roles.EMPLOYER;
    let {
      page,
      search,
      order,
      actives,
      limit
    } = request.all();

    const fields = ['company_name', 'zipcode', 'mobile_contact'];
    page = page && page > 0 ? page : 1;
    search = search ? search : '';
    order = order ? order : 'asc';
    limit = limit ? limit : Ms.PAGINATION_DEFAULT;
    const clauseWhere = this.search(fields, search);
    const values = this.value(fields, search);

    const userEmployers = await UserEmployer.query()
      .with('city')
      .with('state')
      .with('user')
      .where(function () {
        if(actives)
          this.where('active',actives)
      })
      .where(function () {
        this.whereRaw(clauseWhere,values)
      })
      .orderBy('id', order)
      .paginate(page, limit)

    return response.json(Payload.success(userEmployers))
  }

  async avatar({
    params,
    response
  }) {

    try {
      let userEmployer = await UserEmployer.query()
      .where('id',params.id)
      .with('city')
      .with('state')
      .with('user')
      .first();
      console.log(userEmployer)
      return await this.showCompany(response, userEmployer);
    } catch (error) {
      console.log(error);
      await this.responseError(response, error);
    }
  }

  async validateUser({
    response
  }) {
    return response.json(Payload.success());
  }

  async update ({ params, request,response,auth }) {
    const message  = Antl.formatMessage('messages.update');
    const data = request.all()
    const trx = await Database.beginTransaction();
    try
    {
      // Bloque para subir archivos
    const fileName = 'company_logo';
    const file = await this.uploadFile(fileName, request);
      if (file) {
        data.company_logo = file.name;

    } else
      delete data.company_logo;
    // Bloque para subir archivos

    const userEmployers = await Database
    .table('user_employers')
    .where('id', params.id)
    .update(data)
    trx.commit();
      return response.json(Payload.success(data, message))
    }catch(error)
    {
      console.log(error.message);
      trx.rollback();
      response.forbidden(Payload.error(error.message))
    }
  }

  async store({
    request,
    response,
    auth
  }) {
    const message = Antl.formatMessage('messages.store');
    const session = await auth.getUser();
    const userParams = request.only(User.register);
    const employer = request.only(UserEmployer.register);
    const role = Roles.all().EMPLOYER;

    const trx = await Database.beginTransaction();
    try {

      // Bloque para subir archivos
      const fileName = 'company_logo';
      const file = await this.uploadFile(fileName, request);
      if (file) {
        employer.company_logo = file.name;

      } else
        delete employer.company_logo;
      // Bloque para subir archivos

      let user = new User();
      user.fill({
        ...userParams,
        role: role
      });
      user.generateUsername();
      await user.save();

      let userEmployer = new UserEmployer();
      userEmployer.fill({
        ...employer,
        created_by: session.id,
        updated_by: session.id,
        user_id: user.id
      });

      await userEmployer.save();
      trx.commit();
      return response.json(Payload.success(userEmployer, message))
    } catch (error) {
      console.log(error);
      trx.rollback();
      response.forbidden(Payload.error())
    }

  }

  async show ({ params, response }) {
    const userEmployer = await UserEmployer.query()
    .with('updated')
    .with('created')
    .with('state')
    .with('city')
    .with('user')
    .where('id',params.id)
    .first()
    if(!userEmployer)
      return response.forbidden(Payload.error())
    return response.json(userEmployer)
}

async edit({request,response})
{
  const param = request.only(['id', ]);
  const userEmployer = await Database.table('user_employers')
      .where('id',param.id)
      .first();
      return response.json(Payload.success(userEmployer))
}

  async active({
    response,
    request
  }) {
    const param = request.only(['id', ]);

    const trx = await Database.beginTransaction();
    try {
      // Tiene afectado el find por un hook por lo que debe usar Database
      const userEmployer = await Database.table('user_employers')
      .where('id',param.id)
      .first();
      const user = await User.findOrFail(userEmployer.user_id);
      const active = userEmployer.active === 1 ? 0 : 1;
      user.changeActive();

      await Database.table('user_employers')
      .where('id',param.id).update({active:active});

      await user.save();
      trx.commit();
      return response.json(Payload.success())
    } catch (error) {
      console.log(error);
      trx.rollback();
      response.forbidden(Payload.error())
    }
  }

}

module.exports = UserEmployerController
