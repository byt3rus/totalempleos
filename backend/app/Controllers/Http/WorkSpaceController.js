'use strict'
const WorkSpace = use('App/Models/WorkSpace')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const BaseController = use('App/Controllers/Http/BaseController')
class WorkSpaceController  extends BaseController {

    async index ({ response,request }) {
        return super.index(request,response,'work_spaces',['title']);
    }

    async show ({ params, response }) {
        const work_space = await WorkSpace.query()
        .with('updated')
        .with('created')
        .where('id',params.id)
        .first()
        if(!work_space)
          return response.forbidden(Payload.error())
        return response.json(work_space)
    }


    async store ({ request, response, auth }) {
        const message  = Antl.formatMessage('messages.store');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          ])


        let work_space = new WorkSpace();
        work_space.fill({...data,
          created_by: user.id,
          updated_by: user.id
        });
        

        await work_space.save();
         return response.json(Payload.success(work_space,message))
    }

    async update ({ params, request,response,auth }) {
        const message  = Antl.formatMessage('messages.update');
        const user = await auth.getUser();
        const data = request.only([
          'title',
          'description',
          'active',
        ])
    
        const work_space = await WorkSpace.findOrFail(params.id)
    
        work_space.merge(data);
        work_space.updated_by = user.id;
        await work_space.save()
        return response.json(work_space)
      }
    
      async destroy ({ params,response }) {
        const message  = Antl.formatMessage('messages.delete');
        const work_space = await WorkSpace.findOrFail(params.id)
        await work_space.delete()
        return response.json(Payload.success(work_space,message))
      }

      async active ({ response,request }) {
        const param = request.only(['id',])
        const work_space = await WorkSpace.findOrFail(param.id);
        work_space.changeActive()
        await work_space.save();
        return response.json(Payload.success(work_space))
      }
}

module.exports = WorkSpaceController
