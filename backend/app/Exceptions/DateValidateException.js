'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')
const message = 'Estas fechas no son válidas'
const status = 403
const code = 'E_NOT_DATE_VALID'
class DateValidateException extends LogicalException {
  constructor () {
    super(message, status, code)
  }
  handle (error, { response }) {
    response
      .status(500)
      .send('Custom exception handled!')
  }
  
}

module.exports = DateValidateException
