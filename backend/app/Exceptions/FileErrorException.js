const {
    LogicalException
  } = require('@adonisjs/generic-exceptions')
  const Payload = require('../Models/Response/Payload')
  
  /**
   * Manejador de excepciones para archivos
   * @class FileErrorException
   * @extends {LogicalException}
   */
  class FileErrorException extends LogicalException {
      handle (error, { response }) {
          response.status(403)
          .send(error.message);
        }
  }
  
  module.exports = FileErrorException
  