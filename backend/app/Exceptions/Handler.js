'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl')
/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle (error, { request, response }) {


   console.log(error);
    
    let uuid = Payload.uuidv4();
    let message = error.status===404 ? Antl.formatMessage('messages.404_error') :Antl.formatMessage('messages.common_error');
    if(error.status===404)
      uuid= "";
    if(error.status===403)
    {
      return response.unauthorized(Payload.error(error.message))
    }
      
    return response.status(error.status).json(
      Payload.error(`${message} ${uuid}`,error.message,uuid)
    )

    

  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
