'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

const message = 'No tienes acceso a este recurso.'
const status = 403
const code = 'ACCESO'

class RoleException extends LogicalException {
  constructor () {
    super(message, status, code)
  }
}

module.exports = RoleException
