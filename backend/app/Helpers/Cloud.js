'use strict'

const Model = use('Model')
const Env = use('Env')
const Helpers = use('Helpers')
const fs = require('fs');
const {
  BlobServiceClient
} = require("@azure/storage-blob");
const Antl = use('Antl');
const Ms = use('App/Constants');
const Hash = use('Hash')
const Encryption = use('Encryption')
const btoa = require('btoa');
const moment = require("moment");
class Cloud {

  containerName;
  blobSasUrl;
  blobServiceClient;
  static IMAGES ='images';
  static CVS ='cvs';
  


  constructor(containerName) {
    this.blobSasUrl = Env.get('BLOB_SAS_URL');
    this.containerName = containerName;
    this.blobServiceClient = new BlobServiceClient(this.blobSasUrl);
  }

  static async create(containerName) {
    const instance = new this(containerName);
    await instance.createContainer();
  }

  static get containers()
  {
      return [this.IMAGES,this.CVS];
  }

  static async buildContainers()
  {
      for (let index = 0; index < this.containers.length; index++) {
          const container = this.containers[index];
          console.log(container)
          await this.create(container);
      }
  }
  static async upload(content,containerName=this.IMAGES)
  {
    const instance = new this(containerName);
    const blockBlobClient = instance.blockBlobClient;
    const response = await blockBlobClient.upload(content, content.length);
    return blockBlobClient.blobContext.client.url;
  }

  static get baseCloud()
  {
      return Env.get('BASE_CLOUD');
  }

  static async cleanContainers()
  {
      for (let index = 0; index < this.containers.length; index++) {
          const container = this.containers[index];
          console.log(container)
          await this.delete(container);
      }
  }

  static async delete(containerName) {
    const instance = new this(containerName);
    await instance.containerClient.deleteContainer();
  }

  async createContainer() {
    try {
      console.log(`Creating container "${this.containerName}"...`);
      await this.containerClient.create();
      console.log(`Done.`);
    } catch (error) {
      console.log(`Something wrong...`);
      console.log(error.message);
    }
  }
  async deleteContainer() {
    try {
      console.log(`Deleting container "${this.containerName}"...`);
      await this.containerClient.delete();
      console.log(`Done.`);
    } catch (error) {
      console.log(`Something wrong...`);
      console.log(error.message);
    }
  }

  get containerClient() {
    return this.blobServiceClient.getContainerClient(this.containerName)
  }

  get blockBlobClient()
  {
    const blobName = btoa("image_" + new Date().getTime());
    return this.containerClient.getBlockBlobClient(blobName);
  }





}

module.exports = Cloud
