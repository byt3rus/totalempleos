'use strict'

const Model = use('Model')
const Env = use('Env')
const Helpers = use('Helpers')
const fs = require('fs');
const {Storage} = require('@google-cloud/storage');
const { BlobServiceClient } = require("@azure/storage-blob");
const Antl = use('Antl');
const Ms = use('App/Constants');
const Hash = use('Hash')
const Encryption = use('Encryption')
const moment = require("moment");
class Service {


    async getFile(name,bucket)
    {
        return name;
    }

    getAvatarUrl(avatar)
    {
        if(avatar)
            return `${Ms.URL_CLOUD_PUBLIC}${Ms.BUCKET_USERS_IMAGES}/${avatar}`;
        return false;
    }

    getFileURL(name)
    {
        return `${Ms.URL_CLOUD_PUBLIC}${Ms.BUCKET_EMPLOYER_IMAGES}/${name}`;
    }


    getFileURLCV(name)
    {
        return `${Ms.URL_CLOUD_PUBLIC}${Ms.BUCKET_EMPLOYER_IMAGES}/${name}`;
    }

    static app()
    {
        return new this;
    }

    static humanDate(date)
    {
        var options = { year: 'numeric', month: 'long', day: 'numeric', hour:'2-digit' , minute: '2-digit' };
        return date.toLocaleDateString("es-ES", options)
    }

    static seen(value)
    {
        return value ?  Antl.formatMessage('messages.seen') :  Antl.formatMessage('messages.not_seen');
    }

    static agoTime(date)
    {   
        moment.locale('es');   
        return moment(date, "YYYY/MM/DD").fromNow();
    }

    static getAge(date)
    {
        return  `${moment().diff(date, 'years')} años`;
    }

    static currency(numberData)
    {
        const options2 = {
            style: 'currency',
            currency: 'MXN'
          };
          const numberFormat2 = new Intl.NumberFormat('es-MX', options2);
          return numberFormat2.format(numberData);
    }

    /**
     * Permite generar un Token Random
     *
     * @param {*} tokenData
     * @returns
     * @memberof Service
     */
    async getToken(tokenData) {
        tokenData = await Hash.make(tokenData);
        tokenData = Encryption.encrypt(tokenData)
        tokenData = tokenData.substring(0, 50);
        tokenData = tokenData.split("/").join("");
        return tokenData;
      }
    /**
     * 
     * @param {string} bucket nombre del deposito
     */
    async userUpload({bucket,maxSize,file})
    {
        const removeFile = Helpers.promisify(fs.unlink);
        const fileName = await this.getFileName(file);
        const path = Helpers.tmpPath('uploads');
        await file.move(path, {
            name: fileName,
            overwrite: true
          });
        const fullPath = `${path}/${fileName.toString()}`;
        await this.getStorage().bucket(bucket)
        .upload(fullPath, {
            gzip: true,
            metadata: {
                cacheControl: 'public, max-age=31536000',
            },
        }).catch((e)=>{
            console.log(e);
            throw Antl.formatMessage('validations.file_image_error',{size:maxSize});
        });

        await removeFile(fullPath).catch((e)=>{
            console.log(e)
            throw Antl.formatMessage('messages.profile_error');
        });
        return fileName;
    }

    static mapErrorFile(response, size) {
        let error = "";
        console.log("response",response);
        switch (response.type) {
          case 'type':
            error = 'contiene un formato inválido.';
            break;
    
          case 'size':
            error = ` tiene un tamaño que debería ser menor a ${size}.`;
            break;
    
          default:
            break;
        }
        return `El archivo ${response.clientName} ${error}`;
      }
    

    getStorage()
    {
        return new Storage({
            projectId: this.params.projectId,
            keyFilename: this.params.keyFilename
        });
    }

    async getFileName(file,subfix="")
    {
        const name = await this.getToken(`${subfix}${new Date().getTime()}`)
        return `${name}.${file.extname}`;
    }

    get params(){
        return {
            projectId:Env.get('GOOGLE_CLOUD_PROJECT_ID'),
            keyFilename: Helpers.appRoot(Env.get('GOOGLE_CLOUD_KEY_FILE'))
        }
    }

}

module.exports = Service
