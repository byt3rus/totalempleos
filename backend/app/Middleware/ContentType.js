'use strict'

class ContentType {
    async handle ({ request }, next) {
        let headers = request.headers()
        headers.accept = 'application/json'
        await next()
      }
  }

  module.exports = ContentType