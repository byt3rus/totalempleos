'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const RoleException = use('App/Exceptions/RoleException')
const Roles = use('App/Models/Rol/Roles')

class RoleDetector {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request,auth }, next,props) {
    // call next to advance the request
    const user = await auth.getUser();
    //Verifica si el rol que esta mandando es el correcto
    if (!props.includes(user.role) && user.role!==Roles.all().GOD)
      throw new RoleException()
    await next()
  }
}

module.exports = RoleDetector
