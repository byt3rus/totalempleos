'use strict'

const Model = use('Model')
/**
 * Categorías de la vacante
 */
class Category extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
    //Soft delete activo por defecto
    this.addHook('beforeCreate', async (categoryInstance) => {
      categoryInstance.deleted_at = null;
    })

  }

  static get visible () {
    return ['id','title']
  }
  /**
   * Permite ver categorias activas que el usuario ya no tiene
   *
   * @static
   * @param {*} query
   * @param {*} ids
   * @returns
   * @memberof Category
   */
  static scopeUserActives (query,ids) {
    return query
   .whereNotIn("id",ids)
    .actives()
    .orderBy('title', 'asc')
  }

  static scopeOnlyActives (query,ids) {
    return query
   .whereIn("id",ids)
    .actives()
    .orderBy('title', 'asc')
  }

  static scopeActives (query) {
    return query.where('active', 1)
  }
  static get visible () {
    return ['id','title','created_at','updated_at','active']
  }


  async show()
  {
    const {id,title,created_at,active} = this;
    const updatedUser = await this.updated().fetch()
    return {
      id,
      title,
      active,
      created_at,
      updated: updatedUser,
    };
  }


  changeActive()
  {
    this.active = this.active===1 ? 0 : 1;
  }

  toS ()
  {
    return this.title;
  }

  created() {
    return this.belongsTo('App/Models/User','created_by','id')
  }
  
  updated() {
    return this.belongsTo('App/Models/User','updated_by','id')
  }

  job () {
    return this.belongsTo('App/Models/Job')
  }
}

module.exports = Category
