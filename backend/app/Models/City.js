'use strict'

const Model = use('Model')

class City extends Model {
  static boot () {
    super.boot()
  }

  static get visible () {
    return ['id','name']
  }

  /**
   * Mapea la búsqueda por dos modelos
   * @param {*} models 
   */
  static search(models)
  {
    const {cities,states} = models;
    let citiesMap = cities.toJSON().map((city)=>
    {
      return  { 
        id: city.id,
        name: `${city.name} ${city.state.name}`,
        model: 'City',
        description: 'Ciudad',
      };
    });

    let statesMap = states.toJSON().map((state)=>
    {
      return  { 
        id: state.id,
        name: state.name,
        model: 'State',
        description: 'Estado'
      };
    });
    return statesMap.concat(citiesMap);
  }

  toS ()
  {
    return this.name;
  }

  state() {
    return this.belongsTo('App/Models/State')
  }
}

module.exports = City
