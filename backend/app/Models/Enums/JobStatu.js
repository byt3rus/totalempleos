'use strict'
const DATA = use('App/Models/Enums/Json')

class JobStatu {

    static getData()
    {
        let items = [];
        for(let i in DATA.JOBSTATUS)
        {
            items.push({
                index: i,
                value: DATA.JOBSTATUS[i],
            });
        }      
        return items;
    }

    static find(index)
    {
        return DATA.JOBSTATUS[index];
    }
}

module.exports = JobStatu
