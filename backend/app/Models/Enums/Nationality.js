'use strict'
const DATA = use('App/Models/Enums/Json')

class Nationality {

    static getData()
    {
        let items = [];
        for(let i in DATA.NATIONALITIES)
        {
            items.push({
                index: i,
                value: DATA.NATIONALITIES[i],
            });
        }      
        return items;
    }

    static find(index)
    {
        return DATA.NATIONALITIES[index];
    }

    static search(q)
    {
        q = q.toUpperCase();

        let result = [];
        let data = DATA.NATIONALITIES.filter((item,index)=>{
            return item.match(new RegExp(q));
        }); 
        data.forEach((d)=>{
            let i;
            DATA.NATIONALITIES.forEach((item,index)=>{
                if(d===item)
                {
                    i = index
                }
            })
            result.push({
                id: i,
                name: d
            }) 
        });
        return result;
    }
}

module.exports = Nationality
