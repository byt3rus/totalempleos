'use strict'
const DATA = use('App/Models/Enums/Json')

class Sex {

    static getData()
    {
        let items = [];
        for(let i in DATA.SEX)
        {
            items.push({
                index: i,
                value: DATA.SEX[i],
            });
        }      
        return items;
    }

    static find(index)
    {
        return DATA.SEX[index];
    }
}

module.exports = Sex
