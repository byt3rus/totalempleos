'use strict'

const JobHook = exports = module.exports = {}
const Service = use('App/Helpers/Service')

JobHook.setEmployer = async (Jobs) => {
  Jobs.map((Job) => {
    Job.spec = `${Job.getRelated('job_type').title} , ${Job.getRelated('job_work_space').title} `;
    Job.employer = `${Job.getRelated('user_employer').company_name}  `;
    Job.dates = `${Service.humanDate(Job.start_date)} al  ${Service.humanDate(Job.end_time)} `;
    Job.logo = Job.getRelated('user_employer').company_logo;
    Job.location = `${Job.getRelated('city').name}, ${Job.getRelated('state').name} ${Job.zipcode || ""}`;
  })
}
