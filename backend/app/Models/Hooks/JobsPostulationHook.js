'use strict'
const Service = use('App/Helpers/Service')
const JobsPostulationHook = exports = module.exports = {}

JobsPostulationHook.setJob = async (Postulations) => {
    Postulations.map((Postulation) => {
        const job =  Postulation.getRelated('job');
        Postulation.ago = Service.agoTime(job.start_date); 
        Postulation.title = job.title ;
        Postulation.full_seen = Service.seen(Postulation.has_seen);
        
    })
  }