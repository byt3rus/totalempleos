'use strict'

const SeekerProfessionalExperienceHook = exports = module.exports = {}


SeekerProfessionalExperienceHook.checkDates = async (modelInstance) => {
  modelInstance.deleted_at = null;
  if ( modelInstance.start_year > modelInstance.end_year) {
    modelInstance.end_year = modelInstance.start_year;
    modelInstance.start_year = modelInstance.end_year;
    
  }
}
