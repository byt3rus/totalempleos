'use strict'

const UserEmployerHook = exports = module.exports = {}

UserEmployerHook.method = async (modelInstance) => {
}

UserEmployerHook.setLocation = async (Employers) => {
    Employers.map((Employer) => {
        Employer._location = `${Employer.getRelated('city').name} , ${Employer.getRelated('state').name} ` ;
        Employer.email = `<br> ${Employer.getRelated('user').email}  ` ;
        
    })
  }

  UserEmployerHook.findLocation = async (Employer) => {
        Employer._location = `${Employer.getRelated('city').name} , ${Employer.getRelated('state').name} ` ;
        Employer.email = `<br> ${Employer.getRelated('user').email}  ` ;
        
  }
  