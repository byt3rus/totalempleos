'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use('Database')
const Service = use('App/Helpers/Service')
const UserEmployer = use('App/Models/UserEmployer')
const _ = require('lodash');
class Job extends Model {

  static boot() {
    super.boot()
    //this.addHook('afterPaginate', 'JobHook.setEmployer')
  }

  static get computed() {
    return ['employer', 'spec', 'dates', 'logo', 'ago', 'location']
  }

  getLocation() {
    return this.location;
  }
  getAgo({
    start_date
  }) {
    return Service.agoTime(start_date);
  }

  getSpec({}) {
    return `${this.spec}`
  }

  getDates({}) {
    return `${this.dates}`
  }

  getEmployer({}) {
    return `${this.employer}`
  }

  getLogo({}) {
    return this.logo;
  }

  static get visible() {
    return ['id', 'location', 'title', 'confidencial', 'ago', 'logo', 'description', 'show_salary', 'salary', 'employer', 'spec', 'dates', 'active', 'created_at', 'updated_at']
  }

  created() {
    return this.belongsTo('App/Models/User', 'created_by', 'id')
  }

  updated() {
    return this.belongsTo('App/Models/User', 'updated_by', 'id')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  job_type() {
    return this.belongsTo('App/Models/JobType')
  }

  job_work_space() {
    return this.belongsTo('App/Models/WorkSpace')
  }
  user_employer() {
    return this.belongsTo('App/Models/UserEmployer')
  }

  jobsCategories() {
    return this.hasMany('App/Models/JobsCategory')
  }

  categories() {
    return this.manyThrough('App/Models/JobsCategory', 'category')
  }

  city() {
    return this.belongsTo('App/Models/City')
  }
  state() {
    return this.belongsTo('App/Models/State')
  }

  static scopeDefault(query) {
    const todayDate = new Date().toISOString().slice(0, 10);
    return query.where('start_date', '<=', todayDate)
      .where('end_time', '>=', todayDate)

  }

  static scopeMain(query) {
    return query
      .actives()
      .default();
  }

  static scopeByUser(query, ids) {

    return query
      .whereIn("id", ids)
  }


  static scopeActives(query) {
    return query
      .where('active', 1)

  }

  async show() {
    const {
      id,
      title,
      active,
      created_at,
      updated_at,
      description,
      confidencial,
      show_salary,
      salary,
      seen,
      end_time,
      start_date,
      zipcode,
    } = this;
    const employer = await Database.table('user_employers')
      .where('id', this.user_employer_id)
      .first();

    const jobType = await this.job_type().fetch();
    const jobWorkSpace = await this.job_work_space().fetch();
    const spec = `${jobType.title} , ${jobWorkSpace.title} `

    const city = await this.city().fetch();
    const state = await this.state().fetch();
    const location = `${city.name} , ${state.name} ${zipcode || ""}`;

    const updated = await this.updated().fetch();
    const created = await this.created().fetch();

    const categories = [];

    const jobsCategories = await this.jobsCategories().fetch();
    for (let index = 0; index < jobsCategories.rows.length; index++) {
      const jobsCategory = jobsCategories.rows[index];
      const category = await jobsCategory.category().actives().fetch();
      if (category)
        categories.push(category);
    }

    return {
      id: id,
      title: title,
      employer: employer.company_name,
      employer_logo: UserEmployer.logo(employer),
      description: description,
      spec: spec,
      location: location,
      active: active,
      confidencial: confidencial,
      show_salary: show_salary,
      seen: seen,
      salary: salary || 0,
      dates: `${Service.humanDate(start_date)} al ${Service.humanDate(end_time)}`,
      categories: categories,

      //Datos de control
      created_at: created_at,
      updated_at: updated_at,
      created: created,
      updated: updated,

    };
  }

  static mapping(jobs, categories) {
    const collection = jobs.map((job) => {
      const logo = job.confidencial ? null : UserEmployer.logo({
        company_logo: job.logo,
        id: job.employer_id
      });
      const spec = `${job.job_type} , ${job.work_space} `
      const location = `${job.state} , ${job.city} `;
      const _categories = categories.filter((c) => c.job_id === job.id)
      delete job.state;
      delete job.city;
      delete job.work_space;
      delete job.job_type;
      delete job.employer_id;

      return {
        ...job,
        ago: `${Service.agoTime(job.ago)}`,
        logo: logo,
        spec: spec,
        location: location,
        categories: _categories,
      };
    });
    
    return collection;
  }

  static findMatch(search,categories)
  {
    if (search && search.toLowerCase().match(/^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/g)) {
      search = search.toLowerCase().match(/^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/g)[0];
      const titles = categories.map((c)=>c.title.toLowerCase());
      if(titles.includes(search))
        return categories.filter((c)=>search===c.title.toLowerCase());
    }
    return false;
  }

  async search() {
    const {
      id,
      title,
      confidencial,
      description,
      show_salary,
      salary,
      active,
      start_date,
      zipcode,
    } = this;
    const employer = await Database.table('user_employers')
      .where('id', this.user_employer_id)
      .first();

    const jobType = await this.job_type().fetch();
    const jobWorkSpace = await this.job_work_space().fetch();
    const spec = `${jobType.title} , ${jobWorkSpace.title} `

    const city = await this.city().fetch();
    const state = await this.state().fetch();
    const location = `${city.name} , ${state.name} ${zipcode || ""}`;

    const categories = [];

    const jobsCategories = await this.jobsCategories().fetch();
    for (let index = 0; index < jobsCategories.rows.length; index++) {
      const jobsCategory = jobsCategories.rows[index];
      const category = await jobsCategory.category().actives().fetch();
      if (category)
        categories.push(category);
    }

    const logo = confidencial ? null : UserEmployer.logo(employer);
    const company_name = confidencial ? null : employer.company_name;

    return {
      id: id,
      title: title,
      employer: company_name,
      logo: logo,
      active: active,
      description: description,
      spec: spec,
      location: location,
      confidencial: confidencial,
      show_salary: show_salary,
      salary: salary || 0,
      ago: `${Service.agoTime(start_date)}`,
      categories: categories.map((c) => {
        return {
          id: c.id,
          title: c.title
        }
      }),
    };
  }

  static get dates() {
    return super.dates.concat(['start_date', 'end_time'])
  }

  getActive(value) {
    return Number(value)
  }

  getShowSalary(value) {
    return Number(value)
  }

  getConfidencial(value) {
    return Number(value)
  }


  getSalary(value) {
    return Number(value)
  }


  changeActive() {
    this.active = this.active ? 0 : 1;
  }

}

module.exports = Job
