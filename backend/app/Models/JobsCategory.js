'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class JobsCategory extends Model {
    static boot() {
        super.boot()
    
        this.addTrait('@provider:Lucid/SoftDeletes')
        //Soft delete activo por defecto
        this.addHook('beforeCreate', async (userInstance) => {
          userInstance.deleted_at = null
        })
      }

      static get hidden () {
        return ['job_id','created_at','updated_at','deleted_at']
      }

      static scopeByCategoryIds (query,ids) {
        return query
       .whereIn("category_id",ids)
        .orderBy('id', 'asc')
      }
      

      category() {
        return this.belongsTo('App/Models/Category')
      }

      job() {
        return this.belongsTo('App/Models/Job')
      }
}

module.exports = JobsCategory
