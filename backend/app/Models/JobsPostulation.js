'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Service = use('App/Helpers/Service')
class JobsPostulation extends Model {
  static boot() {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
    //Soft delete activo por defecto
    this.addHook('beforeCreate', async (instance) => {
      instance.deleted_at = null;
    });
    this.addHook('afterPaginate', 'JobsPostulationHook.setJob')
  }
  static get computed() {
    return ['title', 'ago', 'date', 'fullseen']
  }
  getTitle({

  }) {
    return this.title;
  }


  async search(isAdmin) {
    const userRecord = {...this.toJSON(),
      admin: true
    };
    return isAdmin ? userRecord : this.toJSON();
  }

  getAgo({

  }) {
    return this.ago;
  }

  getFullseen({
  }) {
    return this.full_seen;
  }

  setSeen({
  }) {
    return Service.seen(this.seen);
  }

  getDate({
    created_at
  }) {
    return Service.agoTime(created_at);
  }

  static get visible() {
    return ['id_main', 'title', 'ago', 'fullseen', 'date','has_seen','seeker_id']
  }

  user() {
    return this.belongsTo('App/Models/User')
  }
  job() {
    return this.belongsTo('App/Models/Job')
  }
  getSeen(value) {
    return Number(value)
  }
}

module.exports = JobsPostulation
