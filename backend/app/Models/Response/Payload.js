'use strict'
/**
 * Modelo que permite dar un schema al API
 * para poder represtarlo de manera estandar
 */
const Antl = use('Antl')
const Logger = use('Logger')

class Payload {
    /**
     * 
     * @param {*} data Son los datos que vas a enviar en caso de ser necesario 
     * @param {*} message Mensaje de éxito
     */
    static success (data=[],message=Antl.formatMessage('messages.common_success')) {
        return {
           // success: true,
            payload: data,
            message: message
        }
    }

    static collections (data=[]) {
        return {
            collections: data,
        }
    }


    static error (error=Antl.formatMessage('messages.common_error'),e=false,uuid=0) {
        if(e!==false)
            Payload.captureError(e,uuid)
        return {
            //success: false,
            error: error,
        }
    }

    static captureError(error,uuid)
    {
        Logger.error(`uuid: ${uuid} error: ${error}`);
        console.error(`uuid: ${uuid} error: ${error}`);
    }

    static paginate()
    {
        return 50;
    }

    static uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
      }
}

module.exports = Payload