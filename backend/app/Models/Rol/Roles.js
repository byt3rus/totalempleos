'use strict'

const ALLROLES = {
  SEEKER: 'seeker',
  EMPLOYER: 'employer',
  ADMIN: 'admin',
  GOD: 'god'
}

class Roles {
    static all () {
      return ALLROLES
    }
    static getRole(rol)
    {
      switch (rol) {
        case 'seeker':
          return 'Candidato'
          break;
          case 'employer':
          return 'Empresa contratante'
          break;
          case 'admin':
          return 'Administrador del sistema'
          break;
          case 'god':
          return 'Super administrador del sistema'
          break;
      }
    }
  }

module.exports = Roles
