'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')
const btoa = require('btoa');
const atob = require('atob');
class SeekerAcademic extends Model {

  static get computed() {
    return ['cv']
  }

  getCv() {
    return this.buildCv();
  }

  buildCv() {
    const path = Env.get('SERVER', 'http://localhost:3333/');
    if (this.cv)
    {
      const encode = SeekerAcademic.encode(this);
      SeekerAcademic.decode(encode)
      return `${path}api/v1/document_cv/${encode}`;
    }
    return false;
  }

  static encode(model)
  {
    const salt = SeekerAcademic.reverse(model.school_name);
    const keyData = `${model.id}_${salt}`;
    return btoa(keyData);
  }

  static decode(hash)
  {
    hash = atob(hash);
    const array = hash.split('_');
    return {
      id: array[0],
      school_name: array[1]
    };
  }

  static reverse(value)
  {
    return [...value.substring(0,3)].reverse().join("");
  }

  static get hidden() {
    return ['user_id', 'created_at']
  }
  user() {
    return this.belongsTo('App/Models/User')
  }

  scholarship() {
    return this.belongsTo('App/Models/Scholarship')
  }

  async _scholarship() {
    const scholarship = await this.scholarship().fetch();
    return {
      'scholarship': scholarship.title,
    };
  }
}

module.exports = SeekerAcademic
