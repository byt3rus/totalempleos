'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SeekerBasic extends Model {
  static get hidden () {
    return ['id','user_id','created_at','updated_at']
  }

  static get dates () {
    return super.dates.concat(['birthday'])
  }

  created() {
    return this.belongsTo('App/Models/User', 'created_by', 'id')
  }

  updated() {
    return this.belongsTo('App/Models/User', 'updated_by', 'id')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = SeekerBasic
