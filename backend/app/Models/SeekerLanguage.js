'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const LanguageProficiency = use('App/Models/LanguageProficiency')
const ResumeLanguage = use('App/Models/ResumeLanguage')

class SeekerLanguage extends Model {
    static boot() {
        super.boot()
    
        this.addTrait('@provider:Lucid/SoftDeletes')
        //Soft delete activo por defecto
        this.addHook('beforeCreate', async (userInstance) => {
          userInstance.deleted_at = null
        })
      }

      static get hidden () {
        return ['user_id','created_at','updated_at','deleted_at']
      }
      user() {
        return this.belongsTo('App/Models/User')
      }

      async _resume_language()
      {
        const resume_language = await this.resume_language().fetch();
        return {
          'resume_language': resume_language.title,
        };
      }

      resume_language() {
        return this.belongsTo('App/Models/ResumeLanguage')
      }

      async _language_proficiency()
      {
        const language_proficiency = await this.language_proficiency().fetch();
        return {
          'language_proficiency': language_proficiency.title,
        };
      }

      language_proficiency() {
        return this.belongsTo('App/Models/LanguageProficiency')
      }
}

module.exports = SeekerLanguage
