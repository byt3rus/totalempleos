'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SeekerLocation extends Model {
    static get hidden () {
        return ['id','user_id','created_at','updated_at']
      }
      user() {
        return this.belongsTo('App/Models/User')
      }

      city() {
        return this.belongsTo('App/Models/City')
      }

      get full_location()
      {
        return `${this.neighborhood} ${this.street} ${this.number_ext} ${this.zip_code}`;
      }

      async _city()
      {
        const city = await this.city().fetch();
        const state = await this.state().fetch();
        return {
          'city': city.name,
          'state': state.name,
        };
      }

      getAvailability(value) {
        return Number(value)
      }

      state() {
        return this.belongsTo('App/Models/State')
      }
}

module.exports = SeekerLocation
