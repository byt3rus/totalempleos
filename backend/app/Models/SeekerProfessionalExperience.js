'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Payload = use('App/Models/Response/Payload')
const DateValidateException = use('App/Exceptions/DateValidateException')

class SeekerProfessionalExperience extends Model {
    static boot() {
        super.boot()
    
        this.addTrait('@provider:Lucid/SoftDeletes')
        //Soft delete activo por defecto
        this.addHook('beforeCreate', 'SeekerProfessionalExperienceHook.checkDates')

      }

      getCurrent(value) {
        return Number(value)
      }

      static get hidden () {
        return ['user_id','created_at','updated_at','deleted_at']
      }
      user() {
        return this.belongsTo('App/Models/User')
      }

}

module.exports = SeekerProfessionalExperience
