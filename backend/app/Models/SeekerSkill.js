'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SeekerSkill extends Model {

  static boot() {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
    //Soft delete activo por defecto
    this.addHook('beforeCreate', async (userInstance) => {
      userInstance.deleted_at = null
    })
  }

  static get hidden () {
    return ['user_id','created_at','updated_at','deleted_at']
  }
  user() {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = SeekerSkill
