'use strict'

const Model = use('Model')

class State extends Model {
  static boot () {
    super.boot()
  }

  static get visible () {
    return ['id','name']
  }

  toS ()
  {
    return this.name;
  }

  cities() {
    return this.hasMany('App/Models/City')
  }
}

module.exports = State
