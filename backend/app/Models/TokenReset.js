'use strict'

const Model = use('Model')

class TokenReset extends Model {

  static boot () {
    super.boot()

    //Activo por defecto
    this.addGlobalScope(function (builder) {
      builder.where('active', 1)
    })
  }
   

  user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = TokenReset
