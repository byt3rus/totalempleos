'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')
const Env = use('Env')
const Encryption = use('Encryption')
const Payload = use('App/Models/Response/Payload')
const Roles = use('App/Models/Rol/Roles')
const Service = use('App/Helpers/Service')
const UserRegister = use('App/Models/UserRegister')
const Category = use('App/Models/Category')
const JobType = use('App/Models/JobType')
const LanguageProficiency = use('App/Models/LanguageProficiency')
const Scholarship = use('App/Models/Scholarship')
const ResumeLanguage = use('App/Models/ResumeLanguage')
const WorkSpace = use('App/Models/WorkSpace')
const Job = use('App/Models/Job')
const SeekerBasic = use('App/Models/SeekerBasic')
const SeekerAcademic = use('App/Models/SeekerAcademic')
const SeekerCategory = use('App/Models/SeekerCategory')
const SeekerSkill = use('App/Models/SeekerSkill')
const SeekerProfessionalExperience = use('App/Models/SeekerProfessionalExperience')
const Database = use('Database');
const UserEmployer = use('App/Models/UserEmployer')
class User extends Model {

  setUserId(user_id) {
    return user_id
  }

  static boot() {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
    /**
     * A hook to hash the user password before saving
     * it to the database.
     */

    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })


    //Soft delete activo por defecto
    this.addHook('beforeCreate', async (userInstance) => {
      userInstance.deleted_at = null
    })
  }

  static scopeActive(query) {
    return query.where('active', true)
  }


  static get computed() {
    return ['fullname','avatar']
  }

  getAvatar() {
    return this.buildAvatar(this.id);
  }

  buildAvatar(id) {
    const path = Env.get('SERVER', 'http://localhost:3333/');
    if (this.avatar)
      return `${path}api/v1/avatar/${id}`;
    return false;
  }
  static buildAllavatar(id,avatar)
  {
    const path = Env.get('SERVER', 'http://localhost:3333/');
    console.log("id",id);
    console.log("avatar",avatar);
    
    if (avatar)
      return `${path}api/v1/avatar/${id}`;
    return false;
  }



  getFullname({
    first_name,
    middle_name,
    last_name
  }) {
    return `${first_name}  ${middle_name}  ${last_name}`
  }

  async confirmToken() {
    try {
      let tokenData = await this.getToken();
      const token = await this.userRegisters().create({
        'token_data': tokenData
      });
      return token.token_data;
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async getToken() {
    let tokenData = Payload.uuidv4();
    tokenData = await Hash.make(tokenData);
    tokenData = Encryption.encrypt(tokenData)
    tokenData = tokenData.substring(0, 100);
    tokenData = tokenData.split("/").join("");
    return tokenData;
  }

  async generateToken() {
    try {
      let tokenData = await this.getToken();
      const token = await this.tokenResets().create({
        'token_data': tokenData
      });
      return token.token_data;
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  static get hidden() {
    return ['password']
  }

  static get visible() {
    return ['id', 'username', 'fullname', 'first_name', 'avatar', 'last_name', 'middle_name', 'email', 'active', 'role']
  }


  static get register() {
    return ['username', 'fullname', 'first_name', 'last_name', 'middle_name', 'email', 'password']
  }




  editProfile() {
    const {
      id,
      username,
      email,
      first_name,
      last_name,
      middle_name,
      role
    } = this;
    return {
      id: id,
      username: username,
      email: email,
      first_name: first_name,
      last_name: last_name,
      middle_name: middle_name,
      description: this.getDescription(role),
      role: role
    };
  }

  static async admins(request) {
    const _roles = Roles.all();
    const exclude = [_roles.ADMIN];

    let {
      search,
      order,
      actives
    } = request.all();
    search = search ? search : '';
    order = order ? order : 'asc';

    let users = await this.query()
      .whereIn('role', exclude)
      .where(function () {
        if (actives)
          this.where('active', actives)
      })
      .where(function () {
        this.where('first_name', 'like', '%' + search + '%')
          .orWhere('last_name', 'like', '%' + search + '%')
          .orWhere('middle_name', 'like', '%' + search + '%')
          .orWhere('username', 'like', '%' + search + '%')
      })
      .orderBy('username', order)
      .fetch()
    return await users.rows.map(user => {
      return {
        id: user.id,
        avatar: this.buildAllavatar(user.id,user.avatar),
        username: user.username,
        active: user.active,
        fullname: user.fullname(),
      }
    });
  }

  changeActive() {
    this.active = this.active === 1 ? 0 : 1;
  }

  async dashboard() {
    const {
      role,
      avatar,
      email
    } = this;
    const userRegister = await this.userRegisters().fetch();
    const indicators = await this.perRol();

    return {
      fullname: this.fullname(),
      description: this.getDescription(role),
      role: role,
      avatar: this.buildAvatar(this.id),
      email: email,
      indicators: indicators,
      userRegister: userRegister
    };
  }

  async timelinePerRol() {
    switch (this.role) {
      case 'seeker':
        const basic = await this.seekerBasic().fetch() || false;
        const location = await this.seekerLocation().fetch() || false;
        const academic = await this.seekerAcademic().fetch() || false;
        let categories = await this.seekerCategories().fetch() || false;
        let skills = await this.seekerSkills().fetch() || false;
        let languages = await this.seekerLanguages().fetch() || false;
        const professionalExperiences = await this.seekerProfessionalExperiences().fetch() || false;


        //Resources
        let locationResources = {};
        let academicResources = {};
        let categoryResources = {};
        let languagesResources = {};

        if (academic)
          academicResources = await academic._scholarship() || {};
        if (location)
          locationResources = await location._city() || {};

        //obtiene las categorias
        if (categories.rows.length !== 0) {
          categoryResources = [];
          for (let index = 0; index < categories.rows.length; index++) {
            const category = categories.rows[index];
            let categoryName = await category._category();

            categoryResources.push(Object.assign(category, categoryName));
          }
          categories = categoryResources;
        }

        //Obtiene los lenguajes
        if (languages.rows.length !== 0) {
          languagesResources = [];
          for (let index = 0; index < languages.rows.length; index++) {
            const language = languages.rows[index];
            let resumeLanguageName = await language._resume_language();
            let languageProficiencyName = await language._language_proficiency();
            const resume = Object.assign(language, resumeLanguageName);
            const proficiency = Object.assign(resume, languageProficiencyName);
            languagesResources.push(proficiency);
          }
          languages = languagesResources;
        }

        return {
          'basic': basic,
          'location': Object.assign(location, locationResources),
            'academic': Object.assign(academic, academicResources),
            'category': categories,
            'skills': skills,
            'languages': languages,
            'professionalExperiences': professionalExperiences,
        }
        break;

      case 'admin':

        const jobs = await this.jobs().fetch();
        const reasons = await this.usersReasons().fetch();
        let reasonsMerge = [];
        for (let index = 0; index < reasons.rows.length; index++) {
          const reason = reasons.rows[index];
          const author = await reason.created().fetch();
          reasonsMerge.push({
            id: reason.id,
            avatar: author.avatar,
            reason: reason.reason,
            actual: reason.actual,
            author: author.fullname(),
            date: Service.humanDate(reason.created_at)
          });
        }


        return {
          jobs: jobs,
            reasons: reasonsMerge,
        }
        default:
          return [];
    }
  }

  async perRol() {
    /**
     * Bussines logic to show indicators
     */
    switch (this.role) {
      case 'admin':
      case 'god':
        const categories = await Category.getCount()
        const job_types = await JobType.getCount()
        const scholarships = await Scholarship.getCount()
        const language_proficiencies = await LanguageProficiency.getCount()
        const resume_languages = await ResumeLanguage.getCount()
        const work_spaces = await WorkSpace.getCount()

        const jobs = await await Database.table('jobs')
          .limit(5)
          .where('active', 1)
          .orderBy('start_date', 'desc')

        const employers = await await Database.table('user_employers')
          .limit(5)
          .where('active', 1)
          .orderBy('id', 'desc')


        return {
          'categories': categories,
          'job_types': job_types,
          'scholarships': scholarships,
          'language_proficiencies': language_proficiencies,
          'resume_languages': resume_languages,
          'work_spaces': work_spaces,
          jobs: jobs.map((j) => {
              return {
                ago: Service.agoTime(j.start_date),
                title: j.title
              }
            }),
            employers: employers.map((e) => {
              return {
                logo: UserEmployer.logo(e),
                name: e.company_name
              }
            }),
        }
        break;
      default:
        return [];
    }
  }

  async profile() {
    const {
      role,
      avatar,
      email,
      username
    } = this;
    const timeline = await this.timelinePerRol()
    return {
      fullname: this.fullname(),
      description: this.getDescription(role),
      email: email,
      role: role,
      username: username,
      avatar: this.buildAvatar(this.id),
      timeline: timeline,
    };
  }

  fullname() {
    return `${this.first_name} ${this.middle_name} ${this.last_name}`;
  }


  get full() {
    return `${this.first_name} ${this.middle_name} ${this.last_name}`;
  }

  roles() {
    return Roles.all();
  }

  getDescription(rol) {
    return Roles.getRole(rol)
  }

  toS() {
    return this.username;
  }
  /**
   *Genera un usuario con el nombre del correo se supone que no se debe repetir
   * @memberof User
   */
  generateUsername() {
    let nameEmail = `${this.email.substring(0, this.email.lastIndexOf("@"))}_${new Date().getUTCMilliseconds()}`;

    this.username = `${nameEmail}`;
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }

  categories() {
    return this.hasMany('App/Models/Category')
  }

  tokenResets() {
    return this.hasMany('App/Models/TokenReset')
  }

  userRegisters() {
    return this.hasOne('App/Models/UserRegister')
  }

  usersReasons() {
    return this.hasMany('App/Models/UsersReason')
  }

  seekerBasic() {
    return this.hasOne('App/Models/SeekerBasic')
  }
  seekerLocation() {
    return this.hasOne('App/Models/SeekerLocation')
  }
  seekerAcademic() {
    return this.hasOne('App/Models/SeekerAcademic')
  }

  userEmployer() {
    return this.hasOne('App/Models/UserEmployer')
  }


  jobs() {
    return this.hasMany('App/Models/Job', 'id', 'created_by')
  }


  postulations() {
    return this.hasMany('App/Models/JobsPostulation')
  }


  seekerCategories() {
    return this.hasMany('App/Models/SeekerCategory')
  }

  seekerSkills() {
    return this.hasMany('App/Models/SeekerSkill')
  }
  seekerLanguages() {
    return this.hasMany('App/Models/SeekerLanguage')
  }

  seekerProfessionalExperiences() {
    return this.hasMany('App/Models/SeekerProfessionalExperience')
  }

}

module.exports = User
