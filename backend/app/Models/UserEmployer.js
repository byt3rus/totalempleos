'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')
class UserEmployer extends Model {

  static boot () {
    super.boot()
    this.addHook('afterPaginate', 'UserEmployerHook.setLocation')
    this.addHook('afterFind', 'UserEmployerHook.findLocation')
  }

  static get hidden() {
    return ['facebook_url','linkedin_url']
  }


  static get computed () {
    return ['avatar','location','contact']
  }


  getContact({ mobile_contact }) {
    return `${mobile_contact} ${this.email}`
  }


  getLocation({}) {
    return `${this._location} ${this.zipcode}`;
  }

  
  getAvatar({ company_logo }) {
    const path = Env.get('SERVER', 'http://localhost:3333/');
    if (this.company_logo)
      return `${path}api/v1/company_logo/${this.id}`;
    return false;
  }

  static logo(model) {
    const path = Env.get('SERVER', 'http://localhost:3333/');
    if (model.company_logo)
      return `${path}api/v1/company_logo/${model.id}`;
    return false;
  }
  static get visible() {
    return [ 'avatar','id','latitude','longitude', 'linkedin_url','facebook_url','company_name', 'location','contact','active','updated_at','created_at','company_description']
  }

  static get display() {
    return [ 'id','company_name',]
  }


  static scopeActive(query) {
    return query.where('active', true)
  }
  /**
   *Solamente cuando se registra un merge de formularios
   *
   * @readonly
   * @static
   * @memberof UserEmployer
   */
  static get register() {
    return [
      'company_name',
      'company_description',
      'mobile_contact',
      'city_id',
      'state_id',
      'zipcode',
      'phone',
      'ext',
      'latitude',
      'longitude',
      'facebook_url',
      'linkedin_url',
    ]
  }

  changeActive() {
    this.active = this.active === 1 ? 0 : 1;
  }

  toS ()
  {
    return `${this.user.full}`;
  }

  created() {
    return this.belongsTo('App/Models/User', 'created_by', 'id')
  }

  updated() {
    return this.belongsTo('App/Models/User', 'updated_by', 'id')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }
  city() {
    return this.belongsTo('App/Models/City')
  }
  state() {
    return this.belongsTo('App/Models/State')
  }
}

module.exports = UserEmployer
