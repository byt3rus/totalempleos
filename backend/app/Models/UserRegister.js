'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserRegister extends Model {
    static boot () {
        super.boot()
    
        //Activo por defecto
        this.addGlobalScope(function (builder) {
          builder.where('active', 1)
        })
      }
       
      static get visible () {
        return ['id','complete','confirmation','skip']
      }
    
      user() {
        return this.belongsTo('App/Models/User')
      }

  skipChange()
  {
    this.skip = true;
  }
}

module.exports = UserRegister
