'use strict'

const Model = use('Model')

class UsersReason extends Model {
  static boot () {
    super.boot()
  }

  static get visible () {
    return ['id','reason','user_id']
  }

 
  toS ()
  {
    return this.reason;
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  created() {
    return this.belongsTo('App/Models/User','created_by','id')
  }
}

module.exports = UsersReason
