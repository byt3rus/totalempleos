'use strict'
/**
 * Espacios de trabajo
 */
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class WorkSpace extends Model {
    static boot () {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
        //Soft delete activo por defecto
        this.addHook('beforeCreate', async (modelInstance) => {
            modelInstance.deleted_at = null;
        })
    
      }

      static scopeActives (query) {
        return query.where('active', 1)
      }
      static get visible () {
        return ['id','title','description','created_at','updated_at','active']
      }

      async show()
    {
        const {id,title,description,created_at,active} = this;
        const updatedUser = await this.updated().fetch()
        return {
        id,
        title,
        description,
        active,
        created_at,
        updated: updatedUser,
        };
    }


  changeActive()
  {
    this.active = this.active===1 ? 0 : 1;
  }

  toS ()
  {
    return this.title;
  }

  created() {
    return this.belongsTo('App/Models/User','created_by','id')
  }
  
  updated() {
    return this.belongsTo('App/Models/User','updated_by','id')
  }
}

module.exports = WorkSpace
