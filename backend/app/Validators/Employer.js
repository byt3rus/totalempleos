'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');


class Employer {

  get sanitizationRules () {
    return {
      mobile_contact: 'strip_tags',
      facebook_url: 'strip_tags',
      linkedin_url: 'strip_tags',
      zipcode: 'strip_tags',
      phone: 'strip_tags',
    }
  }

  get validateAll () {
    return true;
}

  async fails (errorMessages) {
    const { response } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules () {
    return {

      company_name: 'required|min:5|max:255',
      company_description: 'required|min:10|max:255',
      mobile_contact: 'required|min:10|max:50',
      
      city_id: 'required',
      state_id: 'required',
      zipcode: 'max:10',
      phone: 'max:20',
      ext: 'max:5',

      linkedin_url: 'url',
      facebook_url: 'url',
        
    }
  }
  get messages () {
    return {

      'facebook_url.url': "El campo "+Antl.formatMessage('fields.facebook_url')+" tiene un formato inválido.",
      'linkedin_url.url': "El campo "+Antl.formatMessage('fields.linkedin_url')+" tiene un formato inválido.",

      'zipcode.max': "El campo "+Antl.formatMessage('fields.zipcode')+" es demasiado largo.",
      'phone.max': "El campo "+Antl.formatMessage('fields.phone')+" es demasiado largo.",
      'ext.max': "El campo "+Antl.formatMessage('fields.ext')+" es demasiado largo.",

      'city_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.city_id')
      }),
      'state_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.state_id')
      }),

      'mobile_contact.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.mobile_contact')
      }),
      'mobile_contact.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.mobile_contact'),
        min: 10
      }),
      'mobile_contact.max': "El campo "+Antl.formatMessage('fields.mobile_contact')+" es demasiado largo.",
      'company_name.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.company_name')
      }),
      'company_name.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.company_name'),
        min: 5
      }),
      'company_name.max': "El campo "+Antl.formatMessage('fields.company_name')+" es demasiado largo.",
      
      'company_description.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.company_description')
      }),
      'company_description.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.company_description'),
        min: 10
      }),
      'company_description.max': "El campo "+Antl.formatMessage('fields.company_description')+" es demasiado largo.",
      

    
    }
  }
}

module.exports = Employer
