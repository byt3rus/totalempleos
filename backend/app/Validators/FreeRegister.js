'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');


class FreeRegister {

  get sanitizationRules() {
    return {
      first_name: 'strip_tags',
      last_name: 'strip_tags',
      middle_name: 'strip_tags',
    }
  }

  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      username: `unique:users`,
      password: 'required|min:8|max:255',
      email: `required|email|unique:users`,
      first_name: 'required|min:3|max:120',
      last_name: 'required|min:3|max:120',
      middle_name: 'required|min:3|max:120',
    }
  }
  get messages() {
    return {
      'username.unique': Antl.formatMessage('validations.unique', {
        field: Antl.formatMessage('fields.username')
      }),
      'password.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.password')
      }),
      'password.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.password'),min:8
      }), 
      'email.email': Antl.formatMessage('validations.email', {
        field: Antl.formatMessage('fields.email')
      }), 
      'email.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.email')
      }), 
      'email.unique': Antl.formatMessage('validations.unique', {
        field: Antl.formatMessage('fields.email')
      }), 

      'first_name.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.first_name')
      }),
      'last_name.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.last_name')
      }),
      'middle_name.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.middle_name')
      }),

      'password.max': "El campo "+Antl.formatMessage('fields.password')+" es demasiado largo.",
      'first_name.max': "El campo "+Antl.formatMessage('fields.first_name')+" es demasiado largo.",
      'last_name.max': "El campo "+Antl.formatMessage('fields.last_name')+" es demasiado largo.",
      'middle_name.max': "El campo "+Antl.formatMessage('fields.middle_name')+" es demasiado largo.",

      'password.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.password'),
        min: 8
      }),

      'first_name.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.first_name'),
        min: 3
      }),

      'last_name.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.last_name'),
        min: 3
      }),

      'middle_name.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.middle_name'),
        min: 3
      }),

    }
  }
}

module.exports = FreeRegister
