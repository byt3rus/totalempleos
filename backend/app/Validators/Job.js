'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');

class Job {
  get sanitizationRules () {
    return {
      title: 'strip_tags',
      description: 'strip_tags',
    }
  }

  get validateAll () {
    return true;
}

  async fails (errorMessages) {
    const { response } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules () {
    const request = this.ctx.request.only(['start_date','end_time']);
    const starDate = request.start_date;
    const endDate = request.end_time;
    
    return {
      title: 'required|min:5|max:150',
      description: 'required|min:10|max:1500',
      user_employer_id: 'required',
      job_type_id: 'required',
      work_space_id: 'required',
      start_date: ' required|date|after:1950-01-01,',
      end_time: ` required|date|after:${starDate},`,
      city_id: 'required',
      state_id: 'required',
      
    }
  }
  get messages () {
    return {
      'city_id.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.city_id') }),
      'state_id.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.state_id') }),
      
      
     

      'start_date.after': Antl.formatMessage('fields.start_date')+' Debe ser una fecha superior',

      'start_date.date': Antl.formatMessage('validations.date', {
        field: Antl.formatMessage('fields.start_date')
      }),

      'end_time.after': Antl.formatMessage('fields.end_time')+' Debe ser una fecha superior',

      'end_time.date': Antl.formatMessage('validations.date', {
        field: Antl.formatMessage('fields.end_time')
      }),
      
      
      'end_time.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.end_time') }),
      'start_date.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.start_date') }),
     
      'work_space_id.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.work_space_id') }),
      'job_type_id.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.job_type_id') }),
      'user_employer_id.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.user_employer_id') }),

      'title.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.title') }),
      'title.min' : Antl.formatMessage('validations.min',{field:Antl.formatMessage('fields.title'),min:5 }),
      'title.max': "El campo "+Antl.formatMessage('fields.title')+" es demasiado largo.",

      'description.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.description') }),
      'description.min' : Antl.formatMessage('validations.min',{field:Antl.formatMessage('fields.description'),min:10 }),
      'description.max': "El campo "+Antl.formatMessage('fields.description')+" es demasiado largo.",


    }
  }
}

module.exports = Job
