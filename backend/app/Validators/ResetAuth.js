'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');


class ResetAuth {

  get sanitizationRules () {
    return {
      email: 'normalize_email',
    }
  }

  get validateAll () {
    return true;
}

  async fails (errorMessages) {
    const { response } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules () {
    return {
      email: 'required|email'
    }
  }
  get messages () {
    return Antl.list('validations')
  }
}

module.exports = ResetAuth
