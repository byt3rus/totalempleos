'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const User = use('App/Models/User')

class SetProfile {

  get sanitizationRules () {
    return {
      username: 'trim',
      email: 'normalize_email',
    }
  }

  get validateAll () {
    return true;
}

  async fails (errorMessages) {
    const { response } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

   get rules () {
    const {id,email} = this.ctx.request.all();
    return {
      email:  `required|email|unique:users,email,id,${id}`,
      username: `required|unique:users,username,id,${id}`,
      first_name: 'required',
      last_name: 'required',
      middle_name: 'required',
    }
  }

  get messages () {
    return {
      'email.email' : Antl.formatMessage('validations.email',{field:Antl.formatMessage('fields.email') }),
      'email.required'    : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.email') }),
      'username.unique'    : Antl.formatMessage('validations.unique',{field:Antl.formatMessage('fields.username') }),
      'username.alpha_numeric'    : Antl.formatMessage('validations.alpha_numeric',{field:Antl.formatMessage('fields.username') }),
      'email.unique'    : Antl.formatMessage('validations.unique',{field:Antl.formatMessage('fields.email') }),
      'first_name.required'    : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.first_name') }),
      'last_name.required'    : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.last_name') }),
      'middle_name.required'    : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.middle_name') })
    }
  }
}

module.exports = SetProfile
