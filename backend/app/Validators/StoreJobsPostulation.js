'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
class StoreJobsPostulation {
 

  get validateAll () {
    return true;
}

  async fails (errorMessages) {
    const { response } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

   get rules () {
    return {
      job_id: `required`,
    }
  }

  get messages () {
    return {
      'job_id.required'    : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.job_id') }),
    }
  }
}

module.exports = StoreJobsPostulation
