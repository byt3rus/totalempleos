'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');

class StoreSeekerAcademic {
  get sanitizationRules() {
    return {
      school_name: 'strip_tags',
      expected_salary: 'strip_tags',
      linked_in: 'strip_tags',
      facebook: 'strip_tags',
    }
  }

  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      scholarship_id: 'required',
      school_name: 'required|min:5|max:150',
      expected_salary: 'max:255',
      linked_in: 'max:150',
      facebook: 'max:150',
    }
  }
  get messages() {
    return {
      'scholarship_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.scholarship_id')
      }),
      'school_name.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.school_name')
      }),
      'school_name.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.school_name'),
        min: 5
      }),
      'school_name.max': "El campo "+Antl.formatMessage('fields.school_name')+" es demasiado largo.",
      'expected_salary.max': "El campo "+Antl.formatMessage('fields.expected_salary')+" es demasiado largo.",
      'linked_in.max': "El campo "+Antl.formatMessage('fields.linked_in')+" es demasiado largo.",
      'facebook.max': "El campo "+Antl.formatMessage('fields.facebook')+" es demasiado largo.",

      

    }
  }
}

module.exports = StoreSeekerAcademic
