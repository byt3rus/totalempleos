'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');


class StoreSeekerBasic {
  get sanitizationRules() {
    return {
      title: 'strip_tags',
      sex: 'strip_tags',
      nationality: 'strip_tags',
      
    }
  }

  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      title: 'required|min:5|max:100',
      birthday: 'required|date|after:1950-01-01|before:2005-01-01',
      sex: 'required',
      nationality: 'required',
    }
  }
  get messages() {
    return {
      'title.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.title')
      }),
      'title.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.title'),
        min: 5
      }),
      'title.max': "El campo "+Antl.formatMessage('fields.title')+" es demasiado largo.",
      'birthday.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.birthday')
      }),

      'birthday.before': 'Debe ser una fecha anterior',

      'birthday.after': 'Debe ser una fecha superior',

      'birthday.date': Antl.formatMessage('validations.date', {
        field: Antl.formatMessage('fields.birthday')
      }),

      'sex.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.sex')
      }),

      'nationality.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.nationality')
      }),
      

    }
  }
}

module.exports = StoreSeekerBasic
