'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
class StoreSeekerCategory {

  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      category_id: 'required',
    }
  }
  get messages() {
    return {
      'category_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.category_id')
      }),
    }
  }
}

module.exports = StoreSeekerCategory
