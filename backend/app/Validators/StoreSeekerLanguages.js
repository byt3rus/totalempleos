'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');

class StoreSeekerLanguages {
  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      resume_language_id: 'required',
      language_proficiency_id: 'required',
    }
  }
  get messages() {
    return {
      'resume_language_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.resume_language_id')
      }),
      'language_proficiency_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.language_proficiency_id')
      }),
    }
  }
}

module.exports = StoreSeekerLanguages
