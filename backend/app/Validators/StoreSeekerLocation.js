'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
class StoreSeekerLocation {
  get sanitizationRules() {
    return {
      neighborhood: 'strip_tags',
      street: 'strip_tags',
      number_ext: 'strip_tags',
      zip_code: 'strip_tags',
      phone: 'strip_tags',
      phone_extra: 'strip_tags',
      mobile: 'strip_tags',
      website: 'strip_tags', 
    }
  }

  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      city_id: 'required',
      state_id: 'required',
      neighborhood: 'required|min:5|max:150',
      street: 'max:150',
      number_ext: 'max:20',
      zip_code: 'required|max:10',
      phone: 'required|max:50',
      mobile: 'required|max:50',
      website: 'max:150',
    }
  }
  get messages() {
    return {
      'city_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.city_id')
      }),
      'state_id.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.state_id')
      }),
      
      'neighborhood.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.neighborhood')
      }),
      'neighborhood.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.neighborhood'),
        min: 5
      }),
      'neighborhood.max': "El campo "+Antl.formatMessage('fields.neighborhood')+" es demasiado largo.",
      
      'street.max': "El campo "+Antl.formatMessage('fields.street')+" es demasiado largo.",
      'website.max': "El campo "+Antl.formatMessage('fields.website')+" es demasiado largo.",
      'number_ext.max': "El campo "+Antl.formatMessage('fields.number_ext')+" es demasiado largo.",
      
      'zip_code.max': "El campo "+Antl.formatMessage('fields.zip_code')+" es demasiado largo.",
      'zip_code.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.zip_code')
      }),

      'mobile.max': "El campo "+Antl.formatMessage('fields.mobile')+" es demasiado largo.",
      'mobile.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.mobile')
      }),

      'phone.max': "El campo "+Antl.formatMessage('fields.phone')+" es demasiado largo.",
      'phone.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.phone')
      }),


    }
  }
}

module.exports = StoreSeekerLocation
