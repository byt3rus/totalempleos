'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
const { validate, rule } = use("Validator"); 

class StoreSeekerProfessionalExperiencies {
  get sanitizationRules() {
    return {
      job: 'strip_tags',
      company: 'strip_tags',
      activities_achievements: 'strip_tags',
      start_month: 'strip_tags',
      start_year: 'strip_tags',
      end_month: 'strip_tags',
      end_year: 'strip_tags', 
    }
  }

  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      job: 'required|min:6|max:150',
      company: 'required|min:4|max:150',
      activities_achievements: 'required|min:20|max:255',
      start_month: [ rule( "regex", /^0[1-9]|1[0-2]$/i ),rule("required"), ] ,
      end_month: [ rule( "regex", /^0[1-9]|1[0-2]$/i ),rule("required"), ] ,
      start_year: [ rule( "regex", /^(19|20)\d{2}$/i ),rule("required"), ] ,
      end_year: [ rule( "regex", /^(19|20)\d{2}$/i ),rule("required"), ] ,
    }
  }
  get messages() {
    return {
      'job.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.job')
      }),
      'job.max': "El campo "+Antl.formatMessage('fields.job')+" es demasiado largo.",
      'job.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.job'),
        min: 6
      }),

      'company.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.company')
      }),
      'company.max': "El campo "+Antl.formatMessage('fields.company')+" es demasiado largo.",
      'company.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.company'),
        min: 4
      }),
      'activities_achievements.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.activities_achievements')
      }),
      'activities_achievements.max': "El campo "+Antl.formatMessage('fields.activities_achievements')+" es demasiado largo.",
      'activities_achievements.min': Antl.formatMessage('validations.min', {
        field: Antl.formatMessage('fields.activities_achievements'),
        min: 20
      }),

      'start_month.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.start_month')
      }),
      'start_month.regex': "El campo "+Antl.formatMessage('fields.start_month')+" tiene un formato inválido.",

      'end_month.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.end_month')
      }),
      'end_month.regex': "El campo "+Antl.formatMessage('fields.end_month')+" tiene un formato inválido.",
      
      'start_year.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.start_year')
      }),
      'start_year.regex': "El campo "+Antl.formatMessage('fields.start_year')+" tiene un formato inválido.",
      

      'end_year.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.end_year')
      }),
      'end_year.regex': "El campo "+Antl.formatMessage('fields.end_year')+" tiene un formato inválido.",
      

    }
  }
}

module.exports = StoreSeekerProfessionalExperiencies
