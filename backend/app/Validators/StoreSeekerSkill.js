'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');

class StoreSeekerSkill {

  get sanitizationRules() {
    return {
      skills: 'strip_tags',
    }
  }

  get validateAll() {
    return true;
  }

  async fails(errorMessages) {
    const {
      response
    } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules() {
    return {
      skills: 'required',
    }
  }
  get messages() {
    return {
      'skills.required': Antl.formatMessage('validations.required', {
        field: Antl.formatMessage('fields.skills')
      }),
    }
  }
}

module.exports = StoreSeekerSkill
