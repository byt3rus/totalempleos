'use strict'
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl');
class WorkSpace {
  get sanitizationRules () {
    return {
      title: 'strip_tags',
    }
  }

  get validateAll () {
    return true;
}

  async fails (errorMessages) {
    const { response } = this.ctx
    return response.forbidden(Payload.error(errorMessages))
  }

  get rules () {
    const workSpace = this.ctx.params.id
    return {
      title: `required|min:3|max:100|unique:work_spaces,id,${workSpace}`
    }
  }
  get messages () {
    return {
      'title.required' : Antl.formatMessage('validations.required',{field:Antl.formatMessage('fields.title') }),
      'title.unique' : Antl.formatMessage('validations.unique',{field:Antl.formatMessage('fields.title') }),
      'title.min' : Antl.formatMessage('validations.min',{field:Antl.formatMessage('fields.title'),min:3 }),
    }
  }
}

module.exports = WorkSpace
