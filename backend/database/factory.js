'use strict'

const Factory = use('Factory')

Factory.blueprint('App/Models/Category', async (faker) => {
  return {
    title: faker.word({ length: 15 })
  }
})