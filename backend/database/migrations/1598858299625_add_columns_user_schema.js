'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnsUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.timestamp('deleted_at').nullable()
      table.boolean('active').defaultTo(true)
      table.string('role')
      
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('deleted_at')
      table.dropColumn('active')
      table.dropColumn('role')
    })
  }
}

module.exports = AddColumnsUserSchema
