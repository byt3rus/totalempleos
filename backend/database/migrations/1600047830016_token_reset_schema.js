'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TokenResetSchema extends Schema {
  up () {
    this.create('token_resets', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('token_data','100').notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('token_resets')
  }
}

module.exports = TokenResetSchema
