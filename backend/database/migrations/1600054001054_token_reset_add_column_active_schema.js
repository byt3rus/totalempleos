'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TokenResetAddColumnActiveSchema extends Schema {
  up () {
    this.table('token_resets', (table) => {
      table.boolean('active').defaultTo(true)
    })
  }

  down () {
    this.table('token_resets', (table) => {
      table.dropColumn('active')
    })
  }
}

module.exports = TokenResetAddColumnActiveSchema
