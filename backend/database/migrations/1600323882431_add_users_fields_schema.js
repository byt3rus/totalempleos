'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUsersFieldsSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('first_name',120).notNullable()
      table.string('middle_name',120).notNullable()
      table.string('last_name',120).notNullable()
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('middle_name')
      table.dropColumn('first_name')
      table.dropColumn('last_name')
    })
  }
}

module.exports = AddUsersFieldsSchema
