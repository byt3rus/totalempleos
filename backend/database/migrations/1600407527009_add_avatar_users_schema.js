'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAvatarUsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('avatar').nullable();
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('avatar')
    })
  }
}

module.exports = AddAvatarUsersSchema
