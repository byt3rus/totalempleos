'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersAddUpdatedBySchema extends Schema {
  up () {
    this.table('categories', (table) => {
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('updated_by').unsigned().references('id').inTable('users').nullable()
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('created_by')
      table.dropColumn('updated_by')
    })
  }
}

module.exports = UsersAddUpdatedBySchema
