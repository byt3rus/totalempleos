'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoriesAddActiveSchema extends Schema {
  up () {
    this.table('categories', (table) => {
      table.boolean('active').defaultTo(true)
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('active')
    })
  }
}

module.exports = CategoriesAddActiveSchema
