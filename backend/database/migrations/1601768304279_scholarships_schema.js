'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ScholarshipsSchema extends Schema {
  up () {
    this.create('scholarships', (table) => {
      table.increments()
      table.string('title',120).notNullable().unique().index()
      table.text('description','longtext')
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('updated_by').unsigned().references('id').inTable('users').nullable()
      table.boolean('active').defaultTo(true)
      table.timestamp('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('scholarships')
  }
}

module.exports = ScholarshipsSchema
