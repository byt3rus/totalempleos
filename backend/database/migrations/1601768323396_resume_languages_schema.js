'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResumeLanguagesSchema extends Schema {
  up () {
    this.create('resume_languages', (table) => {
      table.increments()
      table.string('title',120).notNullable().unique().index()
      table.text('description','longtext')
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('updated_by').unsigned().references('id').inTable('users').nullable()
      table.boolean('active').defaultTo(true)
      table.timestamp('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('resume_languages')
  }
}

module.exports = ResumeLanguagesSchema
