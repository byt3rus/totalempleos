'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersReasonsSchema extends Schema {
  up () {
    this.create('users_reasons', (table) => {
      table.text('reason','longtext')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('users_reasons')
  }
}

module.exports = UsersReasonsSchema
