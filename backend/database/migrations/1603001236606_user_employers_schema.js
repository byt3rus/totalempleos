'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserEmployersSchema extends Schema {
  up () {
    this.create('user_employers', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.string('mobile_contact',50)
      table.string('email_contact',100)
      
      table.string('company_name')
      table.text('company_description','longtext')
      table.string('company_logo')

      table.integer('city_id').unsigned().references('id').inTable('cities')
      table.integer('state_id').unsigned().references('id').inTable('states')
      table.string('zipcode',10)
      table.string('phone',20)
      table.string('ext',5)
      table.string('latitude')
      table.string('longitude')

      table.string('facebook_url')
      table.string('linkedin_url')

      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('updated_by').unsigned().references('id').inTable('users').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('user_employers')
  }
}

module.exports = UserEmployersSchema
