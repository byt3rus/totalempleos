'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RemoveEmailContactEmployersSchema extends Schema {
  up () {
    this.table('user_employers', (table) => {
      table.dropColumn('email_contact')
    })
  }

  down () {
    this.table('user_employers', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RemoveEmailContactEmployersSchema
