'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserRegistersSchema extends Schema {
  up () {
    this.create('user_registers', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.boolean('confirmation').defaultTo(false)
      table.boolean('complete').defaultTo(false)
      table.boolean('skip').defaultTo(false)
      table.boolean('active').defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('user_registers')
  }
}

module.exports = UserRegistersSchema
