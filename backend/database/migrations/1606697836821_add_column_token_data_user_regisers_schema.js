'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnTokenDataUserRegisersSchema extends Schema {
  up () {
    this.table('user_registers', (table) => {
      table.string('token_data','100').notNullable().unique()
    })
  }

  down () {
    this.table('user_registers', (table) => {
      table.dropColumn('token_data')
    })
  }
}

module.exports = AddColumnTokenDataUserRegisersSchema
