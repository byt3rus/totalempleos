'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeekerBasicSchema extends Schema {
  up () {
    this.create('seeker_basics', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('title',100).notNullable()
      table.date('birthday').notNullable()
      table.string('sex',50).notNullable()
      table.string('nationality').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('seeker_basics')
  }
}

module.exports = SeekerBasicSchema
