'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeekerLocationSchema extends Schema {
  up () {
    this.create('seeker_locations', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('city_id').unsigned().references('id').inTable('cities')
      table.string('neighborhood',150)
      table.string('street',150)
      table.string('number_ext',20)
      table.string('zip_code',10)
      table.string('phone',50)
      table.string('phone_extra',50)
      table.string('mobile',50)
      table.string('website',150)
      table.boolean('availability').defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('seeker_locations')
  }
}

module.exports = SeekerLocationSchema
