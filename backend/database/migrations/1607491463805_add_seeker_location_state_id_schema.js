'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddSeekerLocationStateIdSchema extends Schema {
  up () {
    this.table('seeker_locations', (table) => {
      table.integer('state_id').unsigned().references('id').inTable('states')
    })
  }

  down () {
    this.table('seeker_locations', (table) => {
      table.dropColumn('state_id')
    })
  }
}

module.exports = AddSeekerLocationStateIdSchema
