'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateSeekerAcademicsSchema extends Schema {
  up () {
    this.create('seeker_academics', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('scholarship_id').unsigned().references('id').inTable('scholarships')
      table.string('school_name',150).notNullable()//5
      table.string('expected_salary')
      table.string('linked_in',150)
      table.string('facebook',150)
      table.string('cv')
      table.timestamps()
    })
  }

  down () {
    this.drop('seeker_academics')
  }
}

module.exports = CreateSeekerAcademicsSchema
