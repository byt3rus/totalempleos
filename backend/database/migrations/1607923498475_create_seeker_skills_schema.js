'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateSeekerSkillsSchema extends Schema {
  up () {
    this.create('seeker_skills', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('skills').notNullable()
      table.timestamp('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('seeker_skills')
  }
}

module.exports = CreateSeekerSkillsSchema
