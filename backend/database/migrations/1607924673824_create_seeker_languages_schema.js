'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateSeekerLanguagesSchema extends Schema {
  up () {
    this.create('seeker_languages', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('resume_language_id').unsigned().references('id').inTable('resume_languages')
      table.integer('language_proficiency_id').unsigned().references('id').inTable('language_proficiencies')
      table.timestamp('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('seeker_languages')
  }
}

module.exports = CreateSeekerLanguagesSchema
