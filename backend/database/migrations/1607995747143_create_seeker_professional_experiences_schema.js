'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateSeekerProfessionalExperiencesSchema extends Schema {
  up () {
    this.create('seeker_professional_experiences', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('job',150).notNullable()//5
      table.string('company',150).notNullable()//5
      table.string('activities_achievements').notNullable()//5
      table.string('start_month',2).notNullable()//5
      table.string('start_year',4).notNullable()//5
      table.string('end_month',2).notNullable()//5
      table.string('end_year',4).notNullable()//5
      table.boolean('current').defaultTo(false)
      table.timestamp('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('seeker_professional_experiences')
  }
}

module.exports = CreateSeekerProfessionalExperiencesSchema
