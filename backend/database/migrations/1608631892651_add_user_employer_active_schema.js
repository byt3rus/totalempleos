'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUserEmployerActiveSchema extends Schema {
  up () {
    this.table('user_employers', (table) => {
      table.boolean('active').defaultTo(true)
    })
  }

  down () {
    this.table('user_employers', (table) => {
      // reverse alternations
      table.dropColumn('active')
    })
  }
}

module.exports = AddUserEmployerActiveSchema
