'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateJobsSchema extends Schema {
  up () {
    this.create('jobs', (table) => {
      table.increments()
      table.string('title',150).notNullable()
      table.text('description').notNullable()
      table.integer('user_employer_id').unsigned().references('id').inTable('user_employers')
      table.integer('job_type_id').unsigned().references('id').inTable('job_types')
      table.integer('work_space_id').unsigned().references('id').inTable('work_spaces')
      table.boolean('confidencial').defaultTo(false).notNullable()
      table.boolean('active').defaultTo(true).notNullable()
      table.boolean('show_salary').defaultTo(false).notNullable()
      table.integer('seen').unsigned().defaultTo(0).notNullable()
      table.integer('salary').unsigned().defaultTo(0)
      table.date('start_date').notNullable()
      table.date('end_time').notNullable()
      table.integer('city_id').unsigned().references('id').inTable('cities')
      table.integer('state_id').unsigned().references('id').inTable('states')
      table.string('zipcode',10)
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('updated_by').unsigned().references('id').inTable('users').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('jobs')
  }
}

module.exports = CreateJobsSchema
