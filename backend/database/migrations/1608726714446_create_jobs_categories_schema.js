'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateJobsCategoriesSchema extends Schema {
  up () {
    this.create('jobs_categories', (table) => {
      table.increments()
      table.integer('job_id').unsigned().references('id').inTable('jobs')
      table.integer('category_id').unsigned().references('id').inTable('categories')
      table.timestamp('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('jobs_categories')
  }
}

module.exports = CreateJobsCategoriesSchema
