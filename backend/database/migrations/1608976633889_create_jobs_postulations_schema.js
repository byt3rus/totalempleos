'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateJobsPostulationsSchema extends Schema {
  up () {
    this.create('jobs_postulations', (table) => {
      table.increments()
      table.integer('job_id').unsigned().references('id').inTable('jobs').notNullable()
      table.integer('user_id').unsigned().references('id').inTable('users').notNullable()
      table.boolean('seen').defaultTo(false).notNullable()
      table.timestamp('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('jobs_postulations')
  }
}

module.exports = CreateJobsPostulationsSchema
