'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterUsersReasonsSchema extends Schema {
  up () {
    this.table('users_reasons', (table) => {
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.boolean('actual').notNullable()
    })
  }

  down () {
    this.table('users_reasons', (table) => {
      table.dropColumn('created_by')
      table.dropColumn('actual')
    })
  }
}

module.exports = AlterUsersReasonsSchema
