'use strict'

/*
|--------------------------------------------------------------------------
| StateSeeder
|--------------------------------------------------------------------------
|
|
*/

const User = use('App/Models/User')
const Category = use('App/Models/Category')
const LanguageProficiency = use('App/Models/LanguageProficiency')
const ResumeLanguage = use('App/Models/ResumeLanguage')
const Scholarship = use('App/Models/Scholarship')
const JobType = use('App/Models/JobType')
const WorkSpace = use('App/Models/WorkSpace')

class UserSeeder {
  async run () {
    const user = new User();
    user.username = 'jonathan';
    user.email = 'demo@demo.com';
    user.password = 'demodemo';
    user.role = 'god';
    user.first_name = 'Jonathan Guillermo';
    user.middle_name = 'Cruz';
    user.last_name = 'Palacios';
    await user.save();

    const category = new Category();
    category.title = "Administración";
    category.created_by = user.id;
    category.updated_by = user.id;
    await category.save();

    const languageProficiency = new LanguageProficiency();
    languageProficiency.title = "Avanzado";
    languageProficiency.created_by = user.id;
    languageProficiency.updated_by = user.id;
    await languageProficiency.save();

    const resumeLanguage = new ResumeLanguage();
    resumeLanguage.title = "Inglés";
    resumeLanguage.created_by = user.id;
    resumeLanguage.updated_by = user.id;
    await resumeLanguage.save();

    const scholarship = new Scholarship();
    scholarship.title = "Primaria";
    scholarship.created_by = user.id;
    scholarship.updated_by = user.id;
    await scholarship.save();

    const jobType = new JobType();
    jobType.title = "Contrato Inderteminado";
    jobType.created_by = user.id;
    jobType.updated_by = user.id;
    await jobType.save();

    const workSpace = new WorkSpace();
    workSpace.title = "Oficina";
    workSpace.created_by = user.id;
    workSpace.updated_by = user.id;
    await workSpace.save();

    

  }
}

module.exports = UserSeeder
