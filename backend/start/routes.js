'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Payload = use('App/Models/Response/Payload')
const Antl = use('Antl')
//const JobStatu = use('App/Models/Enums/JobStatu');

Route.get('/', () => {
  return Payload.success([],Antl.formatMessage('messages.app_init'))
});


Route.group(() => {

  Route.get('/', () => {
    return Payload.success([],Antl.formatMessage('messages.app_init'))
  });

  Route.get('avatar/:id', 'AuthController.avatar')
  Route.get('company_logo/:id', 'UserEmployerController.avatar')
  Route.get('document_cv/:hash', 'SeekerController.documentCv')
  //Auth
  Route.post('login', 'AuthController.login')
  Route.post('reset', 'AuthController.resetPassword')
  .validator('ResetAuth')
  Route.post('free_register', 'AuthController.freeRegister')
  .validator('FreeRegister')
  


  Route.post('employeer_validation', 'AuthController.employeerValidation')
  .validator('FreeRegister')
  .middleware(['auth','role:admin'])

  Route.post('confirm_token', 'AuthController.confirmToken')

  Route.post('check_token', 'AuthController.checkToken')
  Route.post('change_password', 'AuthController.changePassword')

  Route.post('settings', 'AuthController.settings')
  .middleware(['auth']);
  Route.get('dashboard', 'DashboardController.index')
  .middleware(['auth'])

  Route.post('profile', 'AuthController.profile')
  .middleware(['auth']);

  Route.post('skip', 'AuthController.skip')
  .middleware(['auth','role:seeker'])

  Route.post('set_profile', 'AuthController.setProfile')
  .validator('SetProfile')
  .middleware(['auth'])

  Route.post('new_admin', 'AuthController.newAdmin')
  .validator('SetProfile')
  .middleware(['auth','role:admin'])

  //Apply
  
  Route.post('jobs_postulations/apply', 'ApplyController.apply')
  .validator('StoreJobsPostulation')
  .middleware(['auth','role:seeker'])

  Route.get('jobs_postulations/me', 'ApplyController.postulations')
  .middleware(['auth'])

  Route.post('jobs_postulations/me/seen', 'ApplyController.seen')
  .middleware(['auth'])


  //Resources
  Route.get('resources/cities', 'ResourceController.cities')
  Route.get('resources/list_cities', 'ResourceController.listCities')
  Route.get('resources/states', 'ResourceController.states')
  Route.get('resources/city_state', 'ResourceController.cityState')

  

  Route.get('search', 'SearchController.index');
  Route.get('search_admin', 'SearchController.admin');

  Route.get('search/:id', 'SearchController.job')

  Route.get('resources/employers', 'ResourceController.employers')
  .middleware(['auth'])
  Route.get('resources/job_types', 'ResourceController.jobType')
  .middleware(['auth'])
  Route.get('resources/work_spaces', 'ResourceController.workSpace')
  .middleware(['auth'])

  Route.get('resources/scholarships', 'ResourceController.scholarships')
  .middleware(['auth'])

  Route.get('resources/categories_list', 'ResourceController.categoriesList')
  .middleware(['auth'])

  Route.get('resources/resume_languages_list', 'ResourceController.resumeLanguagesList')
  .middleware(['auth'])


  Route.get('resources/jobs', 'ResourceController.jobs')
  .middleware(['auth'])

  Route.get('resources/nationalities', 'ResourceController.nationalities')
  //.middleware(['auth'])
  
  //Users
  Route.get('/users_list', 'UserController.index')
  .middleware(['auth','role:admin'])

  //seekers
  Route.get('/job_seekers', 'SeekerController.list')
  .middleware(['auth','role:admin'])

  Route.post('/job_seekers/active', 'SeekerController.seeker_active')
  .middleware(['auth','role:admin'])
  
  Route.post('/seekers/basic', 'SeekerController.basic')
  .validator('StoreSeekerBasic')
  .middleware(['auth','role:seeker'])


  Route.post('/seekers/location', 'SeekerController.location')
  .validator('StoreSeekerLocation')
  .middleware(['auth','role:seeker'])

  Route.post('/seekers/academic', 'SeekerController.academic')
  .validator('StoreSeekerAcademic')
  .middleware(['auth','role:seeker'])

  Route.post('/seekers/categories', 'SeekerController.categories')
  .validator('StoreSeekerCategory')
  .middleware(['auth','role:seeker'])

  Route.post('/seekers/languages', 'SeekerController.languages')
  .validator('StoreSeekerLanguages')
  .middleware(['auth','role:seeker'])

  Route.delete('/seekers/delete_category', 'SeekerController.deleteCategory')
  .middleware(['auth','role:seeker'])

  Route.delete('/seekers/delete_language', 'SeekerController.deleteLanguage')
  .middleware(['auth','role:seeker'])

  Route.delete('/seekers/delete_skill', 'SeekerController.deleteSkill')
  .middleware(['auth','role:seeker'])

  Route.post('/seekers/skills', 'SeekerController.skills')
  .validator('StoreSeekerSkill')
  .middleware(['auth','role:seeker'])

  //@TODO
  Route.get('/seekers/cv/:id', 'SeekerController.cvPdf')
  .middleware(['auth','role:admin'])

  Route.get('/cv/:id', 'ResourceController.seeCv')
  //.middleware(['auth'])


  Route.post('/seekers/professional_experiences', 'SeekerController.professionalExperiencies')
  .validator('StoreSeekerProfessionalExperiencies')
  .middleware(['auth','role:seeker'])

  Route.delete('/seekers/delete_professional_experiences', 'SeekerController.deleteProfessionalExperiency')
  .middleware(['auth','role:seeker'])


  Route.get('/users/:id/edit', 'UserController.edit')
  .middleware(['auth','role:admin'])

  //Categories
  Route.resource('categories', 'CategoryController')
  .apiOnly().validator(new Map(
    [
      [
        ['categories.store','categories.update'],['Category']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

  Route.post('/categories/active', 'CategoryController.active')
  .middleware(['auth','role:admin'])

  //Job types
  Route.resource('job_types', 'JobTypeController')
  .apiOnly().validator(new Map(
    [
      [
        ['job_types.store','job_types.update'],['JobType']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

  Route.post('/job_types/active', 'JobTypeController.active')
  .middleware(['auth','role:admin'])

  //Scholarship
  Route.resource('scholarships', 'ScholarshipController')
  .apiOnly().validator(new Map(
    [
      [
        ['scholarships.store','scholarships.update'],['Scholarship']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

  Route.post('/scholarships/active', 'ScholarshipController.active')
  .middleware(['auth','role:admin'])

  //language_proficiencies
  Route.resource('language_proficiencies', 'LanguageProficiencyController')
  .apiOnly().validator(new Map(
    [
      [
        ['language_proficiencies.store','language_proficiencies.update'],['LanguageProficiency']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

  Route.post('/language_proficiencies/active', 'LanguageProficiencyController.active')
  .middleware(['auth','role:admin'])

  //resume_languages
  Route.resource('resume_languages', 'ResumeLanguageController')
  .apiOnly().validator(new Map(
    [
      [
        ['resume_languages.store','resume_languages.update'],['ResumeLanguage']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

  Route.post('/resume_languages/active', 'ResumeLanguageController.active')
  .middleware(['auth','role:admin'])

  //work_spaces
  Route.resource('work_spaces', 'WorkSpaceController')
  .apiOnly().validator(new Map(
    [
      [
        ['work_spaces.store','work_spaces.update'],['WorkSpace']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

  Route.post('/work_spaces/active', 'WorkSpaceController.active')
  .middleware(['auth','role:admin'])

  //admins
  Route.resource('admins', 'AdminController')
  .apiOnly().validator(new Map(
    [
      [
        ['admins.store','admins.update'],['SetProfile']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

    //jobs
    Route.resource('jobs', 'JobController')
    .apiOnly().validator(new Map(
      [
        [
          ['jobs.store','jobs.update'],['Job']
        ]
      ]
    ))
    .middleware(['auth','role:admin']);

   

    Route.post('/jobs/active', 'JobController.active')
    .middleware(['auth','role:admin'])

    Route.post('/jobs/my_jobs', 'JobController.myJobs')
    .middleware(['auth','role:admin,employer'])

  Route.post('/jobs/categories', 'JobController.categories')
  .validator('StoreJobsCategory')
  .middleware(['auth','role:seeker'])
  
  Route.post('/jobs/search', 'JobController.edit')
  .middleware(['auth','role:admin'])


  Route.post('/admins/active', 'AdminController.active')
  .middleware(['auth','role:admin'])

   Route.post('/employers/search', 'UserEmployerController.edit')
  .middleware(['auth','role:admin'])

  //admins
  Route.resource('employers', 'UserEmployerController')
  .apiOnly().validator(new Map(
    [
      [
        ['employers.store','employers.update'],['Employer']
      ]
    ]
  ))
  .middleware(['auth','role:admin']);

  

  Route.post('/employers/active', 'UserEmployerController.active')
  .middleware(['auth','role:admin'])

  Route.post('/employers/validate_user', 'UserEmployerController.validateUser')
  .validator('SetProfile')
  .middleware(['auth','role:admin'])

}).prefix('api/v1');
