import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { PolicyComponent } from './policy/policy.component';
import { TermsComponent } from './terms/terms.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { PasswordChangeComponent } from './passwordchange/passwordchange.component';

import { IndexComponent } from './index/index.component';
const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'policy',
        component: PolicyComponent
    },
    {
        path: 'terms',
        component: TermsComponent
    },
    {
        path: '',
        component: IndexComponent
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'reset-password',
        component: PasswordresetComponent
    },
    {
        path: 'change-password/:token',
        component: PasswordChangeComponent
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }
