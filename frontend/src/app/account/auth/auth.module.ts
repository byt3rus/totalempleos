import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { UIModule } from '../../shared/ui/ui.module';
import { LoginComponent } from './login/login.component';
import { PolicyComponent } from './policy/policy.component';
import { TermsComponent } from './terms/terms.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { SignupComponent } from './signup/signup.component';
import { AuthRoutingModule } from './auth-routing';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { PasswordChangeComponent } from './passwordchange/passwordchange.component';
import {IndexComponent} from './index/index.component';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  declarations: [
    IndexComponent,
    LoginComponent,
    SignupComponent, 
    PolicyComponent,
    ConfirmComponent,
    TermsComponent,
    PasswordresetComponent,
    PasswordChangeComponent,
  ],
  imports: [
    CommonModule,
    NgxLoadingModule,
    ReactiveFormsModule,
    NgbAlertModule,
    UIModule,
    AuthRoutingModule,
  ],
})
export class AuthModule { }
