import { Component, OnInit } from '@angular/core';
import * as ms from '../../../core/constants/messages';
import { UserProfileService } from '../../../core/services/user.service';
import { AlertService } from '../../../core/services/alert.service';
import {SuperService} from '../../../core/services/super.service'
import { first } from 'rxjs/operators';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
    selector: 'app-confirm',
    templateUrl: './confirm.component.html'
})

/**
 * Policy component
 */
export class ConfirmComponent implements OnInit {

    ms: any;
    loading = true;
    // tslint:disable-next-line: max-line-length
    constructor(
        private userProfileService: UserProfileService,
        private alertService: AlertService,
        private router: Router,
        private route:ActivatedRoute,
        private superService:SuperService,
    ) {
        
        this.ms = ms;
    }

    ngOnInit() {
        this.loading = true;
        this.userProfileService.confirm(this.route.snapshot.params)
            .pipe(first())
            .subscribe(
                response => {
                    //Genera la sesión de nuevo una vez confirmado.
                    const user = response.payload.user;
                    user.token = response.payload.access_token.token;
                    user.avatar = response.payload.avatar; 
                    localStorage.setItem(this.ms.storage, JSON.stringify(user));

                    this.alertService.success(response.message);
                    this.router.navigate(['/dashboard']);
                    this.superService.refreshComponent();
                },
                error => {
                    this.loading = false;
                    this.alertService.notify(error,'warning');
                    this.router.navigate(['/']);
                });
    }
}
