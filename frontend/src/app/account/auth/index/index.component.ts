import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as ms from '../../../core/constants/messages';
@Component({
  selector: 'index',
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

  year: number = new Date().getFullYear();
  currentSection = 'home';
  ms:any;
  payload = {};
  session = false;
  constructor(
    private router:Router) { 
      this.ms = ms; 
    }


  ngOnInit() {
  }

 
}
