import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from '../../../shared/ui/ui.module';
import { WidgetModule } from '../../../shared/widget/widget.module';

import { NgxLoadingModule } from 'ngx-loading';
import { IndexComponent } from './index.component';

@NgModule({
  declarations: [IndexModule],
  imports: [
    NgxLoadingModule,
    CommonModule,
    UIModule,
    WidgetModule,
  ]
})
export class IndexModule { }
