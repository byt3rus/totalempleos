import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router,Params } from '@angular/router';

import { AuthenticationService } from '../../../core/services/auth.service';
import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';
import { AlertService } from '../../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../../core/constants/messages';
import {passwordMatchValidator} from '../shared/password-match.directive'

@Component({
  selector: 'app-passwordchange',
  templateUrl: './passwordchange.component.html'
})

/**
 * Reset-password component
 */
export class PasswordChangeComponent implements OnInit, AfterViewInit {

  changeForm: FormGroup;
  submitted = false;
  error = '';
  showError = false;
  success = '';
  loading = false;
  token:Params;
  ms: any;

  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(
    private alertService:AlertService,
    private formBuilder: FormBuilder, 
    private authFackservice: AuthfakeauthenticationService,
    private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService) { this.ms = ms; }

  ngOnInit() {
    const validations = [Validators.required, Validators.minLength(8)];
    this.loading= true;

    this.checkToken(this.route.snapshot.params)

    this.changeForm = this.formBuilder.group({
        password: ['', validations],
        rePassword: ['', validations],
    },{validator: passwordMatchValidator});
  }

  checkToken(params:Params)
  {
    this.token = params;
    this.authFackservice.checkToken(params)
      .pipe(first())
      .subscribe(
        data => { this.loading = false; },
        error => {
          this.loading = false;
          this.router.navigate(['/account/login']);
        });
  }

  ngAfterViewInit() {
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.changeForm.controls; 
  }

  /**
   * On submit form
   */
  onSubmit() {
    this.success = '';
    this.submitted = true;

    // stop here if form is invalid
    if (this.changeForm.invalid) {
      return;
    }else{
      this.loading = true;
      const token = this.token
      this.authFackservice.changePassword({
        password:this.f.password.value,
        token: this.token.token
      })
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success(data.message);
          this.loading = false;
          this.router.navigate(['/account/login']);
        },
        error => {
          this.loading = false;
          this.alertService.warning(error);
          this.error = error;
        });
    }
  }
}
