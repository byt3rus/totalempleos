import { Component, OnInit } from '@angular/core';
import * as ms from '../../../core/constants/messages';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html'
})

/**
 * Policy component
 */
export class PolicyComponent implements OnInit {

  ms: any;
  loading = false;

  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor() { 
      this.ms = ms;
    }

  ngOnInit() {
  }
}
