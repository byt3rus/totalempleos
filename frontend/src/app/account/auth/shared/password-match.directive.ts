import {Directive} from '@angular/core';
import { AbstractControl,NG_VALIDATORS,ValidatorFn,FormGroup, ValidationErrors, Validator } from '@angular/forms';

@Directive({
    selector: '[appPasswordMatch]',
    providers: [{ provide: NG_VALIDATORS, useExisting: PasswordMatchValidatorDirective, multi: true }]
  })
  export class PasswordMatchValidatorDirective implements Validator {
    validate(control: AbstractControl): ValidationErrors {
      return passwordMatchValidator(control);
    }
  }

  export const passwordMatchValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const rePassword = control.get('rePassword');
    return rePassword.value===password.value ? null : { passwordMatch: true } ;
  };