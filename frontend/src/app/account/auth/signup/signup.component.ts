import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../core/services/alert.service';
import { AuthenticationService } from '../../../core/services/auth.service';
import { first } from 'rxjs/operators';
import { UserProfileService } from '../../../core/services/user.service';
import * as ms from '../../../core/constants/messages';
import {passwordMatchValidator} from '../shared/password-match.directive'
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit, AfterViewInit {

  signupForm: FormGroup;
  submitted = false;
  loading = false;
  error = '';
  successmsg = false;
  ms: any;
  fieldTextType: boolean;
  maxChar = 50;
  minChar = 3;
  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(
    private alertService: AlertService,
    private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService,
    private userService: UserProfileService) { this.ms= ms;}

  ngOnInit() {
    const validations = [Validators.required, Validators.minLength(8)];
    const namesValidations = [Validators.required, Validators.minLength(this.minChar), Validators.maxLength(this.maxChar)];
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['',validations],
      rePassword: ['',],
      first_name: ['', namesValidations],
      last_name: ['', namesValidations],
      middle_name: ['', namesValidations],
    } ,{validator: passwordMatchValidator});
  }


  ngAfterViewInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  onClickFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    } else {
      this.loading = true;
        this.userService.register(this.signupForm.value)
          .pipe(first())
          .subscribe(
            data => {
              this.alertService.notify(data.message, 'success');
              this.loading = false;
                this.router.navigate(['/account/login']);

            },
            error => {
              this.loading = false;
              this.error = error ? error : '';
            });
    }
  }
}
