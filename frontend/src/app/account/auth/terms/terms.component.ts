import { Component, OnInit } from '@angular/core';
import * as ms from '../../../core/constants/messages';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html'
})

/**
 * Policy component
 */
export class TermsComponent implements OnInit {

  ms: any;
  loading = false;

  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor() { 
      this.ms = ms;
    }

  ngOnInit() {
  }
}
