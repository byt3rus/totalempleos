import { Component,OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import * as ms from './core/constants/messages';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {

  ms;
  constructor(private titleService: Title, private metaService: Meta) {
    this.ms = ms;
  }
  ngOnInit() {
    this.titleService.setTitle(this.ms.title);
    this.metaService.addTags([
      {name: 'keywords', content: this.ms.keywords},
      {name: 'description', content: this.ms.description},
      {name: 'robots', content: 'index, follow'}
    ]);
  }
}
