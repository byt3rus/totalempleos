import * as job_types from '../../../app/pages/job_types/config';
import * as categories from '../../../app/pages/categories/config';
import * as scholarships from '../../../app/pages/scholarships/config';
import * as resume_languages from '../../../app/pages/resume_languages/config';
import * as language_proficiencies from '../../../app/pages/language_proficiencies/config';
import * as work_spaces from '../../../app/pages/work_spaces/config';

export const dashboard = {
    admin:{
        job_types: job_types.model,
        categories: categories.categories,
        scholarships: scholarships.model,
        resume_languages: resume_languages.model,
        language_proficiencies: language_proficiencies.model,
        work_spaces: work_spaces.model,

    },
}