export const domain = 'https://www.totalempleos.com';
export const current_version = 'Versión 1.0.0';
export const brand_me = ' Desarrollado por <a target="_blank" href="https://www.aurisdevelopment.com/">AurisDevelopment</a>';


export const god = "god";
export const seeker = "seeker";
export const employer = "employer";
export const admin = "admin";


export const register_admin = "Bitácora de registros";

export const reasons = "Razones de activación";
export const status_active = (status) =>{ return status ? `Actualmente se encuentra activo`:`Se ha desactivado`};
export const created_by = (author,date) =>{ return `Fue creado por ${author} el ${date}`};


//Tags
export const title = "Total Empleos";
export const keywords = "Total Empleos,buscar empleo,empleos chihuahua, vacantes en Chihuahua, vacantes en méxico, sistema para búsqueda de empleo, quiero trabajo, busco trabajo en chihuahua";
export const description = "Sistema para búsqueda de empleo en México Chihuahua y alrededores";


export const success = "¡Buen trabajo!";
export const warning = "¡Ups!";
export const info = "Información";
export const signup = "Registrarse";
export const signup_wait  = "¿Te intereso la vacante?";
export const signup_want = "¡Quiero registrarme!";
export const i_have_account = "¡Ya tengo una cuenta!";
export const not_allow = "¡Acción no permitida!";
export const total = 'TOTAL';

export const addNew = "Agregar nuevo";
export const order = "Ordenar";
export const config = "Editar Perfil";
export const logout = "Cerrar sesión";
export const confirmation_account = "Es necesario que confirmes tu cuenta de correo.";


//resources
export const sexs = [
    'MASCULINO',
    'FEMENINO',
    'PREFIERO NO DECIRLO'
];

export const nationalities = [
    "MEXICANA",
    "NAMIBIANA",
    "ANGOLESA",
    "ARGELIANA",
    "DE BENNIN",
    "BOTSWANESA",
    "BURUNDESA",
    "DE CABO VERDE",
    "COMORENSE",
    "CONGOLESA",
    "MARFILE�A",
    "CHADIANA",
    "DE DJIBOUTI",
    "EGIPCIA",
    "ETIOPE",
    "GABONESA",
    "GAMBIANA",
    "GHANATA",
    "GUINEA",
    "GUINEA",
    "GUINEA ECUATORIANA",
    "LIBIA",
    "KENIANA",
    "LESOTHENSE",
    "LIBERIANA",
    "MALAWIANA",
    "MALIENSE",
    "MARROQUI",
    "MAURICIANA",
    "MAURITANA",
    "MOZAMBIQUE�A",
    "NIGERINA",
    "NIGERIANA",
    "CENTRO AFRICANA",
    "CAMERUNESA",
    "TANZANIANA",
    "RWANDESA",
    "DEL SAHARA",
    "DE SANTO TOME",
    "SENEGALESA",
    "DE SEYCHELLES",
    "SIERRA LEONESA",
    "SOMALI",
    "SUDAFRICANA",
    "SUDANESA",
    "SWAZI",
    "TOGOLESA",
    "TUNECINA",
    "UGANDESA",
    "ZAIRANA",
    "ZAMBIANA",
    "DE ZIMBAWI",
    "ARGENTINA",
    "BAHAME�A",
    "DE BARBADOS",
    "BELICE�A",
    "BOLIVIANA",
    "BRASILE�A",
    "CANADIENSE",
    "COLOMBIANA",
    "COSTARRICENSE",
    "CUBANA",
    "CHILENA",
    "DOMINICA",
    "SALVADORE�A",
    "ESTADOUNIDENSE",
    "GRANADINA",
    "GUATEMALTECA",
    "BRITANICA",
    "GUYANESA",
    "HAITIANA",
    "HONDURE�A",
    "JAMAIQUINA",
    "NICARAGUENSE",
    "PANAME�A",
    "PARAGUAYA",
    "PERUANA",
    "PUERTORRIQUE�A",
    "DOMINICANA",
    "SANTA LUCIENSE",
    "SURINAMENSE",
    "TRINITARIA",
    "URUGUAYA",
    "VENEZOLANA",
    "AMERICANA",
    "AFGANA",
    "DE BAHREIN",
    "BHUTANESA",
    "BIRMANA",
    "NORCOREANA",
    "SUDCOREANA",
    "CHINA",
    "CHIPRIOTA",
    "ARABE",
    "FILIPINA",
    "HINDU",
    "INDONESA",
    "IRAQUI",
    "IRANI",
    "ISRAELI",
    "JAPONESA",
    "JORDANA",
    "CAMBOYANA",
    "KUWAITI",
    "LIBANESA",
    "MALASIA",
    "MALDIVA",
    "MONGOLESA",
    "NEPALESA",
    "OMANESA",
    "PAKISTANI",
    "DEL QATAR",
    "SIRIA",
    "LAOSIANA",
    "SINGAPORENSE",
    "TAILANDESA",
    "TAIWANESA",
    "TURCA",
    "NORVIETNAMITA",
    "YEMENI",
    "ALBANESA",
    "ALEMANA",
    "ANDORRANA",
    "AUSTRIACA",
    "BELGA",
    "BULGARA",
    "CHECOSLOVACA",
    "DANESA",
    "VATICANA",
    "ESPA�OLA",
    "FINLANDESA",
    "FRANCESA",
    "GRIEGA",
    "HUNGARA",
    "IRLANDESA",
    "ISLANDESA",
    "ITALIANA",
    "LIECHTENSTENSE",
    "LUXEMBURGUESA",
    "MALTESA",
    "MONEGASCA",
    "NORUEGA",
    "HOLANDESA",
    "PORTUGUESA",
    "BRITANICA",
    "SOVIETICA BIELORRUSA",
    "SOVIETICA UCRANIANA",
    "RUMANA",
    "SAN MARINENSE",
    "SUECA",
    "SUIZA",
    "YUGOSLAVA",
    "AUSTRALIANA",
    "FIJIANA",
    "SALOMONESA",
    "NAURUANA",
    "PAPUENSE",
    "SAMOANA",
    "ESLOVACA",
    "BURKINA FASO",
    "ESTONIA",
    "MICRONECIA",
    "REINO UNIDO(DEPEN. TET. BRIT.)",
    "REINO UNIDO(BRIT. DEL EXT.)",
    "REINO UNIDO(C. BRIT. DEL EXT.)",
    "REINO UNIDO",
    "KIRGUISTAN",
    "LITUANIA",
    "MOLDOVIA, REPUBLICA DE",
    "MACEDONIA",
    "ESLOVENIA",
    "ESLOVAQUIA"
];

//confirmation
export const complete = {
    title: '¡Completa tu perfil! ',
    desc: '¿Quieres que las empresas descubran tu talento? Tan solo te tomará un par de minutos de y de esta forma podrás lograr que más personas te encuentren y se interesen en ti.',
    skip: 'No volver a mostrar'
}


//Branding
export const brand = {
    SYSTEM: 'Total empleos',
}

//Auth
export const login = {
    WELCOME: 'Bienvenido',
    GREETING: 'Inicia sesión para continuar',
    SIGN_IN: 'Entrar',
    FORGOT_PASSWORD: '¿Olvidaste tu contraseña?',
    HAS_ACCOUNT: '¿No tienes cuenta aún? ',
    REGISTER: 'Crear cuenta',
    LOG: 'Inicia sesión aquí',
    remember: 'Recordarme',
}

export const policy = {
    WELCOME: 'Aviso de privacidad',
    title: 'Maribel Ramirez Alvarado comercialmente conocido como TotalEmpleos reconoce que nos estás proporcionando tus datos de contacto, así como tus datos laborales y académicos. Nuestro compromiso es tratar estos datos para los fines de promoción laboral frente a terceros propios de este sitio, así como para ofrecerte promociones comerciales de bienes o servicios propios o de terceros que consideramos que pueden ser de tu interés. Antes de que nos proporciones cualquier información, deberás de leer y comprender los términos de este Aviso de Privacidad, manifestando expresamente su consentimiento al Aceptar los mismos. Salvo para cumplir con las finalidades anteriores, no transferiremos sus datos a ningún tercero.',
}

export const terms = {
    WELCOME: 'Uso, términos y condiciones.',

}

export const register = {
    CREATE_ACCOUNT: 'Crea una cuenta',
    FREE: '¡Es gratis!',
    FREE_INDEX: '¿Qué esperar para registrarte? Muchas empresas te están esperando ¡Es gratis y rápido!',
    
    DESC: 'Crear cuenta',
    TERMS_USE: 'Al registrarte aceptas nuestros ',
    TERMS_USE_LINK: 'Terminos y condiciones',
    POLICY: 'y también nuestra',
    POLICY_LINK: 'Política de privacidad',
    ALREADY_ACCOUNT: '¿Ya tienes una cuenta?',
    NEXT: 'Siguiente',
    EMPLOYER: 'Cuenta de sesión',
    EMPLOYER_DATA: 'Datos de contacto',

}

//Common

export const storage = "session";//Nombre de la sessión como se guarda
export const sessionError = "Error al intentar conectarse a los servicios";

export const defaultImage = '/assets/images/anon.webp';

export const dashboard = {
    TITLE: 'Escritorio',
    LABEL: 'Inicio',
    GREETING: 'Bienvenid@ de nuevo',
    DESCRIPTION: "Candidato"
};

export const settings = {
    CHOOSE: 'Elija una imagen',
    FILE: 'Elje un archivo',
    TITLE: 'Editar perfil',
    SELECT: 'Selecciona'

};

export const months = [
    {id:"01",month:"Enero"},
    {id:"02",month:"Febrero"},
    {id:"03",month:"Marzo"},
    {id:"04",month:"Abril"},
    {id:"05",month:"Mayo"},
    {id:"06",month:"Junio"},
    {id:"07",month:"Julio"},
    {id:"08",month:"Agosto"},
    {id:"09",month:"Septiembre"},
    {id:"10",month:"Octubre"},
    {id:"11",month:"Noviembre"},
    {id:"12",month:"Diciembre"},
    
];

export const not_found = {
    title:  `No hay resultados` ,
    description: "El termino que introdujiste no arrojó ningún resultado. Tal vez esté mal escrito.",
}

export const helpers = {
    years: (e) => `${e} Años`,
    tel_icon: (n) => `<i class="bx bx-phone-outgoing "></i> ${n}`,
    cel_icon: (n) => `<i class="bx bx-mobile-vibration  "></i> ${n}`,
    neighborhood: (n) => `Colonia: ${n}`,
    street: (n) => `Calle: ${n}`,
    number: (n) => `# ${n}`,
    salaray: (s) => `${s} Mensuales `,
}
export const seekers_edit = {
    basic: {
        title: '',
        _title: 'Título',
        _title_desc: '¿Cuál es tu profesión?',
        sex: 'Sexo',
        birthday: 'Fecha de nacimiento',
        nationality: 'Nacionalidad',
    },
    location: {
        title: 'Información de ubicación',
        availability: "Disponible",
        location: 'Ubicación',
        contact: "Contacto",
        website: "Sitio web",
        neighborhood: "Colonia",
        street: "Calle",
        number_ext: "Número exterior",
        zip_code: "Código postal",
        phone: "Telefono",
        phone_extra: "Tel. segundo contacto",
        mobile: "Celular",

    },
    academic: {
        title: 'Área académica',
        "scholarship_id": "Escolaridad",
        "school_name": "Nombre escuela",
        "expected_salary": "Expectativa salarial",
        "linked_in": "Linkedin",
        "facebook": "Facebook",
        cv: 'Curriculum Vitae',

    },
    professional_current: {
        title: 'Escolaridad'
    },
    skills: {
        title: 'Habilidades',
        suggest: 'Escribe alguna habilidad que tengas...',

    },
    languages: {
        title: 'Idiomas',
        resume_language_id: 'Idiomas',
        language_proficiency_id: 'Nivel de idioma',
    },
    categories: {
        title: 'Intereses',
        category_id: 'Categorias',
    },
    professionalExperience: {
        title: 'Experiencia laboral',
        job: 'Puesto',
        company: 'Compañía',
        activities_achievements: 'Actividades',
        start_month: 'Mes de inicio',
        end_month: 'Mes de termino',
        start_year: 'Año de inicio',
        end_year: 'Año de termino',
        current: 'Trabajo actual',

    },

};


export const seeker_component = {
    TITLE: 'Buscar empleo',
    LABEL: 'Inicio',
    GREETING: 'Bienvenid@ de nuevo',
    DESCRIPTION: "Candidato"
};

export const search_dashboard = {
    WHAT: '¿Qué?',
    WHERE: '¿Dónde?',
    SEARCH: 'Buscar',
    WHAT_DESC: 'Empleos disponibles.',
    WHERE_DESC: 'Ciudad  o Estado',
};

export const profile = {
    TITLE: 'Mi perfil',
    LABEL: 'Perfil de usuario',
    GREETING: 'Hola!',
    DESCRIPTION: "Mi información",
    FULLNAME: "Nombre completo",
    PERSONAL: "Información",

};

export const excludesFormats = {
    collections: ['avatar','dates','fullseen'], 
    get_fullseen: (value)=>
    {
        return `<span class="badge badge-success">${value}</span>`;
    }, 
    get_avatar:  (value)=>{
        if(value==="null" ||value===false || value===null){
        return `
        <div class="avatar-xs">
        <img class="rounded-circle avatar-xs" src="/assets/images/anon.webp" alt="">
        
      </div>
        `;
    } else{
            return `<img class="rounded-circle avatar-xs" src="${value}" alt="">`
        }
    },
    get_dates: (value)=>{
        return `
        <span class="badge badge-pill badge-info ml-1">${value}</span>
        `
    },
}   


export const employeers = {
    TITLE: 'Empresas',
    LAST: 'Últimas empresas activas',
    LABEL: 'Listado de empresas',
    headers: ['','Compañía','Lugar','Contacto'],
    fields: ['avatar','company_name','location','contact'],
    endpoint: 'employers',
    new: 'Nueva área de especialidad',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    hasEdit: false,
    hasNew: true,
    icon: 'bx bx-copy',
    description: 'Listado de empresas que con las que se va a poder crear vacantes.',
};

export const jobs = {
    TITLE: 'Vacantes',
    LAST: 'Últimas vacantes activas',
    LABEL: 'Listado de vacantes',
    headers: ['Título','Fecha','Empresa','Especificación'],
    fields: ['title','dates','employer','spec'],
    endpoint: 'jobs',
    my_jobs: "Mis vacantes creadas",
    new: 'Nueva vacante',
    edit: (value) => `Editar ${value}`,
    timesSeen: (value) => ` Se ha visto ${value} veces`,
    hasDelete: false,
    hasActive: true,
    hasEdit: false,
    hasNew: true,
    icon: 'bx bx-copy',
    description: 'Listado de vacantes creadas.',
    apply_success: '¡Felicidades te has postulado exitosamente! Ahora solo debes esperar a que la empresa te contacte.',
};


export const seekers = {
    TITLE: 'Candidatos',
    LABEL: 'Listado de candidatos',
    headers: ['','Nombre','Usuario', 'Email'],
    fields: ['avatar','fullname','username', 'email'],
    formFields: {
        'avatar': ' ',
        'fullname': 'Nombre',
        'username': 'Usuario',
        'email': 'Email',
        
    },
    endpoint: 'job_seekers',
    new: 'Nueva área de especialidad',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    hasEdit: false,
    hasNew: false,
    icon: 'bx bx-copy',
    description: 'Listado de candidatos.',
};


export const categories = {
    TITLE: 'Áreas de especialidad',
    LABEL: 'Listado de área de especialidad',
    headers: ['Titúlo', 'Fecha de creación'],
    fields: ['title', 'created_at'],
    formFields: {
        'title': 'Titulo',
        'active': 'Estado actual'
    },
    endpoint: 'categories',
    new: 'Nueva área de especialidad',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    icon: 'bx bx-copy',
    description: 'Catálogo para  área de especialidad del empleo, que permitirán hacer búsquedas más específicas.',
};

export const valid = {
    required: 'Este campo es requerido',
    email: 'Este campo debe ser una dirección de e-mail válida.',
    password_confirm: 'Las contraseñas deben de coincidir.',
    serverError: (errors) => `Se presentaron los siguientes errores:<br><hr> ${errors}`,
    minChar: (total) => `Debe de contener al menos ${total} caracteres`,
    maxChar: (total) => `Debe de contener un máximo de ${total} caracteres`,
    url: 'Tiene un formato incorrecto debe comenzar con https://  .',
    match: 'Deben de coincidir.',
    not_found_url: 'Esta dirección no se encuentra disponible',
    postulate_guest: '<span style="font-size:30px;">&#127882; &#127882; &#128512;</span>   ¿Aún no tienes cuenta? Te invitamos a que te registres, es muy sencillo y <strong>rápido y gratis</strong> <span style="font-size:30px;">&#128521;</span>.',

}

export const fields = {
    "FIRST_NAME": "Nombre(s)",
    "MIDDLE_NAME": "Apellido Paterno",
    "LAST_NAME_CONTACT": "Apellido Materno contacto",
    "FIRST_NAME_CONTACT": "Nombre(s) contacto",
    "MIDDLE_NAME_CONTACT": "Apellido Paterno contacto",
    "LAST_NAME": "Apellido Materno",
    "email": 'Dirección de correo electrónico',
    "password": 'Contraseña',
    "password_confirm": 'Confirmar contraseña',
    "location": "Ubicación",
    "PASSWORD": "Contraseña",
    "USERNAME": "Usuario",
    "AVATAR": "Foto de perfil",
    "company_name": "Nombre compañía",
    "company_description": "Descripción compañía",
    "company_logo": "Logo",
    "mobile_contact": "Celular contacto",
    "facebook_url": 'Facebook',
    "linkedin_url": "Linkedin",
    "employer":"Empresa",
    "categories": "Áreas de especialidad",
    "title": "Título",
    "description": "Descripción",
    "spec": "Especificaciones",
    "seen":"Veces visto",
    "user_employer_id": "Empresa",
    "confidencial": "[Confidencial].",
    "active":"Activar",
    "show_salary":"Mostrar salario",
    "salary":"Salario Mensual",
    "start_date": "Fecha comienzo",
    "end_time": "Fecha termino",
    "category_id":"Áreas de especialidad",
    "job_type_id": "Tipo de contratación",
    "work_space_id": "Espacio de trabajo",
    "published": "Publicación",
    "city_id": "Ciudad",
    "state_id": "Estado",
    "zipcode": "Código postal",
    "latitude": "Latitude",
    "longitude": "Longitud",
    "dates": "Fechas",


    "phone": "Teléfono",
    "ext": "Extensión",
    "contact":"Contacto",

    "map": "Mapa",

}

export const controls = {
    "SAVE": 'Guardar',
    "ADD": 'Agregar',
    "CLOSE": 'Cerrar',
    "CONFIRM": "Confirmar"
}


export const confirms = {
    "ACTIVE": 'Es necesario agregar un motivo antes de realizar esta acción.',
    "COMMON": '¿Está seguro de realizar esta acción?',

}


export const cruds = {
    edit: (id) => `/${id}/edit`,
    new: 'new',
    save: 'Guardar',
    cancel: 'Cancelar',
    add: 'Agregar',
    next: 'Siguiente',
    previous: 'Anterior',
    back: 'Regresar',
    show: 'Mostrando',
    more: 'Ver más...',
    apply: 'Postularse',
    created: 'Fecha de creación',
    updated: 'Última actualización',
    humanDate: (date) => {
        const timestamp = Date.parse(date);
        const  dateObject = new Date(timestamp);
        return dateObject.toLocaleDateString("es-ES", { year: 'numeric', month: 'long', day: 'numeric' });
       
    },
    created_at: (date, author) => `${date} por ${author.first_name} ${author.middle_name} ${author.last_name}`,
    updated_at: (date, author) => `${date} por ${author.first_name} ${author.middle_name} ${author.last_name}`,
}


export const actions = {
    GO_PROFILE: 'Ir a perfil',
    ACTIONS: 'Acciones',
    SEEN:'Marcar como visto',
    EDIT: 'Editar',
    SHOW: 'Mostrar',
    DELETE: 'Eliminar',
    NOT_FOUND: 'No se encontraron registros.',
    SELECT: 'Seleccionar.',
    SEARCH: '',
    SEMENGT_PAGINATION: 14,
    ORDER_DEFAULT: 'asc',
    ORDER_DESC: 'desc',
    CONFIRM_DELETE: '¿Estás seguro que deseas eliminar este registro?',
    CONFIRM_APPLY: '¿Está seguro que deseas aplicar a esta vacante?',
    SUCCESS_DELETE: 'Se ha eliminado correctamente',
    CONFIRM_ACTIVE: function (value) {
        value = value ? 'desactivar' : 'activar';
        return `¿Estás seguro que deseas ${value} este registro?`;
    },
    SUCCESS_ACTIVE: 'Se ha realzado correctamente',
    SUCCESS_INACTIVE: 'Se activo correctamente',
    SUCCESS_ACTIVE_LOAD: 'Ahora se encuentra desactivado',
    ERROR_NOT_FOUND: 'Este registro es posible que aún no se haya creado o se haya eliminado.',
    ACTIVE: 'Activar',
    INACTIVE: 'Desactivar',
    ISACTIVE: 'Activo',
    ISINAACTIVE: 'Inactivo',

    LIMIT: [50, 100, 200, 1000],
    SEARCH_ACTIVES: [
        { name: 'Todos', value: '' },
        { name: 'Activos', value: 1 },
        { name: 'Inactivos', value: 0 },
    ],

    PAGINATION: function (page = 0, pagination = 0, total = 0) {
        return `Mostrando ${page} a ${pagination} de ${total}`;
    },
    FOUND_PAGINATE: function(total)
    {
        return  `${total} Resultados encontrados `;
    },
};