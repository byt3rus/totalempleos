import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../services/auth.service';
import {  Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private router:Router,
        private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.router.navigate(['/']);
            }

            if (err.status === 500) {
                return throwError('¡Uy! ¡Algo salió mal!Ayúdanos a mejorar tu experiencia enviando un informe de error.');
            }

            if (err.status === 0){
                return throwError('Al parecer estamos teniendo problemas, intenta nuevamente más tarde.');
            } //Falla internet
                

            if(Array.isArray(err.error.error))
            {
                const errors = err.error.error;
                let finalError = '';
                errors.forEach(error => {
                    finalError += error.message+'<br>';
                });
                return throwError(finalError);
            }
            const error = err.error.error || err.statusText;
            return throwError(error);
        }));
    }
}
