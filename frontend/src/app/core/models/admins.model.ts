export class Admin {
    id: number;
    avatar: any;
    username: string;
    fullname: string;
    active: boolean;
}
