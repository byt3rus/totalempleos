export class User {
    id: number;
    username: string;
    role: string;
    token?: string;
    email: string;
    first_name: string;
    middle_name: string;
    last_name: string;
    avatar: any;
    fullname: string;
    description: string
}
