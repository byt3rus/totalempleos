export class Employer {
    id: number;
    title: string;
    created_at: string;
    updated_at: string;
    company_name: string;
    company_description: string;
    location: string;
    contact: string;
    active: boolean;
    updated:any;
    created:any;
    user:any;
    avatar:string;
    linkedin_url:string;
    facebook_url:string;
    latitude:any;
    longitude:any;
}

export class Display {
    id: number;
    title: string;
}


export class EmployerEdit {
    id: number;
    mobile_contact: string;
    company_name: string;
    company_description: string;
    city_id: string;
    state_id: string;
    zipcode: string;
    phone: string;
    active: boolean;
    ext:any;
    latitude:any;
    longitude:any;
    facebook_url:string;
    linkedin_url:string;
}

