export class Job {
    id: number;
    title: string;
    description: string;
    user_employer_id: number;
    job_type_id: number;
    work_space_id: number;
    category_id: number;
    confidencial: boolean;
    active: boolean;
    show_salary:boolean;
    seen:number;
    salary: number;
    start_date: string;
    end_time: string;
    city_id:number;
    state_id:number;
    zipcode:string;
    spec:string;
    employer:string;
    created_at: string;
    updated_at: string;
    created: string;
    updated: string;
    dates:string;
    location:string;
    employer_logo:string;
    categories: any;
    logo:string;
    ago:string;
    
}


export class JobEdit {
    id: number;
    title: string;
    description: string;
    user_employer_id: number;
    category_id: number;
    job_type_id: number;
    work_space_id: number;
    confidencial: boolean;
    active: boolean;
    show_salary:boolean;
    seen:number;
    salary: number;
    start_date: string;
    end_time: string;
    city_id:number;
    state_id:number;
    zipcode:string;
    spec:string;
    employer:string;
    dates:string;
    created_at: string;
    updated_at: string;
    created: string;
    updated: string;
    location:string;
    logo:string;
    categories: any;
    ago:string;
}

