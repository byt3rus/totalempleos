export class State {
    id: number;
    name: string;
}

export class City {
    id: number;
    name: string;
    model: string;
    description: string;
}
