export class Pagination {
    data: Array<any>;
    lastPage: number;
    page: number;
    total: number;
    perPage: number;
}

export class Payload {
    response: Pagination;
    config: any;
    endpoint: string;
}
