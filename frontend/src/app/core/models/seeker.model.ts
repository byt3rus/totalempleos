export class Basic {
    title: string;
    birthday: string;
    nationality: string;
    sex: string;
}


export class Location {
    availability: any;
    city_id: number;
    state_id: number;
    city: string;
    state: string;
    full_location: string;
    full_contact: string;
    neighborhood: string;
    street: string;
    number_ext: string;
    zip_code: string;
    phone: string;
    phone_extra: string;
    mobile: string;
    website: string;
}

export class Academic {
    id:number;
    scholarship_id: number;
    scholarship: string;
    school_name: string;
    expected_salary: string;
    linked_in: string;
    facebook: string;
    cv: string;
}

export class CategoryModel {
    category_id: number;
    category: string;
    id: number;
}

export class Skill {
    skill: any;
    id: number;
}

export class Language {
    language_proficiency_id: number;
    resume_language_id: number;
    language_proficiency: string;
    resume_language: string;
    id: number;
}

export class ProfessionalExperience {
    start_month: string;
    end_month: string;
    activities_achievements: string;
    start_year: string;
    end_year: string;
    company: string;
    current: boolean;
    job: string;
    id: number;
}