export class User {
    username?: string;
    email?: string;
    first_name?: string;
    middle_name?: string;
    last_name?: string;
    rePassword: string;
    password: string;
}
