import { Injectable } from '@angular/core';
import { Router} from '@angular/router';
import Swal from 'sweetalert2';
import { Notify,} from "notiflix";
import * as ms from './../constants/messages';

@Injectable({ providedIn: 'root' })
export class AlertService {
    ms;
    constructor(
      private router: Router) {
          this.ms= ms;
       }

    success(message: string): void
    {
        Swal.fire(ms.success,message,'success');
    }

    get isAdmin()
    {
        const item = localStorage.getItem(ms.storage);
        const user = JSON.parse(item);
        return [this.ms.admin,this.ms.god].includes(user.role)
    }

    warning(message: string): void
    {

        Swal.fire(ms.warning,message,'warning');
    }

    redirect(message: string): void
    {
        Swal.fire({
            title: ms.signup_wait,
            icon: 'info',
            html:
            message+' <br><br><a class="btn btn-lg btn-outline-pink" '+
            ' href="/account/signup">'+ms.signup_want+'</a> <br><br><hr>'+
            '<a class="btn btn-lg btn-outline-success" '+
            ' href="/account/login">'+ms.i_have_account+'</a>',
            showCloseButton: true,
            backdrop: `
                rgba(0,0,123,0.4)
                
                left top
                no-repeat
            `,
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
            showConfirmButton: false
          });
    }

    info(message: string): void
    {

        Swal.fire(ms.info,message,'info');
    }

    error(message: string): void
    {
        
        Swal.fire(ms.warning,message,'error');
    }

    confirm(callBack,message=ms.actions.CONFIRM_DELETE)
    {
        Swal.fire({
            title: 'Confirmación',
            text: message,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#f46a6a',
            cancelButtonColor: 'rgb(113, 130, 147)',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.value) {
                callBack();
            }
          });
    }

    notify(message: string,type: string): void
    {
        switch (type) {
            case 'success':
                Notify.Success(message);
                break;
            case 'warning':
                Notify.Warning(message);
                break;
            case 'error':
                Notify.Failure(message);
                break;
            case 'info':
                Notify.Info(message);
            default:
                break;
        }
    }

}
