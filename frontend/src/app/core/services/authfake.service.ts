import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models/auth.models';
import { environment } from '../../../environments/environment';
import { Router} from '@angular/router';
import * as ms from './../constants/messages';

import {SuperService} from './super.service'

@Injectable({ providedIn: 'root' })
export class AuthfakeauthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    ms: any;

    constructor(
        private superService: SuperService,
        private http: HttpClient,
        private router: Router
        ) {
            this.ms = ms;
        try {
            const item = localStorage.getItem(ms.storage);
            let user = JSON.parse(item);
            this.currentUserSubject = new BehaviorSubject<User>(user);
            this.currentUser = this.currentUserSubject.asObservable();
        } catch (error) {
            localStorage.clear();
            this.router.navigate(['/']);
        }
        
    }

    public get currentUserValue() {
        try {
            const value = this.currentUserSubject.value;
            return value;
        } catch (error) {
            localStorage.clear()
            this.superService.refreshComponent();
        }
        
    }

    authNotSeeker()
    {
        if(![this.ms.admin,this.ms.god,this.ms.employer].includes(this.currentUserValue.role))
            this.router.navigate(['/']);
    }
    

    public updateUser(user)
    {
        const newUser = this.currentUserSubject.value;
        for(let data in user){
            if(newUser.hasOwnProperty(data))
            {
                newUser[data] = user[data];
            }
        }
        this.superService.updateSession(newUser);
        this.currentUserSubject.next(newUser);
    }

    login(email: string, password: string) {
        const baseURL = environment.apiURL;
        return this.http.post<any>(`${baseURL}login`, { email, password })
            .pipe(map(response => {
                // login successful if there's a jwt token in the response
                const user = response.payload.user;
                user.token = response.payload.access_token.token;
                user.avatar = response.payload.avatar;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem(this.ms.storage, JSON.stringify(user));
                    this.currentUserSubject.next(user);
                return user;
            }));
    }

    checkToken(token)
    {
        const baseURL = environment.apiURL;
        return this.http.post<any>(`${baseURL}check_token`, token)
            .pipe(map(response => {
                return response;
            }));
    }



    resetPassword(email: string)
    {
        const baseURL = environment.apiURL;
        return this.http.post<any>(`${baseURL}reset`, { email })
            .pipe(map(response => {
                return response;
            }));
    }

    changePassword(params: any)
    {
        const baseURL = environment.apiURL;
        return this.http.post<any>(`${baseURL}change_password`, params)
            .pipe(map(response => {
                return response;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.clear();
        this.currentUserSubject.next(null);
    }
}
