import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Payload } from '../../core/models/payload.models';
import { Crud } from '../../core/models/crud.model';

@Injectable({ providedIn: 'root' })
export class CrudService {

    base:string;
    baseURL = environment.apiURL;

    constructor(private http: HttpClient) {

    }

    setBase(base: string)
    {
        this.base = base;
    }

    list()
    {
        return this.http.get<Payload>(`${this.baseURL}${this.base}`)
            .pipe(map(response => {
                return response;
            }));
    }

    store(params:Crud)
    {
        return this.http.post<Crud>(`${this.baseURL}${this.base}`,params)
            .pipe(map(response => {
                return response;
            }));
    }
    update(params:Crud,id:number=0)
    {
        return this.http.put<Crud>(`${this.baseURL}${this.base}/${id}`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    upsert(params:Crud,id:any)
    {
        if(id)
            return this.update(params,id);
        return this.store(params);
    }

    show(id)
    {
        return this.http.get<Crud>(`${this.baseURL}${this.base}/${id}`)
            .pipe(map(response => {
                return response;
            }));
    }
}
