import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable,Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class DashboardService {

    constructor(private http: HttpClient) {
    }

    get dashboard()
    {
        const baseURL = environment.apiURL;
        return this.http.get<any>(`${baseURL}dashboard`)
            .pipe(map(response => {
                return response;
            }));
    }

    skip()
    {
        const baseURL = environment.apiURL;
        return this.http.post<any>(`${baseURL}skip`,{})
            .pipe(map(response => {
                return response;
            }));
    }


}
