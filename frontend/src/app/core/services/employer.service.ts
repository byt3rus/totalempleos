import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Payload } from '../../core/models/payload.models';
import { Employer,EmployerEdit } from '../../core/models/employer.model';

@Injectable({ providedIn: 'root' })
export class EmployerService {

    base = 'employers';
    baseURL = environment.apiURL;

    constructor(private http: HttpClient) {
    }

    list()
    {
        return this.http.get<Payload>(`${this.baseURL}${this.base}`)
            .pipe(map(response => {
                return response;
            }));
    }

    validate_user(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/validate_user`,params)
            .pipe(map(response => {
                return response;
            }));
    }
    update(params:Employer,id:number=0)
    {
        return this.http.put<Employer>(`${this.baseURL}${this.base}/${id}`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    edit(id)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/search`,{id:id})
        .pipe(map(response => {
            return response;
        }));
    }

    show(id)
    {
        return this.http.get<Employer>(`${this.baseURL}${this.base}/${id}`)
            .pipe(map(response => {
                return response;
            }));
    }
}
