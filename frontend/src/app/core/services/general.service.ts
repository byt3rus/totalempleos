import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Payload } from '../../core/models/payload.models';
import { environment } from '../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class GeneralService {

    baseURL = environment.apiURL;

    constructor(private http: HttpClient) {
    }

    search(
        endpoint:string,
        page: number=1,
        order:string='asc',
        search:string='',
        scopeActives:string='',
        limit:string,
    )
    {
        scopeActives = scopeActives===''  ? '': `&actives=${scopeActives}`;
        limit = limit  ? `&limit=${limit}`: '';
        const baseURL = environment.apiURL;
        return this.http.get<any>(`${baseURL}${endpoint}?page=${page}&order=${order}&search=${search}${scopeActives}${limit}`)
            .pipe(map(response => {
                return response;
            }));
    }

    /**
     * Delete universal
     * @param endpoint 
     * @param id 
     */
    delete(endpoint:string,id:number=0)
    {
        return this.http.delete<Payload>(`${this.baseURL}${endpoint}/${id}`)
            .pipe(map(response => {
                return response;
            }));
    }

    active(endpoint:string,id)
    {
        return this.http.post<Payload>(`${this.baseURL}${endpoint}/active`,{id:id})
            .pipe(map(response => {
                return response;
            }));
    }

    seen(endpoint:string,id)
    {
        return this.http.post<Payload>(`${this.baseURL}${endpoint}/seen`,{id:id})
            .pipe(map(response => {
                return response;
            }));
    }


}
