import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Payload } from '../../core/models/payload.models';
import { JobEdit,Job } from '../../core/models/job.model';

@Injectable({ providedIn: 'root' })
export class JobService {

    base = 'jobs';
    baseURL = environment.apiURL;

    constructor(private http: HttpClient) {
    }

    list()
    {
        return this.http.get<Payload>(`${this.baseURL}${this.base}`)
            .pipe(map(response => {
                return response;
            }));
    }

    get myJobs()
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/my_jobs`,{})
            .pipe(map(response => {
                return response;
            }));
    }
    update(params:Job,id:number=0)
    {
        return this.http.put<Job>(`${this.baseURL}${this.base}/${id}`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    store(params,id)
    {
        if(id)
        {
            return this.http.put<any>(`${this.baseURL}${this.base}/${id}`,params)
            .pipe(map(response => {
                return response;
            }));
        }
        return this.http.post<any>(`${this.baseURL}${this.base}`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    categories(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/categories`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    edit(id)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/search`,{id:id})
        .pipe(map(response => {
            return response;
        }));
    }

    show(id)
    {
        return this.http.get<Job>(`${this.baseURL}${this.base}/${id}`)
            .pipe(map(response => {
                return response;
            }));
    }
}
