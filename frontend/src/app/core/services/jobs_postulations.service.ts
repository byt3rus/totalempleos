import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Payload } from '../../core/models/payload.models';
import { JobPostulation} from '../../core/models/jobs_postulations.model';

@Injectable({ providedIn: 'root' })
export class JobService {

    base = 'jobs_postulations/';
    baseURL = environment.apiURL;

    constructor(private http: HttpClient) {
    }


    store(id:number)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}apply`,{job_id:id})
            .pipe(map(response => {
                return response;
            }));
    }

    index(params)
    {
        return this.http.get<Payload>(`${this.baseURL}${this.base}index`)
            .pipe(map(response => {
                return response;
            }));
    }

    byUser(params)
    {
        return this.http.get<Payload>(`${this.baseURL}${this.base}byUser`)
            .pipe(map(response => {
                return response;
            }));
    }
}
