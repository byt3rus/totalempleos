import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


/**
 * Enviar la información de un componente a otro mediante suscripciones
 */
@Injectable({ providedIn: 'root' })
export class MessageService {
    private subject = new Subject<any>();

    sendMessage(data: any) {
        this.subject.next(data);
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}