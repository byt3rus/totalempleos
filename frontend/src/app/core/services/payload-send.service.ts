import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PayloadSendService {
    private subject = new Subject<any>();

    sendPayload(data) {
        this.subject.next(data);
    }

    getPayload(): Observable<any> {
        return this.subject.asObservable();
    }
}