import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import {Payload} from '../models/payload.models';
import * as ms from '../constants/messages';
@Injectable({ providedIn: 'root' })
export class ProfiledService {
    
  constructor(private http: HttpClient) {
  }

dataUser(id: number)
    {
        const baseURL = environment.apiURL;
        return this.http.post<Payload>(`${baseURL}profile`,{id:id})
            .pipe(map(response => {
                return response;
            }));
    }
}
