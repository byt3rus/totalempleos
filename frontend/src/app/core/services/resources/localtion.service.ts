import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import {City} from '../../../core/models/location.model';

@Injectable({ providedIn: 'root' })
export class LocationService {

    constructor(private http: HttpClient) {
    }

    list(q:string)
    {
        q = q ? q : '';
        const baseURL = environment.apiURL;
        return this.http.get<City[]>(`${baseURL}resources/cities?q=${q}`)
            .pipe(map(response => {
                return response;
            }));
    }


}
