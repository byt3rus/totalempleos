import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import {City} from '../../../core/models/location.model';
import {Job} from '../../../core/models/job.model';

@Injectable({ providedIn: 'root' })
export class OtherService {

    constructor(private http: HttpClient) {
    }

    sex(q:string)
    {
        q = q ? q : '';
        const baseURL = environment.apiURL;
        return this.http.get<City[]>(`${baseURL}resources/sex?q=${q}`)
            .pipe(map(response => {
                return response;
            }));
    }

    employers(q: any)
    {
        q = q ? q : '';
        const baseURL = environment.apiURL;
        return this.http.get<any>(`${baseURL}resources/employers?q=${q}`)
            .pipe(map(response => {
                return response;
            }));
    }

    jobTypes(q: any)
    {
        q = q ? q : '';
        const baseURL = environment.apiURL;
        return this.http.get<any>(`${baseURL}resources/job_types?q=${q}`)
            .pipe(map(response => {
                return response;
            }));
    }

    workSpaces(q: any)
    {
        q = q ? q : '';
        const baseURL = environment.apiURL;
        return this.http.get<any>(`${baseURL}resources/work_spaces?q=${q}`)
            .pipe(map(response => {
                return response;
            }));
    }

    getJob(id:number)
    {
        const baseURL = environment.apiURL;
        return this.http.get<Job>(`${baseURL}search/${id}`)
            .pipe(map(response => {
                return response;
            }));
    }

    categories(q: any)
    {
        q = q ? q : '';
        const baseURL = environment.apiURL;
        return this.http.get<any>(`${baseURL}resources/categories_list?q=${q}`)
            .pipe(map(response => {
                return response;
            }));
    }

    search(
        page: number=1,
        limit:string,
        params: any,
        admin=false)
    {
        limit = limit  ? `&limit=${limit}`: '';
        params = Object.keys(params).map(key => key + '=' + params[key]).join('&');
        const baseURL = environment.apiURL;
        let action = 'search';
        if(admin)  
            action = 'search_admin';
        const url = `${baseURL}${action}?${params}&page=${page}${limit}`;
        return this.http.get<any>(url)
            .pipe(map(response => {
                return response;
            }));
    }

    jobs(id)
    {
        let param = '';
        if(id)
            param = '?id='+id;
        const baseURL = environment.apiURL;
        return this.http.get<any>(`${baseURL}resources/jobs${param}`)
            .pipe(map(response => {
                return response;
            }));
    }

    nationalities(q:string)
    {
        q = q ? q : '';
        const baseURL = environment.apiURL;
        return this.http.get<City[]>(`${baseURL}resources/nationalities?q=${q}`)
            .pipe(map(response => {
                return response;
            }));
    }


}
