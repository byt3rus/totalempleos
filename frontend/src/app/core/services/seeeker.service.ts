import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Payload } from '../../core/models/payload.models';
import { Employer } from '../../core/models/employer.model';

@Injectable({ providedIn: 'root' })
export class SeekerService {

    base = 'seekers';
    baseURL = environment.apiURL;

    constructor(private http: HttpClient) {
    }

    list(params)
    {
        return this.http.get<any>(`${this.baseURL}job_seekers`)
            .pipe(map(response => {
                return response;
            }));
    }

    basic(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/basic`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    location(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/location`,params)
            .pipe(map(response => {
                return response;
            }));
    }


    academic(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/academic`,params)
            .pipe(map(response => {
                return response;
            }));
    }


    skills(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/skills`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    categories(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/categories`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    languages(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/languages`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    resume_languages_list(){
        return this.http.get<any>(`${this.baseURL}resources/resume_languages_list`)
            .pipe(map(response => {
                return response;
            }));
    }

    deleteCategory(id)
    {
        return this.http.delete<any>(`${this.baseURL}${this.base}/delete_category?id=${id}`)
            .pipe(map(response => {
                return response;
            }));
    }

    deleteProfessionalExperience(id)
    {
        return this.http.delete<any>(`${this.baseURL}${this.base}/delete_professional_experiences?id=${id}`)
            .pipe(map(response => {
                return response;
            }));
    }

    deleteLanguage(id)
    {
        return this.http.delete<any>(`${this.baseURL}${this.base}/delete_language?id=${id}`)
            .pipe(map(response => {
                return response;
            }));
    }


    deleteSkill(id)
    {
        return this.http.delete<any>(`${this.baseURL}${this.base}/delete_skill?id=${id}`)
            .pipe(map(response => {
                return response;
            }));
    }



    professional_experiences(params)
    {
        return this.http.post<any>(`${this.baseURL}${this.base}/professional_experiences`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    cityState(id: number)
    {
        return this.http.get<any>(`${this.baseURL}resources/city_state?id=${id}`)
            .pipe(map(response => {
                return response;
            }));
    }

    cities(id: number)
    {
        return this.http.get<any>(`${this.baseURL}resources/list_cities?id=${id}`)
            .pipe(map(response => {
                return response;
            }));
    }

    categories_list()
    {
        return this.http.get<any>(`${this.baseURL}resources/categories_list`)
            .pipe(map(response => {
                return response;
            }));
    }

    states()
    {
        return this.http.get<any>(`${this.baseURL}resources/states?q=`)
            .pipe(map(response => {
                return response;
            }));
    }

    scholarships()
    {
        return this.http.get<any>(`${this.baseURL}resources/scholarships`)
            .pipe(map(response => {
                return response;
            }));
    }


    see_cv(id:number)
        {
            return this.http.get<any>(`${this.baseURL}cv/${id}`)
                .pipe(map(response => {
                    return response;
                }));
        }

   
}
