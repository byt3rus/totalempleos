import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


/**
 * Enviar la información de un componente a otro mediante suscripciones
 */
@Injectable({ providedIn: 'root' })
export class SendDataService {
    private subject = new Subject<any>();

    sendPayload(data: any) {
        this.subject.next(data);
    }

    getPayload(): Observable<any> {
        return this.subject.asObservable();
    }
}