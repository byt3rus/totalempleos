import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import {Payload} from '../models/payload.models';


@Injectable({ providedIn: 'root' })
export class SettingsService {

    constructor(private http: HttpClient) {
    }

    getUser(id)
    {
        const baseURL = environment.apiURL;
        return this.http.post<Payload>(`${baseURL}settings`,{id:id})
            .pipe(map(response => {
                return response;
            }));
    }

    setProfile(params)
    {
        const baseURL = environment.apiURL;
        return this.http.post<Payload>(`${baseURL}set_profile`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    newAdmin(params)
    {
        const baseURL = environment.apiURL;
        return this.http.post<Payload>(`${baseURL}new_admin`,params)
            .pipe(map(response => {
                return response;
            }));
    }


}
