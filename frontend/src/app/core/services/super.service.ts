import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router} from '@angular/router';
import {AlertService} from './alert.service';
import * as ms from './../constants/messages';
import { MessageService } from '../../core/services/message.service';
@Injectable({ providedIn: 'root' })
export class SuperService {
    ms;
    constructor(
      private alertService:AlertService,
      private router: Router,
      private messages: MessageService,
      private http: HttpClient) {
        this.ms = ms;
    }

    get session() : any
    {
      try {
        const item = localStorage.getItem(this.ms.storage);
        return JSON.parse(item);
      } catch (error) {
        return false; 
      }
    }

    stringDate(date:any)
    {
        const regex = new RegExp(/(\d{6})(\d{2})?/);
        if (regex.test(date)) 
          date = date.substring(0, 4) + '-' + date.substring(4, 6) + '-' + date.substring(6, 8)
        return date;
    }

    /**
     * Cambiar los booleans
     * @param params 
     * @param array 
     */
    setBoolean(params,array)
    {
      for (let index = 0; index < array.length; index++) {
        const field = array[index];
        const boolVal = params.get(field);
        if(boolVal==="true" || boolVal==="1")
          params.set(field,"1")
        else 
          params.set(field,"0")
      }
      return params;
    }

    get getId()
    {
      try {
        const session = this.session;
        if(!session)
        {
          const error = ms.sessionError;
          this.alertService.notify(error,'error')
          throw ms.sessionError
        }
        return session.id;
        
      } catch (error) {
        localStorage.clear();
        this.refreshComponent()
      }
      return 1;
    }

    updateSession(user)
    {
      localStorage.setItem(this.ms.storage,JSON.stringify(user));
    }
    getParams(f)
    {
        let params = {};
        for(let param in f)
        {
        const keyValue = f[param];
        params[param] = keyValue.value;
        }
        return params;
    }


    updateAvatar(data)
    {
      const session = this.session;
      session.avatar = data.payload.avatar;
      localStorage.setItem(ms.storage,JSON.stringify(session));
      this.alertService.success(data.message);
      this.messages.sendMessage({
        username: data.payload.username,
        avatar: data.payload.avatar
      });
    }

    refreshComponent()
    {
      setTimeout(()=>{
        location.reload()
      },2500)
    }

    goHome()
    {
      this.router.navigate(['/'])
    }

    toFormData<T>( formValue: T ) {
        const formData = new FormData();
      
        for ( const key of Object.keys(formValue) ) {
          let value = formValue[key];
          value = value=="null" || value===undefined || value===null ? "" : value; 
          formData.append(key, value);
        }
      
        return formData;
      }

      mergeData(form,params)
      {
        for ( const key of Object.keys(form) ) {
          let value = form[key];
          value = value=="null" || value===undefined || value===null ? "" : value; 
          params.append(key, value);
        }
        return params;
      }


}
