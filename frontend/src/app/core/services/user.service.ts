import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from '../models/auth.models';
import { Payload } from '../models/payload.models';

@Injectable({ providedIn: 'root' })
export class UserProfileService {
    constructor(private http: HttpClient) { }
   baseURL = environment.apiURL;
    confirm(token) {
    return this.http.post<Payload>(`${this.baseURL}confirm_token`, token);
    }

    register(user: User) {
        return this.http.post<Payload>(`${this.baseURL}free_register`, user);
    }

    employers(params,id) {
        if(id)
            return this.http.put<Payload>(`${this.baseURL}employers/${id}`, params);
        else
            return this.http.post<Payload>(`${this.baseURL}employers`, params);
    }

    employeer_validation(user: User) {
        return this.http.post<Payload>(`${this.baseURL}employeer_validation`, user);
    }
}
