import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Payload } from '../../core/models/payload.models';
import { Category } from '../../core/models/category.model';

@Injectable({ providedIn: 'root' })
export class AdminService {

    base = 'admins';
    baseURL = environment.apiURL;

    constructor(private http: HttpClient) {
    }

    list(params)
    {
        
        return this.http.get<Payload>(`${this.baseURL}${this.base}?${this.toEncodeURI(params)}`)
            .pipe(map(response => {
                return response.payload;
            }));
    }

    toEncodeURI(obj)
    {
        let str = '';
        for (let key in obj) {
            if (str != "") {
                str += "&";
            }
            str += key + "=" + obj[key];
        }
        return str;
    }

    active(params:any)
    {
        return this.http.post<Payload>(`${this.baseURL}${this.base}/active`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    store(params:Category)
    {
        return this.http.post<Category>(`${this.baseURL}${this.base}`,params)
            .pipe(map(response => {
                return response;
            }));
    }
    update(params:Category,id:number=0)
    {
        return this.http.put<Category>(`${this.baseURL}${this.base}/${id}`,params)
            .pipe(map(response => {
                return response;
            }));
    }

    upsert(params:Category,id:any)
    {
        if(id)
            return this.update(params,id);
        return this.store(params);
    }

    show(id)
    {
        return this.http.get<Category>(`${this.baseURL}${this.base}/${id}`)
            .pipe(map(response => {
                return response;
            }));
    }
}
