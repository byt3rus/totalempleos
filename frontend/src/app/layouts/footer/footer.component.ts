import { Component, OnInit } from '@angular/core';
import * as ms from '../../core/constants/messages';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

/**
 * Footer component
 */
export class FooterComponent implements OnInit {

  // set the currenr year
  year: number = new Date().getFullYear();
  ms:any;
  constructor() { 
    this.ms =ms;
  }

  ngOnInit() {
  }

}
