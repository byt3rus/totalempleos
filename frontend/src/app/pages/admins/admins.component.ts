import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from '../../core/services/alert.service';
import { AdminService } from '../../core/services/users.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
import * as config from './config';
import {Admin} from '../../core/models/admins.model';
import { FormControl,FormGroup,FormBuilder,Validators } from '@angular/forms';
import { SuperService } from '../../core/services/super.service';
@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html'
})
export class AdminsComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;
 model;
 form: FormGroup;
 reasonForm: FormGroup;
 admins: Admin[];
 skeleton = [0,1,2,3];
 query:string='';
 orderToggle=true;
 order='';
 scopeActives = "";
 currentActiveId:any;
 submitted = false;

  constructor(
    private superService:SuperService,
    private modalService: NgbModal,
    private adminService:AdminService,
    private formBuilder: FormBuilder,
    private alertService:AlertService) { 
      this.ms=ms; 
      this.model = config.model;
    }

  ngOnInit() {
    this.breadCrumbItems = [ { label: this.model.LABEL, active: true }];


    this.reasonForm = this.formBuilder.group({
      reason: ['', [Validators.required, Validators.minLength(8)]],
    });

    this.form = new FormGroup({
      query: new FormControl()
   });
   this.searchData();
    
  }

  get f() {
    return this.reasonForm.controls;
  }


  openActive(content: any,id) {
    this.reasonForm.reset();
    this.currentActiveId = id;
    this.modalService.open(content,{windowClass:'modal-holder'});
  }

  getActiveValue(active)
  {
    return !active ? this.ms.actions.ISINAACTIVE : this.ms.actions.ISACTIVE;
  }

  onEnter(e)
  {
    this.query = e.currentTarget.value;
    this.searchData();
  }

  onSearchClick(value)
  {
    this.query = value;
    this.searchData();
  }

  onOrderClick(e)
  {
    this.orderToggle = !this.orderToggle;
    this.order = this.orderToggle ? ms.actions.ORDER_DEFAULT : ms.actions.ORDER_DESC;
    this.searchData();
  }

  onActiveChange(e)
  {
    this.scopeActives = e.currentTarget.value;
    this.searchData();
  }


  searchData()
  {
    this.loading = true;
    this.adminService.list({
      search: this.query,
      order: this.order,
      actives: this.scopeActives,
    })
      .pipe(first())
      .subscribe(
        admins => { 
          this.admins = admins;
          this.loading = false;
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
  }

  onSubmit(modal){
    this.submitted = true;
    if (this.reasonForm.invalid)
      return;
    else {
    this.loading = true;
    let params = this.superService.toFormData(this.reasonForm.value);
    params.append('id', this.currentActiveId);
    this.adminService.active(params)
      .pipe(first())
      .subscribe(
        response => { 
          this.loading = false;
          this.searchData();
          this.reasonForm.reset();
          modal.close('Close click')
          this.alertService.notify(this.ms.actions.SUCCESS_ACTIVE,'success');
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });

      }
   }

}
