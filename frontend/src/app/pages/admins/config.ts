export const model = {
    TITLE: 'Usuarios administradores',
    LABEL: 'Listado de usuarios administradores',
    headers: ['Titúlo','Descripción'],
    fields: ['title','description'],
    formFields: {
        'title': 'Titulo',
        'description': 'Descripción',
        'active': 'Estado actual',
    },
    endpoint: 'job_types',
    new: 'Nuevo tipo de trabajo',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    description: 'Catálogo para tipos de jornada de empleo.',
    icon: 'bx bx-spray-can ',
};