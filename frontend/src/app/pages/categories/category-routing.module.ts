import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category.component';
import { UpsertComponent } from './upsert.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: CategoryComponent,
    },
    {
        path: 'categories/new', 
        component: UpsertComponent,
    },
    {
        path: 'categories/:id/edit', 
        component: UpsertComponent,
    },
    {
        path: 'categories/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoryRoutingModule {}
