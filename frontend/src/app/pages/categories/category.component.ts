import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../core/services/categories.service';
import { SendDataService } from '../../core/services/send-data.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';

@Component({
  selector: 'app-categories',
  templateUrl: './category.component.html'
})
export class CategoryComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;

  constructor(
    private sendDataService:SendDataService,
    private alertService:AlertService,
    private categoryService: CategoryService) { this.ms=ms; }

  ngOnInit() {
    this.breadCrumbItems = [ { label: this.ms.categories.LABEL, active: true }];

    this.categoryService.list()
      .pipe(first())
      .subscribe(
        response => { 
          this.sendDataService.sendPayload({
            config: this.ms.categories,
            response: response.payload,
            endpoint: this.ms.categories.endpoint
          });
          this.loading = false;
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
  }

}
