import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CategoryService } from '../../core/services/categories.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
import { Router,ActivatedRoute } from '@angular/router';
import { Category } from '../../core/models/category.model';

@Component({
  selector: 'show-categories',
  templateUrl: './show.component.html'
})
export class ShowComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;
 category:Category;
 title:string;
 updated:string;
 created:string;
 id:any;
 endpoint:string;
 pipe:any;
 active:string;

  constructor(
    private alertService:AlertService,
    private router:Router,
    private route: ActivatedRoute,
    private categoryService: CategoryService) { this.ms=ms; }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.endpoint = this.route.snapshot.url[0].path;
    this.getCategory(); 
      
  }

  getCategory()
  {
    this.categoryService.show(this.id)
    .pipe(first())
    .subscribe(
      category => { 
        this.category = category;
        this.title = category.title;
        this.dates()
        this.setBreadCum();
        this.setActive()
        this.loading = false;
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
  }

  setActive()
  {
    this.active = this.category.active ? 'Activo' : 'Inactivo';
  }

  dates()
  {

    
    this.pipe= new DatePipe('en-US');
    let date = this.category.created_at;
    date = this.ms.cruds.humanDate(date);

    let author = this.category.created;
    this.created = this.ms.cruds.created_at(date,author);

    date = this.category.updated_at;
    date = this.ms.cruds.humanDate(date);
    author = this.category.updated;
    this.updated = this.ms.cruds.updated_at(date,author);
  }

  setBreadCum()
  {
    this.breadCrumbItems = [ 
        {  label: this.ms.categories.LABEL  },
        {  label: this.ms.cruds.show  },
        {  label: this.category?.id, active: true  },
    ];
  }

  onBackClick()
  {
    this.router.navigate([`/${this.ms.categories.endpoint}`]);
  }
}
