import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../core/services/categories.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, FormControl, RequiredValidator } from '@angular/forms';
import * as ms from '../../core/constants/messages';
import { Router,ActivatedRoute } from '@angular/router';


@Component({
  selector: 'upsert-categories',
  templateUrl: './upsert.component.html'
})
export class UpsertComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 form :FormGroup;
 ms;
 value;
 submitted=false;
 errors:any=false;
 id:any=false;

  constructor(
    private router:Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder, 
    private alertService:AlertService,
    private categoryService: CategoryService) { this.ms=ms; }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      active: [''],
      });

    if(!this.id)
    {
        this.value = this.ms.categories.new;
        this.setBreadCum();
        this.loading = false;
    }else
        this.getCategory()
       
  }

  getCategory()
  {
    this.categoryService.show(this.id)
    .pipe(first())
    .subscribe(
      category => { 
        this.value = this.ms.categories.edit(category.title);
        this.form.setValue({
          title:category.title,
          active:category.active,
        })
        this.setBreadCum();
        this.loading = false;
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
  }

  setBreadCum()
  {
    this.breadCrumbItems = [ 
        {  label: this.ms.categories.LABEL  },
        {  label: this.value, active: true  },
    ];
  }

  get f() {
    return this.form.controls; 
  }

  onSubmit()
  {
    
    this.submitted = true;
    if (this.form.invalid && this.form.dirty) 
      return;
    else
    {
        this.errors = "";
        this.loading = true;
        this.categoryService.upsert(this.form.value,this.id)
      .pipe(first())
      .subscribe(
        response => { 
          this.errors  = false;
          this.router.navigate([`/${this.ms.categories.endpoint}`]);
          this.loading = false;
        },error => {
          this.loading = false;
          this.errors = this.ms.valid.serverError(error);
        });
    }
  }

  onBackClick()
  {
    this.router.navigate([`/${this.ms.categories.endpoint}`]);
  }
}
