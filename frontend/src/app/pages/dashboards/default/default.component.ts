import { Component, OnInit } from '@angular/core';
import * as ms from '../../../core/constants/messages';
import { DashboardService } from '../../../core/services/dashboard.service';
import { AlertService } from '../../../core/services/alert.service';
import { SuperService } from '../../../core/services/super.service';
import { SendDataService } from '../../../core/services/send-data.service';
import { Router} from '@angular/router';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
loading = true;

  constructor(
    private sendDataService:SendDataService,
    private alertService:AlertService,
    private dashboardService:DashboardService,
    private superService: SuperService,
    private router: Router
  ) { 
  }

  ngOnInit() {
    this.dashboardService.dashboard
     .pipe(first())
      .subscribe(
        data => { 
          //Manda la información al servicio
          this.sendDataService.sendPayload(data.payload);
          this.loading = false;
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
  }

}
