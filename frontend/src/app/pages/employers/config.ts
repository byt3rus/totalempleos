export const model = {
    TITLE: 'Empresas',
    LABEL: 'Listado de empresas',
    formFields: {
        'company': 'Datos empresa',
        'place': 'Lugar',
        'contact': 'Datos contacto'
    },
    new: "Nueva empresa",
    endpoint: 'employers',

    general: 'Datos generales',
    contact: 'Datos de contacto',
    company: 'Datos de compañía',
    location: 'Datos de ubicacion',
    extra: 'Datos extra',
};