import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployerComponent } from './employer.component';
import { NewComponent } from './new.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: EmployerComponent,
    },
    {
        path: 'employers/new', 
        component: NewComponent,
    },
    {
        path: 'employers/:id/edit', 
        component: NewComponent,
    },
    {
        path: 'employers/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmployerRoutingModule {}
