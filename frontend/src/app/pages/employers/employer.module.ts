import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EmployerRoutingModule } from './employer-routing.module';
import { UIModule } from '../../shared/ui/ui.module';
import { WidgetModule } from '../../shared/widget/widget.module';

import { NgApexchartsModule } from 'ng-apexcharts';
import { NgbDropdownModule, NgbTooltipModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgxLoadingModule } from 'ngx-loading';
import { EmployerComponent } from './employer.component';
import { NewComponent } from './new.component';
import { ShowComponent } from './show.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { SafePipeModule } from 'safe-pipe';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ArchwizardModule } from 'angular-archwizard';
import { NgxMaskModule } from 'ngx-mask';

import {UserComponent} from './forms/user.component'
import {FormComponent} from './forms/form.component'
import {EmployerNodesModule} from './forms/employer.module';

import { AgmCoreModule } from '@agm/core';
@NgModule({
  declarations: [
    EmployerComponent,
    NewComponent,
    ShowComponent,
    UserComponent,
    FormComponent,
  ],
  imports: [
    SafePipeModule,
    ArchwizardModule,
    NgxMaskModule.forRoot(),
    EmployerNodesModule,
    AgmCoreModule,
    NgxSkeletonLoaderModule,
    NgbAlertModule,
    CommonModule,
    NgxLoadingModule,
    FormsModule,
    ReactiveFormsModule,
    EmployerRoutingModule,
    UIModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbNavModule,
    WidgetModule,
    NgApexchartsModule,
    PerfectScrollbarModule
  ]
})
export class EmployerModule { }
