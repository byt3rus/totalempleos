
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../core/services/alert.service';
import { AuthenticationService } from '../../../core/services/auth.service';
import { first } from 'rxjs/operators';
import { UserProfileService } from '../../../core/services/user.service';
import { User } from '../../../core/models/user.model';
import * as ms from '../../../core/constants/messages';
import { SeekerService } from '../../../core/services/seeeker.service';
import { City, State } from '../../../core/models/location.model';
import { EmployerEdit } from '../../../core/models/employer.model';
import { SuperService } from '../../../core/services/super.service';
import { SendDataService } from '../../../core/services/send-data.service';
@Component({
  selector: 'employers-user-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {
  @Input() employer = {} as User;
  @Input() data = {} as EmployerEdit;
  submitted = false;
  loading = false;
  user = true;
  errors: any = false;
  error: any = false;
  successmsg = false;
  ms: any;
  fieldTextType: boolean;
  maxChar = 120;
  minChar = 3;
  payload: any;
  cities: [City];
  states: [State];
  employerEdit:EmployerEdit;
  isEdit = false;
  // set the currenr year
  year: number = new Date().getFullYear();
  userForm: FormGroup;
  fileName:string;
  avatarFile:any;
  // tslint:disable-next-line: max-line-length
  constructor(
    private alertService: AlertService,
    private sendDataService:SendDataService,
    private superService: SuperService,
    private seekerService: SeekerService,
    private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService,
    private userService: UserProfileService) { 
      this.ms = ms; 

    }

    getControls() {
      
      const model = this.data;
      const result = {
        city_id: model.city_id,
        state_id: model.state_id,
        company_name: model.company_name,
        mobile_contact: model.mobile_contact,
        company_description: model.company_description,
        zipcode: model.zipcode,
        phone: model.phone,
        ext: model.ext,
        latitude: model.latitude,
        longitude: model.longitude,
        facebook_url: model.facebook_url,
        linkedin_url: model.linkedin_url,
      };
      this.loading = false;
      return result;
    }

  ngOnInit() {
    
    this.userForm = this.formBuilder.group({
      company_name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(255)]],
      company_description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(255)]],
      company_logo: [''],
      mobile_contact: ['', [Validators.required, Validators.maxLength(50), Validators.minLength(10)]],
      city_id: ['', [Validators.required]],
      state_id: ['', [Validators.required]],
      zipcode: ['', [Validators.maxLength(10), Validators.required]],
      latitude: [''],
      longitude: [''],
      phone: ['', [Validators.maxLength(20), Validators.minLength(10)]],
      ext: ['', [Validators.maxLength(5)]],
      linkedin_url: ['', [Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
      facebook_url: ['', [Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
    });
    if(this.data.id)
      this.isEdit = true;
    this.displayStates();
  }

  get f() {
    return this.userForm.controls;
  }


  onBackClick() {
    this.sendDataService.sendPayload({user:true});
  }

  onFileChanged(event) {
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      this.avatarFile = file;
      this.fileName = file.name;
      this.userForm.markAsDirty()
    }
  }

  onStateChange(e) {
    this.loading = true;
    this.seekerService.cities(e.currentTarget.value)
      .pipe(first())
      .subscribe(
        cities => {
          this.cities = cities;
            this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }

  displayCities(id: any) {
    this.loading = true;
    this.seekerService.cities(id)
      .pipe(first())
      .subscribe(
        cities => {
          this.cities = cities;
          if(this.isEdit)
            this.userForm.patchValue(this.getControls());//
          else
            this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }
  displayStates() {
    this.loading = true;
    this.seekerService.states()
      .pipe(first())
      .subscribe(
        states => {
          this.loading = false;
          this.states = states;
          
          if(this.isEdit)
            this.displayCities(this.data.state_id);
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }
  /**
  * On submit form
  */
 onSubmit() {
  this.submitted = true;
  // stop here if form is invalid
  if (this.userForm.invalid && this.userForm.dirty) {
      return;
  } else {
      this.loading = true;
      this.errors  = false;
      let params = this.superService.toFormData(this.userForm.value);
      params.append('company_logo', this.avatarFile);
      params = this.superService.mergeData(this.employer,params);

      this.userService.employers(params,this.data.id)
        .pipe(first())
        .subscribe(
          data => {
            this.errors  = false;
            this.loading = false;
            this.user = false;
            this.alertService.notify(this.ms.actions.SUCCESS_ACTIVE,'success');
            this.router.navigate([`/${this.ms.employeers.endpoint}`]);
          },
          error => {
            this.errors = this.ms.valid.serverError(error);
            this.loading = false;
          });
    }
  }
}

