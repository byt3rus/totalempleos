
import { Component, OnInit, AfterViewInit,Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../core/services/alert.service';
import { AuthenticationService } from '../../../core/services/auth.service';
import { first } from 'rxjs/operators';
import { UserProfileService } from '../../../core/services/user.service';
import {  User} from '../../../core/models/user.model';
import { SendDataService } from '../../../core/services/send-data.service';
import * as ms from '../../../core/constants/messages';
import {passwordMatchValidator} from '../../../account/auth/shared/password-match.directive';
import { Subscription } from 'rxjs';
@Component({
    selector: 'employers-user',
    templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {

  signupForm: FormGroup;
  subscription: Subscription;
  submitted = false;
  loading = false;
  user = true;
  employer:User;
  error = '';
  successmsg = false;
  ms: any;
  fieldTextType: boolean;
  maxChar = 120;
  minChar = 3;
  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(
    private sendDataService:SendDataService,
    private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService,
    private userService: UserProfileService) { 
      this.subscription = this.sendDataService.getPayload().subscribe(payload => {
        if (payload) 
            this.user= payload.user;
      });

      this.ms= ms;}

  ngOnInit() {
    const validations = [Validators.required, Validators.minLength(8)];
    const namesValidations = [Validators.required, Validators.minLength(this.minChar), Validators.maxLength(this.maxChar)];
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['',validations],
      rePassword: ['',],
      first_name: ['', namesValidations],
      last_name: ['', namesValidations],
      middle_name: ['', namesValidations],
    } ,{validator: passwordMatchValidator});
  }
  onBackClick()
  {
    this.router.navigate([`/${this.ms.employeers.endpoint}`]);
  }


  ngAfterViewInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  onClickFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    } else {
      this.loading = true;
        this.userService.employeer_validation(this.signupForm.value)
          .pipe(first())
          .subscribe(
            data => {
              this.loading = false;
              this.user = false;
              this.employer = data.payload;

            },
            error => {
              this.loading = false;
              this.error = error ? error : '';
            });
    }
  }
}

