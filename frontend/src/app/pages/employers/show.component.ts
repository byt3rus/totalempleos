import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { EmployerService } from '../../core/services/employer.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
import { Router,ActivatedRoute } from '@angular/router';
import { Employer, EmployerEdit } from '../../core/models/employer.model';
import { User } from '../../core/models/user.model';
import { EmployerModule } from './employer.module';
@Component({
  selector: 'show-categories',
  templateUrl: './show.component.html',
  styleUrls: ['../profile/roles/seeker.scss']
})
export class ShowComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;
 employer:Employer;
 userEmployer: User;
 title:string;
 updated:string;
 created:string;
 id:any;
 endpoint:string;
 pipe:any;
 active:string;
 edit = false;
 username:string;
 employerEdit:EmployerEdit;

  constructor(
    private alertService:AlertService,
    private router:Router,
    private route: ActivatedRoute,
    private employerService: EmployerService) { this.ms=ms; }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.endpoint = this.route.snapshot.url[0].path;
    this.getEmployer(); 
      
  }

  longitude(employer:Employer)
  {
    return  Number(employer.longitude);
  }

  latitude(employer:Employer)
  {
    return Number(employer.latitude);
  }

  getEmployer()
  {
    this.employerService.show(this.id)
    .pipe(first())
    .subscribe(
      employer => { 
        this.employer = employer;
        this.title = employer.user.email;
        this.username = employer.user.username;
        this.userEmployer = employer.user;
        this.dates()
        this.setBreadCum();
        this.setActive()
        this.loading = false;
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
  }

  setActive()
  {
    this.active = this.employer.active ? 'Activo' : 'Inactivo';
  }


  onEditBasicClick() {
    this.loading = true;
    this.employerService.edit(this.employer.id)
    .pipe(first())
    .subscribe(
      employer => { 
        this.edit = true;
        this.loading = false;
        this.employerEdit = employer.payload;
       
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
      
  }
  onCloseBasicClick() {
      this.edit = false;
  }
  dates()
  {

    
    this.pipe= new DatePipe('en-US');
    let date = this.employer.created_at;
    date = this.ms.cruds.humanDate(date);

    let author = this.employer.created;
    this.created = this.ms.cruds.created_at(date,author);

    date = this.employer.updated_at;
    date = this.ms.cruds.humanDate(date);
    author = this.employer.updated;
    this.updated = this.ms.cruds.updated_at(date,author);
  }

  setBreadCum()
  {
    this.breadCrumbItems = [ 
        {  label: this.ms.employeers.LABEL  },
        {  label: this.ms.cruds.show  },
        {  label: this.employer?.id, active: true  },
    ];
  }

  onBackClick()
  {
    this.router.navigate([`/${this.ms.employeers.endpoint}`]);
  }
}
