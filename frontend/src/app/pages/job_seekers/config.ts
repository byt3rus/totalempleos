export const model = {
    TITLE: 'Candidatos',
    LABEL: 'Listado de Candidatos',
    formFields: {
        'username': 'Usuario',
        'email': 'Correo',
        'fullname': 'Nombre',
        'avatar': 'Perfil'
    },
    new: "Nueva empresa",
    endpoint: 'job_seekers',
};