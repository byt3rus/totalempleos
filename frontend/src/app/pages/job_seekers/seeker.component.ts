import { Component, OnInit } from '@angular/core';
import { SeekerService } from '../../core/services/seeeker.service';
import { AlertService } from '../../core/services/alert.service';
import { SendDataService } from '../../core/services/send-data.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
import * as config from './config';

import { FormControl,FormGroup,FormBuilder,Validators } from '@angular/forms';

@Component({
  selector: 'app-sekeer',
  templateUrl: './sekeer.component.html'
})
export class SeekerComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;
 form: FormGroup;
 model;
 skeletons =[...Array(5).keys()]

  constructor(
    private sendDataService:SendDataService,
    private alertService:AlertService,
    private seekerService: SeekerService) { 
      this.ms=ms; 
      this.model  = config.model;
    }

  ngOnInit() {
    this.breadCrumbItems = [ { label: this.model.LABEL, active: true }];
    this.form = new FormGroup({
      query: new FormControl()
   });
    this.seekerService.list({})
      .pipe(first())
      .subscribe(
        response => { 

          this.sendDataService.sendPayload({
            config: this.ms.seekers,
            response: response.payload,
            endpoint: this.ms.seekers.endpoint
          });

          this.loading = false;
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
  }

  onActiveChange(e)
  {

  }
  onEnter(e){}
  onSearchClick(value){}

}
