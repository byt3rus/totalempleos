import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeekerComponent } from './seeker.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: SeekerComponent,
    },
    {
        path: 'job_seekers/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SeekerRoutingModule {}
