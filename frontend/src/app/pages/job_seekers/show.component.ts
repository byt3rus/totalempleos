import { Component, OnInit } from '@angular/core';
import * as ms from '../../core/constants/messages';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'show-categories',
  templateUrl: './show.component.html'
})
export class ShowComponent implements OnInit {

 ms;
 id:any;
 loading = true;

  constructor(
    private router:Router,
    private route: ActivatedRoute,) { this.ms=ms; }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.router.navigate(["/profile/"+this.id]);
      
  }
}
