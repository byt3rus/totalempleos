export const model = {
    TITLE: 'Tipos de contrataciones',
    LABEL: 'Listado de tipos de contrataciones',
    headers: ['Titúlo','Descripción'],
    fields: ['title','description'],
    formFields: {
        'title': 'Titulo',
        'description': 'Descripción',
        'active': 'Estado actual',
    },
    endpoint: 'job_types',
    new: 'Nuevo tipo de contratación',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    description: 'Catálogo para tipos de contratación para los empleos.',
    icon: 'bx bx-spray-can ',
};