import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobTypeComponent } from './job-type.component';
import { UpsertComponent } from './upsert.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: JobTypeComponent,
    },
    {
        path: 'job_types/new', 
        component: UpsertComponent,
    },
    {
        path: 'job_types/:id/edit', 
        component: UpsertComponent,
    },
    {
        path: 'job_types/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class JobTypeRoutingModule {}
