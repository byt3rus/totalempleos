import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JobTypeRoutingModule } from './job-type-routing.module';
import { UIModule } from '../../shared/ui/ui.module';
import { WidgetModule } from '../../shared/widget/widget.module';

import { NgApexchartsModule } from 'ng-apexcharts';
import { NgbDropdownModule, NgbTooltipModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgxLoadingModule } from 'ngx-loading';
import { JobTypeComponent } from './job-type.component';
import { UpsertComponent } from './upsert.component';
import { ShowComponent } from './show.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { SafePipeModule } from 'safe-pipe';

@NgModule({
  declarations: [
    JobTypeComponent,
    UpsertComponent,
    ShowComponent,
  ],
  imports: [
    SafePipeModule,
    NgbAlertModule,
    CommonModule,
    NgxLoadingModule,
    FormsModule,
    ReactiveFormsModule,
    JobTypeRoutingModule,
    UIModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbNavModule,
    WidgetModule,
    NgApexchartsModule,
    PerfectScrollbarModule
  ]
})
export class JobTypeModule { }
