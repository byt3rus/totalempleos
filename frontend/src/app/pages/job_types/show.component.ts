import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CrudService } from '../../core/services/crud.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
import { Router,ActivatedRoute } from '@angular/router';
import { Crud } from '../../core/models/crud.model';
import * as config from './config';

@Component({
  selector: 'show-job-types',
  templateUrl: '../common/show.component.html'
})
export class ShowComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;
 model;
 crud:Crud;
 title:string;
 updated:string;
 created:string;
 id:any;
 endpoint:string;
 pipe:any;
 active:string;

  constructor(
    private alertService:AlertService,
    private router:Router,
    private route: ActivatedRoute,
    private crudService: CrudService) { 
      this.ms=ms; 
      this.model  = config.model;
      this.crudService.setBase(this.model.endpoint);
    }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.endpoint = this.route.snapshot.url[0].path;
    this.getCategory(); 
      
  }

  getCategory()
  {
    this.crudService.show(this.id)
    .pipe(first())
    .subscribe(
      record => { 
        this.crud = record;
        this.title = record.title;
        this.dates()
        this.setBreadCum();
        this.setActive()
        this.loading = false;
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
  }

  setActive()
  {
    this.active = this.crud.active ? 'Activo' : 'Inactivo';
  }

  dates()
  {

    
    this.pipe= new DatePipe('en-US');
    let date = this.crud.created_at;
    date = this.ms.cruds.humanDate(date);

    let author = this.crud.created;
    this.created = this.ms.cruds.created_at(date,author);

    date = this.crud.updated_at;
    date = this.ms.cruds.humanDate(date);
    author = this.crud.updated;
    this.updated = this.ms.cruds.updated_at(date,author);
  }

  setBreadCum()
  {
    this.breadCrumbItems = [ 
        {  label: this.model.LABEL  },
        {  label: this.ms.cruds.show  },
        {  label: this.crud?.id, active: true  },
    ];
  }

  onBackClick()
  {
    this.router.navigate([`/${this.model.endpoint}`]);
  }
}
