export const model = {
    TITLE: 'Vacantes',
    LABEL: 'Listado de vacantes',
    formFields: {
        'company': 'Datos empresa',
        'place': 'Lugar',
        'contact': 'Datos contacto'
    },
    new: "Nueva vacante",
    endpoint: 'jobs',
};