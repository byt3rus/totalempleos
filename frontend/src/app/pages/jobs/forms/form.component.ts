
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../core/services/alert.service';
import { AuthenticationService } from '../../../core/services/auth.service';
import { first } from 'rxjs/operators';
import { JobService } from '../../../core/services/job.service';
import * as ms from '../../../core/constants/messages';
import { SeekerService } from '../../../core/services/seeeker.service';
import { OtherService } from '../../../core/services/resources/others.service';
import { City, State } from '../../../core/models/location.model';
import { JobEdit } from '../../../core/models/job.model';
import { Category } from '../../../core/models/category.model';
import { Employer,Display } from '../../../core/models/employer.model';
import { SuperService } from '../../../core/services/super.service';
import { SendDataService } from '../../../core/services/send-data.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'job-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {
  @Input() data = {} as JobEdit;
  submitted = false;
  loading = false;
  user = true;
  errors: any = false;
  error: any = false;
  ms: any;
  cities: [City];
  states: [State];
  jobEdit:JobEdit;
  isEdit = false;
  employers:[Employer];
  jobTypes:[Display];
  workSpaces:[Display];
  categories:[Category];
  q:string;
  public Editor = ClassicEditor;
  // set the currenr year
  year: number = new Date().getFullYear();
  jobForm: FormGroup;
  constructor(
    private alertService: AlertService,
    private sendDataService:SendDataService,
    private superService: SuperService,
    private seekerService: SeekerService,
    private resource:OtherService,
    private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService,
    private jobService: JobService) { 
      this.ms = ms; 

    }

    getControls() {
      
      const model = this.data;
      const result = {
        title: model.title,
        description: model.description,
        user_employer_id: model.user_employer_id,
        job_type_id: model.job_type_id,
        work_space_id: model.work_space_id,
        confidencial: model.confidencial,
        active: model.active,
        show_salary: model.show_salary,
        salary: model.salary,
        zipcode: model.zipcode,
        start_date: model.start_date,
        end_time: model.end_time,
        city_id: model.city_id,
        state_id: model.state_id,
      };
      this.loading = false;
      return result;
    }

  ngOnInit() {
    
    this.jobForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(1500)]],
      user_employer_id: ['', [Validators.required,]],
      job_type_id: ['', [Validators.required,]],
      work_space_id: ['', [Validators.required,]],
      confidencial: [''],
      active: [''],
      show_salary: [''],
      salary: [''],
      zipcode: [''],

      start_date: ['', [Validators.required,]],
      end_time: ['', [Validators.required,]],
      city_id: ['', [Validators.required,]],
      state_id: ['', [Validators.required,]],

      category_id: ['', [Validators.required,]],
      
    });
    if(this.data.id)
      this.isEdit = true;
    this.displayStates();
  }

  get f() {
    return this.jobForm.controls;
  }


  onBackClick() {
    this.sendDataService.sendPayload({user:true});
  }

  onEmployerChange(e)
  {
    this.loading = true;
    this.resource.employers(e.term)
      .pipe(first())
      .subscribe(
        response => {
            this.employers = response;
            this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }

  onJobTypeChange(e)
  {
    this.loading = true;
    this.resource.jobTypes(e.term)
      .pipe(first())
      .subscribe(
        response => {
            this.jobTypes = response;
            this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }

  onCategoriesChange(e)
  {
    this.loading = true;
    this.resource.categories(e.term)
      .pipe(first())
      .subscribe(
        response => {
            this.categories = response.payload;
            this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }

  onWorkSpaceChange(e)
  {
    this.loading = true;
    this.resource.workSpaces(e.term)
      .pipe(first())
      .subscribe(
        response => {
            this.workSpaces = response;
            this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }


  onStateChange(e) {
    this.loading = true;
    this.seekerService.cities(e.currentTarget.value)
      .pipe(first())
      .subscribe(
        cities => {
          this.cities = cities;
          this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }
  displayCities(id: any) {
    this.loading = true;
    this.seekerService.cities(id)
      .pipe(first())
      .subscribe(
        cities => {
          this.cities = cities;
          if(this.isEdit)
            this.jobForm.patchValue(this.getControls());//
          else
            this.loading = false;
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }
  displayStates() {
    this.loading = true;
    this.resource.jobs(this.data.id)
      .pipe(first())
      .subscribe(
        response => {
          this.loading = false;
          this.states = response.collections.states;
          this.employers = response.collections.userEmployers;
          this.jobTypes = response.collections.jobTypes;
          this.workSpaces = response.collections.workSpaces;

          const categories = response.collections.categories;
          if(categories.length>0)
          {
            // Completa las categorias
            this.categories = categories;
            this.jobForm.patchValue({
              category_id:categories.map((c)=> c.id )
            });
            this.jobForm.patchValue(this.getControls());//
          }
          
         
          if(this.isEdit)
            this.displayCities(this.data.state_id);
        },
        error => {
          this.error = (typeof error === 'string') ? error : error.error;
          this.loading = false;
        });
  }
  /**
  * On submit form
  */
 onSubmit() {
  this.submitted = true;
  // stop here if form is invalid
  if (this.jobForm.invalid && this.jobForm.dirty) {
      return;
  } else {
      this.loading = true;
      this.errors  = false;
      let params = this.superService.toFormData(this.jobForm.value);

      const startDate = this.superService.stringDate(params.get('start_date'));
      params.set('start_date', startDate)
      const endTime = this.superService.stringDate(params.get('end_time'));
      params.set('end_time', endTime)

      params = this.superService.setBoolean(params,['active','confidencial','show_salary']);

      this.jobService.store(params,this.data.id)
        .pipe(first())
        .subscribe(
          data => {
            this.errors  = false;
            this.loading = false;
            this.user = false;
            this.alertService.notify(this.ms.actions.SUCCESS_ACTIVE,'success');
            this.router.navigate([`/${this.ms.jobs.endpoint}`]);
          },
          error => {
            this.errors = this.ms.valid.serverError(error);
            this.loading = false;
          });
    }
  }
}

