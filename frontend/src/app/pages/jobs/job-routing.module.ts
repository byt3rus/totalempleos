import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobComponent } from './job.component';
import { NewComponent } from './new.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: JobComponent,
    },
    {
        path: 'jobs/new', 
        component: NewComponent,
    },
    {
        path: 'jobs/:id/edit', 
        component: NewComponent,
    },
    {
        path: 'jobs/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class JobRoutingModule {}
