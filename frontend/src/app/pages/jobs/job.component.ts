import { Component, OnInit } from '@angular/core';
import { JobService } from '../../core/services/job.service';
import { AlertService } from '../../core/services/alert.service';
import * as ms from '../../core/constants/messages';
import * as config from './config';
import { AuthfakeauthenticationService } from '../../core//services/authfake.service';
@Component({
  selector: 'app-jobs',
  templateUrl: './job.component.html'
})
export class JobComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 ms;
 model;

  constructor(
    private alertService:AlertService,
    private loginSession:AuthfakeauthenticationService,
    private jobService: JobService) { 
      this.ms=ms; 
      this.model  = config.model;
      //Prevenir que puedan acceder al módulo de vacantes
      loginSession.authNotSeeker();
    }

  ngOnInit() {
    this.breadCrumbItems = [ { label: this.model.LABEL, active: true }];
   
  }

}
