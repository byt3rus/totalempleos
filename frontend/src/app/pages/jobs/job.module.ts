import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JobRoutingModule } from './job-routing.module';
import { UIModule } from '../../shared/ui/ui.module';
import { WidgetModule } from '../../shared/widget/widget.module';

import { NgApexchartsModule } from 'ng-apexcharts';
import { NgbDropdownModule, NgbTooltipModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgxLoadingModule } from 'ngx-loading';
import { JobComponent } from './job.component';
import { NewComponent } from './new.component';
import { ShowComponent } from './show.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { SafePipeModule } from 'safe-pipe';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ArchwizardModule } from 'angular-archwizard';
import { NgxMaskModule } from 'ngx-mask';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import {FormComponent} from './forms/form.component'
import {JobNodesModule} from './forms/job.module';


@NgModule({
  declarations: [
    JobComponent,
    NewComponent,
    ShowComponent,
    FormComponent,
  ],
  imports: [
    SafePipeModule,
    CKEditorModule,
    ArchwizardModule,
    NgxMaskModule.forRoot(),
    JobNodesModule,
    NgxSkeletonLoaderModule,
    NgbAlertModule,
    CommonModule,
    NgxLoadingModule,
    FormsModule,
    ReactiveFormsModule,
    JobRoutingModule,
    UIModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbNavModule,
    WidgetModule,
    NgApexchartsModule,
    PerfectScrollbarModule
  ]
})
export class JobModule { }
