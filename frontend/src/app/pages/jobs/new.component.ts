
import { Component, OnInit } from '@angular/core';
import { EmployerService } from '../../core/services/employer.service';
import { LocationService } from '../../core/services/resources/localtion.service';
import { AlertService } from '../../core/services/alert.service';
import { SuperService } from '../../core/services/super.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as ms from '../../core/constants/messages';
import * as config from './config';
import { Router,ActivatedRoute } from '@angular/router';
import { City } from '../../core/models/location.model';
@Component({
  selector: 'new-employers',
  templateUrl: './new.component.html'
})
export class NewComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = false;
 form :FormGroup;
 ms;
 value;
 submitted=false;
 errors:any=false;
 model;
 companyForm: FormGroup;
 locationForm: FormGroup;
 extraForm: FormGroup;
 userForm: FormGroup;
 avatar_file;
 fileName;
 q='';
 citys:City[];
 search:City;
 loadingText = false;
 userRegisterValidation = true;

  constructor(
    private router:Router,
    private route: ActivatedRoute,
    private locationService:LocationService,
    private superService: SuperService,
    private employerService: EmployerService,
    private formBuilder: FormBuilder, 
    private alertService:AlertService,
    ) { 
      this.ms=ms; 
      this.model  = config.model;
    }

  ngOnInit() {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      active: [''],
      });


      let controlsUser ={
        username: ['', [Validators.required, Validators.minLength(5)]],
        email: ['', [Validators.required, Validators.minLength(8), Validators.email]],
        first_name: ['', [Validators.required, Validators.minLength(3),Validators.maxLength(120)]],
        middle_name: ['', [Validators.required, Validators.minLength(3),Validators.maxLength(120)]],
        last_name: ['', [Validators.required, Validators.minLength(3),Validators.maxLength(120)]],
        password: ['', [Validators.required, Validators.minLength(8)]],
      };
      let controlsCompany ={
        company_name: ['',[Validators.required, Validators.minLength(10)]],
        company_description: ['',[Validators.required, Validators.maxLength(255)]],
        company_logo: ['', [Validators.required]],
        mobile_contact:['',[Validators.maxLength(50), Validators.minLength(10)]],
      };
      let controlsLocation ={
        city_id: ['', [Validators.required]],
        state_id: ['', [Validators.required]],
        zipcode: ['', [Validators.maxLength(10),Validators.required]],
        latitude: [''],
        longitude: [''],
      };
      let controlsExtra ={
        phone: ['', [Validators.maxLength(20)]],
        ext: ['', [Validators.maxLength(5)]],
        linkedin_url: ['', [Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
        facebook_url: ['', [Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
      };
  
      this.userForm = this.formBuilder.group(controlsUser);
      this.companyForm = this.formBuilder.group(controlsCompany);
      this.locationForm = this.formBuilder.group(controlsLocation);
      this.extraForm = this.formBuilder.group(controlsExtra);
      this.submitted = true;

  }


  setBreadCum()
  {
    this.breadCrumbItems = [ 
        {  label: this.model.LABEL  },
        {  label: this.value, active: true  },
    ];
  }


  get u() {
    return this.userForm.controls; 
  }

  get c() {
    return this.companyForm.controls; 
  }

  get l() {
    return this.locationForm.controls; 
  }

  get e() {
    return this.extraForm.controls; 
  }

  load()
  {
    this.loadingText = true;
    this.locationService.list(this.q)
    .pipe(first())
    .subscribe(
      response => { 
        this.citys = response;
        this.loadingText = false;
      },error => {
        this.loadingText = false;
      });
  }

  onSearchKey(e)
  {
      this.q = e.term;
      this.load();
  }

  onSearchChange(e)
  {
    this.search = e;
    this.citys = [];
  }

  onFileChanged(event: any) {
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      this.avatar_file = file;
      this.fileName = file.name;
      this.companyForm.markAsDirty()
    }
  }

  onSaveUser()
  {
    this.loading = true;
    let params = this.superService.toFormData(this.userForm.value);
    this.employerService.validate_user(params)
    .pipe(first())
    .subscribe(
      response => { 
        this.loading = false;
      },error => {
        this.loading = false;
        this.alertService.warning(error);
      });
  }

  onBackClick()
  {
    this.router.navigate([`/${this.model.endpoint}`]);
  }
}
