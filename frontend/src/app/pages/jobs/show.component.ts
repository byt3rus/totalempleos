import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { JobService } from '../../core/services/job.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
import { Router,ActivatedRoute } from '@angular/router';
import { Job, JobEdit } from '../../core/models/job.model';
import { Category } from '../../core/models/category.model';

@Component({
  selector: 'show-jobs',
  templateUrl: './show.component.html',
  styleUrls: ['../profile/roles/seeker.scss']
})
export class ShowComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;
 job:Job;
 title:string;
 updated:string;
 created:string;
 id:any;
 endpoint:string;
 pipe:any;
 active:string;
 edit = false;
 username:string;
 jobEdit:JobEdit;
 categories: [Category];

  constructor(
    private alertService:AlertService,
    private router:Router,
    private route: ActivatedRoute,
    private jobService: JobService) { this.ms=ms; }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.endpoint = this.route.snapshot.url[0].path;
    this.getJob(); 
      
  }

  getJob()
  {
    this.jobService.show(this.id)
    .pipe(first())
    .subscribe(
      job => { 
        this.job = job;
        this.title = job.title;
        this.categories = job.categories;
        this.dates()
        this.setBreadCum();
        this.setActive()
        this.loading = false;
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
  }

  setActive()
  {
    this.active = this.job.active ? 'Activo' : 'Inactivo';
  }


  onEditBasicClick() {
    this.loading = true;
    this.jobService.edit(this.job.id)
    .pipe(first())
    .subscribe(
      job => { 
        this.edit = true;
        this.loading = false;
        this.jobEdit = job.payload;
       
       
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
      
  }
  onCloseBasicClick() {
      this.edit = false;
  }
  dates()
  {

    
    this.pipe= new DatePipe('en-US');
    let date = this.job.created_at;
    date = this.ms.cruds.humanDate(date);

    let author = this.job.created;
    this.created = this.ms.cruds.created_at(date,author);

    date = this.job.updated_at;
    date = this.ms.cruds.humanDate(date);
    author = this.job.updated;
    this.updated = this.ms.cruds.updated_at(date,author);
  }

  setBreadCum()
  {
    this.breadCrumbItems = [ 
        {  label: this.ms.jobs.LABEL  },
        {  label: this.ms.cruds.show  },
        {  label: this.job?.id, active: true  },
    ];
  }

  onBackClick()
  {
    this.router.navigate([`/${this.ms.jobs.endpoint}`]);
  }
}
