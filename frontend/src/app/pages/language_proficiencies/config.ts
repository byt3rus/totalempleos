export const model = {
    TITLE: 'Dominio de idiomas',
    LABEL: 'Listado de los dominios en los idiomas',
    headers: ['Titúlo','Descripción'],
    fields: ['title','description'],
    formFields: {
        'title': 'Titulo',
        'description': 'Descripción',
        'active': 'Estado actual',
    },
    endpoint: 'language_proficiencies',
    new: 'Nuevo dominio de idioma',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    description: 'Catálogo para los niveles de idioma que puede dominar una persona.',
    icon: 'bx bx-world  ',
};