import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguageProficiencyComponent } from './language-proficiency.component';
import { UpsertComponent } from './upsert.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: LanguageProficiencyComponent,
    },
    {
        path: 'language_proficiencies/new', 
        component: UpsertComponent,
    },
    {
        path: 'language_proficiencies/:id/edit', 
        component: UpsertComponent,
    },
    {
        path: 'language_proficiencies/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LanguageProficiencyRoutingModule {}
