import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../core/services/crud.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as ms from '../../core/constants/messages';
import { Router,ActivatedRoute } from '@angular/router';
import * as config from './config';

@Component({
  selector: 'upsert-language-proficiencies',
  templateUrl: '../common/upsert.component.html'
})
export class UpsertComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 form :FormGroup;
 ms;
 model;
 value;
 submitted=false;
 errors:any=false;
 id:any=false;

  constructor(
    private router:Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder, 
    private alertService:AlertService,
    private crudService: CrudService) { 
      this.ms=ms;
      this.model  = config.model;
      this.crudService.setBase(this.model.endpoint);
    }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      active: [''],
      description: [''],
      });

    if(!this.id)
    {
        this.value = this.model.new;
        this.setBreadCum();
        this.loading = false;
    }else
        this.getData()
       
  }

  getData()
  {
    this.crudService.show(this.id)
    .pipe(first())
    .subscribe(
      record => { 
        this.value = this.model.edit(record.title);
        this.form.setValue({
          title:record.title,
          active:record.active,
          description: record.description,
        })
        this.setBreadCum();
        this.loading = false;
      },error => {
        this.alertService.notify(this.ms.actions.ERROR_NOT_FOUND,'error');
        this.loading = false;
        this.onBackClick();
      });
  }

  setBreadCum()
  {
    this.breadCrumbItems = [ 
        {  label: this.model.LABEL  },
        {  label: this.value, active: true  },
    ];
  }

  get f() {
    return this.form.controls; 
  }

  onSubmit()
  {
    
    this.submitted = true;
    if (this.form.invalid && this.form.dirty) 
      return;
    else
    {
        this.errors = "";
        this.loading = true;
        this.crudService.upsert(this.form.value,this.id)
      .pipe(first())
      .subscribe(
        response => { 
          this.errors  = false;
          this.router.navigate([`/${this.model.endpoint}`]);
          this.loading = false;
        },error => {
          this.loading = false;
          this.errors = this.ms.valid.serverError(error);
        });
    }
  }

  onBackClick()
  {
    this.router.navigate([`/${this.model.endpoint}`]);
  }
}
