import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../core/services/crud.service';
import { SendDataService } from '../../core/services/send-data.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
@Component({
  selector: 'app-my-postulations',
  templateUrl: '../common/list.component.html'
})
export class MyPostulationComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;
  ms;
  model;
  loading = true;
  payload = [];
  constructor(
    private sendDataService: SendDataService,
    private alertService: AlertService,
    private crudService: CrudService) {
    this.ms = ms;
    this.model = this.customModel;
    this.crudService.setBase(this.model.endpoint);
  }

  ngOnInit() {
    this.breadCrumbItems = [{ label: this.model.LABEL, active: true }];
    this.payload = [];
    this.crudService.list()
      .pipe(first())
      .subscribe(
        response => {
          this.payload = response.payload;
          this.checkAdmin()
          this.sendDataService.sendPayload({
            config: this.model,
            response: this.payload,
            endpoint: this.model.endpoint
          });
          this.loading = false;
        }, error => {
          this.loading = false;
          this.alertService.warning(error);
        });
  }

  get customModel() {
    let model = {
      TITLE: 'Mis postulaciones',
      LABEL: 'Listado de postulaciones',
      endpoint: 'jobs_postulations/me',
      headers: ['Titúlo', 'Fecha de publicación vacante', 'Fecha postulación', 'Visto',],
      fields: ['title', 'ago', 'date', 'fullseen'],
      //Datos de admin
      headerExtraAdmin: 'Usuario',
      fieldExtraAdmin: 'see_user',

      seenAdmin: false,
      hasEdit: false,
      hasNew: false,
      actions: false,
    };
    return model;
  }

  checkAdmin() {
    const isAdmin = this.payload['data'].filter((e) => e.hasOwnProperty('admin')).length;
    if (isAdmin > 0)
      this.model.seenAdmin = true;
  }
}
