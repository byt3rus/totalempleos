import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultComponent } from './dashboards/default/default.component';

/** CRUDS */
import { CategoryComponent } from './categories/category.component';
import { JobTypeComponent } from './job_types/job-type.component';
import { LanguageProficiencyComponent } from './language_proficiencies/language-proficiency.component';
import { ResumeLanguageComponent } from './resume_languages/resume-language.component';
import { ScholarshipComponent } from './scholarships/scholarship.component';
import { WorkSpaceComponent } from './work_spaces/work-space.component';

import { AdminsComponent } from './admins/admins.component';
import { EmployerComponent } from './employers/employer.component';
import { SeekerComponent } from './job_seekers/seeker.component';
import { JobComponent } from './jobs/job.component';
import { MyPostulationComponent } from './my_postulations/my_postulations';

import { ApplyComponent } from '../shared/ui/dashboards/complements/apply.component';


import { SettingsComponent } from './settings/settings.component';
import { ProfileComponent } from './profile/profile.component';
const routes: Routes = [
  { path: '', redirectTo: 'dashboard' },
  { path: 'dashboard', component: DefaultComponent },
  { path: 'apply/:id', component: ApplyComponent },
  { path: 'categories', component: CategoryComponent },
  { path: 'job_types', component: JobTypeComponent },
  { path: 'resume_languages', component: ResumeLanguageComponent },
  { path: 'scholarships', component: ScholarshipComponent },
  { path: 'work_spaces', component: WorkSpaceComponent },
  { path: 'language_proficiencies', component: LanguageProficiencyComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'profile', component: ProfileComponent },

  { path: 'admins', component: AdminsComponent },
  { path: 'employers', component: EmployerComponent },
  { path: 'job_seekers', component: SeekerComponent },
  { path: 'jobs', component: JobComponent },
  { path: 'my_postulations', component: MyPostulationComponent },
  { path: 'job_postulations', component: MyPostulationComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
