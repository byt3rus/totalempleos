import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { NgbNavModule, NgbDropdownModule, NgbModalModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';

import { WidgetModule } from '../shared/widget/widget.module';
import { UIModule } from '../shared/ui/ui.module';

import { FullCalendarModule } from '@fullcalendar/angular';

import { PagesRoutingModule } from './pages-routing.module';


//Cruds
import { DashboardsModule } from './dashboards/dashboards.module';
import { CategoryModule } from './categories/catergory.module';
import { JobTypeModule} from './job_types/job-type.module';
import { MyPostulationsModule} from './my_postulations/my-postulations.module';
import { LanguageProficiencyModule} from './language_proficiencies/language-proficiency.module';
import { ResumeLanguageModule} from './resume_languages/resume-language.module';
import { ScholarshipModule} from './scholarships/scholarship.module';
import { WorkSpaceModule} from './work_spaces/work-space.module';
import { SeekerModule} from './job_seekers/sekeer.module';

import { AdminsModule} from './admins/admins.module';
import { EmployerModule} from './employers/employer.module';
import { JobModule} from './jobs/job.module';

//--------------

import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { LoaderService } from '../core/services/loader.service';
import { LoaderInterceptorService } from '../core/services/interceptors/loader-interceptor.service';


//Pages
import { SettingModule } from './settings/settings.module';
import { ProfileModule } from './profile/profile.module';



const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 0.3
};

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    FormsModule,
    NgbDropdownModule,
    NgbModalModule,
    PagesRoutingModule,
    NgApexchartsModule,
    ReactiveFormsModule,
    DashboardsModule,
    CategoryModule,
    JobTypeModule,
    MyPostulationsModule,
    LanguageProficiencyModule,
    ResumeLanguageModule,
    ScholarshipModule,
    WorkSpaceModule,
    SeekerModule,
    AdminsModule,
    EmployerModule,
    JobModule,
    SettingModule,
    ProfileModule,
    HttpClientModule,
    UIModule,
    WidgetModule,
    FullCalendarModule,
    NgbNavModule,
    NgbTooltipModule,
    PerfectScrollbarModule
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true }
  ]
})
export class PagesModule { }
