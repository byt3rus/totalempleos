import { Component, OnInit } from '@angular/core';
import { ProfiledService } from '../../core/services/profile.service';

import { first } from 'rxjs/operators';
import {User} from '../../core/models/auth.models';
import {SuperService} from '../../core/services/super.service';
import { AlertService } from '../../core/services/alert.service';
import { SendDataService } from '../../core/services/send-data.service';
import * as ms from '../../core/constants/messages';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit {
 breadCrumbItems: Array<{}>;

 profile: User;
 loading = true;
 ms: any;
  id:any

  constructor(
    private sendDataService:SendDataService,
    private alertService:AlertService,
    private superService:SuperService,
    private router: Router,
    private route: ActivatedRoute,
    private  profiledService: ProfiledService) {
        this.ms = ms;
        
    }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    this.breadCrumbItems = [ { label: this.ms.profile.LABEL, active: true }];
    const id =  this.id!==null ? this.id : this.superService.getId;
    this.profiledService.dataUser(id)
      .pipe(first())
      .subscribe(
        data => { 
          this.profile = data.payload;
          this.sendDataService.sendPayload(data.payload);
          this.loading = false;
        },error => {
          this.loading = false;
          this.router.navigate(['/dashboard']);
        });
  }

}
