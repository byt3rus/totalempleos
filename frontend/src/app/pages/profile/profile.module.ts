import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from '../../shared/ui/ui.module';
import { WidgetModule } from '../../shared/widget/widget.module';

import { NgxLoadingModule } from 'ngx-loading';
import { ProfileComponent } from './profile.component';
import {ProfileRolesModule} from '../profile/roles/roles.module';
import {ProfileRoutingModule} from './profile-routing.module';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    ProfileRoutingModule,
    ProfileRolesModule,
    NgxLoadingModule,
    CommonModule,
    UIModule,
    WidgetModule,
  ]
})
export class ProfileModule { }
