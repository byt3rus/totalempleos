import {Component,OnDestroy} from '@angular/core';

import * as ms from '../../../core/constants/messages';
import { DashboardService } from '../../../core/services/dashboard.service';
import { AlertService } from '../../../core/services/alert.service';
import { SendDataService } from '../../../core/services/send-data.service';
import {Job} from '../../../core/models/job.model';
import { Subscription } from 'rxjs';
@Component({
  selector: 'profile-admin-data',
  templateUrl: './admin.component.html'
})
export class AdminComponent implements OnDestroy {

  ms;
  subscription: Subscription;
  authValid = [ms.god,ms.admin];
  payload: any;
  isAuth=false;
  id:any;
  reasons =[];
  jobs:[Job];
  constructor(
    private sendDataService:SendDataService,
    
  ) {


    this.subscription = this.sendDataService.getPayload().subscribe(payload => {
      if (payload) {
          this.payload= payload;
          if(this.authValid.includes(payload.role))
              this.nextLoad();
      } 
    });
      this.ms = ms;
   }

   nextLoad()
   {
     this.isAuth=true;
     this.jobs = this.payload.timeline.jobs;
     this.reasons = this.payload.timeline.reasons;
   }

  ngOnDestroy() {
    this.subscription.unsubscribe();
   }

}
