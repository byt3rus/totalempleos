import {Component,OnDestroy} from '@angular/core';
import { Router } from '@angular/router';

import * as ms from '../../../core/constants/messages';
import { DashboardService } from '../../../core/services/dashboard.service';
import { AlertService } from '../../../core/services/alert.service';
import { SendDataService } from '../../../core/services/send-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'profile-employer-data',
  templateUrl: './employer.component.html'
})
export class EmployerComponent implements OnDestroy {

  ms;
  subscription: Subscription;
  authValid = [ms.employer];
  payload: any;
  isAuth = false;

  constructor(
    private sendDataService:SendDataService,
    private alertService:AlertService,
    private dashboardService:DashboardService,
    private router: Router
  ) {
    this.subscription = this.sendDataService.getPayload().subscribe(payload => {
      if (payload) {
          this.payload= payload;
          if(this.authValid.includes(payload.role))
              this.nextLoad();
      } 
    });
      this.ms = ms;
   }

   nextLoad()
   {
     this.isAuth=true;
   }

  ngOnDestroy() {
    this.subscription.unsubscribe();
   }

}
