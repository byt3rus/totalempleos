import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { EmployerComponent } from './employer.component';
import { SeekerComponent } from './seeker.component';
import { SeekerModule } from './seekers_components/seeker.module';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { WidgetModule } from '../../../shared/widget/widget.module';

@NgModule({
  imports: [
    CommonModule, 
    FormsModule, 
    ReactiveFormsModule,
    NgxLoadingModule, 
    NgbAlertModule, 
    NgSelectModule, 
    NgxMaskModule.forRoot(),
    WidgetModule,
    NgbDatepickerModule,
    SeekerModule
  ],
  declarations: [AdminComponent, EmployerComponent, SeekerComponent],
  exports: [AdminComponent, EmployerComponent, SeekerComponent],
})
export class ProfileRolesModule { }