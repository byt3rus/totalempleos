import { Component, Input, OnInit } from '@angular/core';

import * as ms from '../../../../core/constants/messages';
import { SeekerService } from '../../../../core/services/seeeker.service';
import { SuperService } from '../../../../core/services/super.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Academic } from '../../../../core/models/seeker.model';
import { Scholarship } from '../../../../core/models/scholarships.model';

@Component({
    selector: 'academic-seeker',
    templateUrl: './academic.component.html',
    styleUrls: ['../seeker.scss']
})
export class AcademicComponent implements OnInit {
    @Input() academic: boolean;
    @Input() data = {} as Academic;
    ms;
    seeker;
    academicForm: FormGroup;
    submitted = false;
    loading = false;
    errors: any = false;
    error: any = false;
    scholarships:[Scholarship];
    fileName:string;
    cvFile:any;

    constructor(
        private formBuilder: FormBuilder,
        private seekerService: SeekerService,
        private superService: SuperService,
    ) {
        this.ms = ms;
        this.seeker = ms.seekers_edit;
    }

    ngOnInit() {
        this.academicForm = this.formBuilder.group({
            scholarship_id: ['', [Validators.required]],
            school_name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
            expected_salary: ['', [ Validators.maxLength(255)]],
            linked_in: ['', [ Validators.maxLength(150)]],
            facebook: ['', [ Validators.maxLength(150)]],
            cv: [''],
        });
        
    }

    getControls() {
        const model = this.data;
        return {
            scholarship_id:model.scholarship_id,
            school_name:model.school_name,
            expected_salary:model.expected_salary,
            linked_in: model.linked_in,
            facebook: model.facebook,
            cv: model.cv,
        };
      }

    get f() { return this.academicForm.controls; }


    onFileChanged(event)
    {
        if (event.target.files && event.target.files.length) {
            const file = event.target.files[0];
            this.cvFile = file;
            this.fileName = file.name;
            this.academicForm.markAsDirty()
          }
    }

    onEditBasicClick() {
        this.displayScholarship();
        
    }
    onCloseBasicClick() {
        this.academicForm.reset();
        this.error = false;
        this.academic = true;
    }

    displayScholarship()
    {
        this.loading = true;
        this.seekerService.scholarships()
                .pipe(first())
                .subscribe(
                    scholarships => {
                        this.loading = false;
                        this.scholarships = scholarships;
                       this.load();
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
    }
   
    load()
    {
        this.academicForm.patchValue(this.getControls());//
         this.academic = false;
    }

   

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.academicForm.invalid && this.academicForm.dirty) {
            return;
        } else {
            this.loading = true;
            let params = this.superService.toFormData(this.academicForm.value);
            params.append('cv', this.cvFile);
            this.seekerService.academic(params)
                .pipe(first())
                .subscribe(
                    data => {
                        this.loading = false;
                        this.data = data.payload;
                        this.academic = true;
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }
    }

}
