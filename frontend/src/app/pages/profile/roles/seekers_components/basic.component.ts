import { Component, Input, OnInit } from '@angular/core';

import * as ms from '../../../../core/constants/messages';
import { AlertService } from '../../../../core/services/alert.service';
import { SeekerService } from '../../../../core/services/seeeker.service';
import { SuperService } from '../../../../core/services/super.service';
import { OtherService } from '../../../../core/services/resources/others.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Basic } from '../../../../core/models/seeker.model';

@Component({
    selector: 'basic-seeker',
    templateUrl: './basic.component.html',
    styleUrls: ['../seeker.scss']
})
export class BasicComponent implements OnInit {
    @Input() basic: boolean;
    @Input() data: Basic;
    ms;
    seeker;
    basicForm: FormGroup;
    submitted = false;
    loading = false;
    errors: any = false;
    error: any = false;

    queryNationality = '';
    _nationalities: any;
    _searchNationality: any;


    sex: number;
    sexs: any;
    nationalitiesData: any;

    constructor(
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private seekerService: SeekerService,
        private superService: SuperService,
        private otherService: OtherService,
    ) {
        this.ms = ms;
        this.seeker = ms.seekers_edit;
        this.sexs = ms.sexs;
        this.nationalitiesData = ms.nationalities;
    }

    ngOnInit() {
        this.basicForm = this.formBuilder.group({
            title: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
            nationality: ['', [Validators.required]],
            sex: ['', [Validators.required]],
            birthday: ['', [Validators.required]],

        });
        
    }

    getControls() {
        const model = this.data;
        return {
          title: model.title,
          birthday: model.birthday,
          sex: model.sex,
          nationality: model.nationality,
        };
      }

    get f() { return this.basicForm.controls; }

    onEditBasicClick() {
        this.basicForm.patchValue(this.getControls());//
        this.basic = false;
    }
    onCloseBasicClick() {
        this.basicForm.reset();
        this.error = false;
        this.basic = true;
    }


    getYears(seeker: Basic) {
        if (seeker) {
            let d1 = new Date;
            let d2= new Date(seeker.birthday)
            let yearsDiff =  d1.getFullYear() -d2.getFullYear();
            return this.ms.helpers.years(yearsDiff);
        }
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.basicForm.invalid) {
            return;
        } else {
            this.loading = true;
            let params = this.superService.toFormData(this.basicForm.value);
            let dateBirthday = this.superService.stringDate(params.get('birthday'));
            params.set('birthday', dateBirthday)
            this.seekerService.basic(params)
                .pipe(first())
                .subscribe(
                    data => {
                        this.loading = false;
                        this.data = data.payload;
                        this.basic = true;
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }
    }

}
