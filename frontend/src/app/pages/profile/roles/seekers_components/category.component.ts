import { Component, Input, OnInit } from '@angular/core';

import * as ms from '../../../../core/constants/messages';
import { AlertService } from '../../../../core/services/alert.service';
import { SeekerService } from '../../../../core/services/seeeker.service';
import { SuperService } from '../../../../core/services/super.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryModel } from '../../../../core/models/seeker.model';
import { Category } from '../../../../core/models/category.model';

@Component({
    selector: 'category-seeker',
    templateUrl: './category.component.html',
    styleUrls: ['../seeker.scss']
})
export class CategoryComponent implements OnInit {
    @Input() category: boolean;
    @Input() data = {} as any;
    ms;
    seeker;
    categoryForm: FormGroup;
    submitted = false;
    loading = false;
    errors: any = false;
    error: any = false;
    categories: [Category];
    isEdit = false;
    constructor(
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private seekerService: SeekerService,
        private superService: SuperService,
    ) {
        this.ms = ms;
        this.seeker = ms.seekers_edit;
    }

    ngOnInit() {
        this.categoryForm = this.formBuilder.group({
            category_id: ['', [Validators.required]],
        });

    }


    get f() { return this.categoryForm.controls; }

    onEditBasicClick() {
        this.displayCategories();
    }
    onCloseBasicClick() {
        this.categoryForm.reset();
        this.error = false;
        this.category = true;
    }

    /**
     * 
     * @param id Hace un soft delete al id indicado
     */
    onDeleteCategoryClick(id: number) {
        this.alertService.confirm(function () {
            this.loading = true;
            this.seekerService.deleteCategory(id)
                .pipe(first())
                .subscribe(
                    response => {
                        this.loading = false;
                        this.data = response['payload'];
                        
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }.bind(this), this.ms.actions.CONFIRM_DELETE)


    }

    displayCategories() {
        this.loading = true;
        this.seekerService.categories_list()
            .pipe(first())
            .subscribe(
                response => {
                    this.loading = false;
                    this.categories = response.payload;
                    this.category = false;
                },
                error => {
                    this.error = (typeof error === 'string') ? error : error.error;
                    this.loading = false;
                });
    }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.categoryForm.invalid) {
            return;
        } else {
            this.loading = true;
            let params = this.superService.toFormData(this.categoryForm.value);

            this.seekerService.categories(params)
                .pipe(first())
                .subscribe(
                    data => {
                        this.loading = false;
                        this.data = data.payload;
                        this.isEdit = true;
                        this.category = true;
                        this.categoryForm.reset();
                        this.error = false;
                    },
                    error => {
                        console.log(error)
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }
    }

}
