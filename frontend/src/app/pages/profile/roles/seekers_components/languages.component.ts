import { Component, Input, OnInit } from '@angular/core';

import * as ms from '../../../../core/constants/messages';
import { AlertService } from '../../../../core/services/alert.service';
import { SeekerService } from '../../../../core/services/seeeker.service';
import { SuperService } from '../../../../core/services/super.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Language } from '../../../../core/models/seeker.model';
import { ResumeLanguage } from '../../../../core/models/resume_language.model';
import { LanguageProficiency } from '../../../../core/models/language_proficiency.model';

@Component({
    selector: 'language-seeker',
    templateUrl: './languages.component.html',
    styleUrls: ['../seeker.scss']
})
export class LanguageComponent implements OnInit {
    @Input() language: boolean;
    @Input() data = {} as any;
    ms;
    seeker;
    languageForm: FormGroup;
    submitted = false;
    loading = false;
    errors: any = false;
    error: any = false;
    languages: [Language];

    resumeLanguages: [ResumeLanguage];
    languageProficiencies: [LanguageProficiency];

    constructor(
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private seekerService: SeekerService,
        private superService: SuperService,
    ) {
        this.ms = ms;
        this.seeker = ms.seekers_edit;
    }

    ngOnInit() {
        this.languageForm = this.formBuilder.group({
            resume_language_id: ['', [Validators.required]],
            language_proficiency_id: ['', [Validators.required]],
        });

    }


    get f() { return this.languageForm.controls; }

    onEditBasicClick() {
        this.displayLanguages();
    }
    onCloseBasicClick() {
        this.languageForm.reset();
        this.error = false;
        this.language = true;
    }

    /**
     * 
     * @param id Hace un soft delete al id indicado
     */
    onDeleteLanguageClick(id: number) {
        this.alertService.confirm(function () {
            this.loading = true;
            this.seekerService.deleteLanguage(id)
                .pipe(first())
                .subscribe(
                    response => {
                        this.loading = false;
                        this.data = response['payload'];
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }.bind(this), this.ms.actions.CONFIRM_DELETE)


    }

    displayLanguages() {
        this.loading = true;
        this.seekerService.resume_languages_list()
            .pipe(first())
            .subscribe(
                response => {
                    this.loading = false;
                    this.resumeLanguages = response.payload.resumeLanguages;
                    this.languageProficiencies = response.payload.languageProficiencies;
                    this.language = false;
                },
                error => {
                    this.error = (typeof error === 'string') ? error : error.error;
                    this.loading = false;
                });
    }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.languageForm.invalid) {
            return;
        } else {
            this.loading = true;
            let params = this.superService.toFormData(this.languageForm.value);

            this.seekerService.languages(params)
                .pipe(first())
                .subscribe(
                    data => {
                        this.loading = false;
                        this.data = data.payload;
                        this.language = true;
                        this.languageForm.reset();
                        this.error = false;
                    },
                    error => {
                        console.log(error)
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }
    }

}
