import { Component, Input, OnInit } from '@angular/core';

import * as ms from '../../../../core/constants/messages';
import { AlertService } from '../../../../core/services/alert.service';
import { SeekerService } from '../../../../core/services/seeeker.service';
import { SuperService } from '../../../../core/services/super.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '../../../../core/models/seeker.model';
import { City,State } from '../../../../core/models/location.model';

@Component({
    selector: 'location-seeker',
    templateUrl: './location.component.html',
    styleUrls: ['../seeker.scss']
})
export class LocationComponent implements OnInit {
    @Input() location: boolean;
    @Input() data = {} as Location;
    ms;
    seeker;
    locationForm: FormGroup;
    submitted = false;
    loading = false;
    errors: any = false;
    error: any = false;
    cities:[City];
    states:[State];
    isEdit = false;
    constructor(
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private seekerService: SeekerService,
        private superService: SuperService,
    ) {
        this.ms = ms;
        this.seeker = ms.seekers_edit;
    }

    ngOnInit() {
        this.locationForm = this.formBuilder.group({
            city_id: ['', [Validators.required]],
            state_id: ['', [Validators.required]],
            neighborhood: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(150)]],
            street: ['', [ Validators.maxLength(150)]],
            number_ext: ['', [ Validators.maxLength(20)]],
            zip_code: ['', [Validators.required,Validators.minLength(5), Validators.maxLength(10)]],
            phone: ['', [Validators.required, Validators.maxLength(50)]],
            mobile: ['', [Validators.required, Validators.maxLength(50)]],
            website: ['', [Validators.maxLength(150)]],
            availability: [''],
            phone_extra: [''],
        });
        
    }

    getControls() {
        const model = this.data;
        return {
            availability: parseInt(model.availability),
            city_id:model.city_id,
            state_id:model.state_id,
            neighborhood:model.neighborhood,
            street: model.street,
            number_ext: model.number_ext,
            zip_code: model.zip_code,
            phone: model.phone,
            phone_extra: model.phone_extra,
            mobile: model.mobile,
            website: model.website,
        };
      }

    get f() { return this.locationForm.controls; }

    get full_location() {return this.data ? `${this._street || ""} ${this._number|| ""} ${this._neighborhood|| ""}  ${this.data.city|| ""} ${this.data.state|| ""} ${this.data.zip_code|| ""}` : ""  }
    get full_contact() {return this.data ? `${this._phone|| ""} ${this.data.phone_extra|| ""} ${this._mobile|| ""}` : ""  }


    get _neighborhood()
    {
        return this.data.neighborhood ? this.ms.helpers.neighborhood(this.data.neighborhood) : "";
    }

    get _street()
    {
        return this.data.street ? this.ms.helpers.street(this.data.street) : "";
    }

    get _number()
    {
        return this.data.number_ext ? this.ms.helpers.number(this.data.number_ext) : "";
    }

    get _mobile()
    {
        return this.data.mobile ? this.ms.helpers.cel_icon(this.data.mobile) : "";
    }

    get _phone()
    {
        return this.data.phone ? this.ms.helpers.tel_icon(this.data.phone) : "";
    }

    onStateChange(e)
    {
       this.displayCities(e.currentTarget.value);
    }

    onEditBasicClick() {
        if(!this.data.city_id)
            this.displayStates();
        else
            this.displayCityState(this.data.state_id);
            
        
    }
    onCloseBasicClick() {
        this.locationForm.reset();
        this.error = false;
        this.location = true;
    }

    displayCityState(id: number)
    {
        this.loading = true;
        this.seekerService.cityState(id)
                .pipe(first())
                .subscribe(
                    response => {
                        this.loading = false;
                        this.cities = response.cities;
                        this.states = response.states;
                       this.load();
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
    }
    displayCities(id: number)
    {
        this.loading = true;
        this.seekerService.cities(id)
                .pipe(first())
                .subscribe(
                    cities => {
                        this.loading = false;
                        this.cities = cities;
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
    }

    load()
    {
        this.locationForm.patchValue(this.getControls());//
         this.location = false;
    }

    displayStates()
    {
        this.loading = true;
        this.seekerService.states()
                .pipe(first())
                .subscribe(
                    states => {
                        this.loading = false;
                        this.states = states;
                        this.load();
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
    }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.locationForm.invalid) {
            return;
        } else {
            this.loading = true;
            let params = this.superService.toFormData(this.locationForm.value);
            const availability = params.get("availability");
            if(availability==="true" || availability==="1")
                params.set("availability","1")
            else 
                params.set("availability","0")
            this.seekerService.location(params)
                .pipe(first())
                .subscribe(
                    data => {
                        this.loading = false;
                        this.data = data.payload;
                        this.isEdit = true;
                        this.location = true;
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }
    }

}
