import { Component, Input, OnInit } from '@angular/core';

import * as ms from '../../../../core/constants/messages';
import { AlertService } from '../../../../core/services/alert.service';
import { SeekerService } from '../../../../core/services/seeeker.service';
import { SuperService } from '../../../../core/services/super.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfessionalExperience } from '../../../../core/models/seeker.model';

@Component({
    selector: 'professional_experience-seeker',
    templateUrl: './professional_experience.component.html',
    styleUrls: ['../seeker.scss']
})
export class ProfessionalExperienceComponent implements OnInit {
    @Input() professionalExperience: boolean;
    @Input() data = {} as any;
    ms;
    seeker;
    professionalExperienceForm: FormGroup;
    submitted = false;
    loading = false;
    errors: any = false;
    error: any = false;
    months = ms.months;
    years = [];
    maximYears = 60;
    professionalExperiences: [ProfessionalExperience];
    constructor(
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private seekerService: SeekerService,
        private superService: SuperService,
    ) {
        this.ms = ms;
        this.seeker = ms.seekers_edit;
    }

    ngOnInit() {
        this.professionalExperienceForm = this.formBuilder.group({
            job: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(150)]],
            company: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]],
            activities_achievements: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(255)]],
            start_month: ['', [Validators.required]],
            end_month: ['', [Validators.required]],
            start_year: ['', [Validators.required]],
            end_year: ['', [Validators.required]],
            current: [''],
        });
        this._years();
    }

    _years() {
        const date = new Date();
        const lastYear = date.getFullYear() - this.maximYears;
        for (let year = date.getFullYear(); year >= lastYear; year--) {
            this.years.push(year);
        }
    }

    get f() { return this.professionalExperienceForm.controls; }

    onEditBasicClick() {
        this.professionalExperience = false;
    }
    onCloseBasicClick() {
        this.professionalExperienceForm.reset();
        this.error = false;
        this.professionalExperience = true;
    }

    /**
     * 
     * @param id Hace un soft delete al id indicado
     */
    onDeleteClick(id: number) {
        this.alertService.confirm(function () {
            this.loading = true;
            this.seekerService.deleteProfessionalExperience(id)
                .pipe(first())
                .subscribe(
                    response => {
                        this.loading = false;
                        this.data = response['payload'];
                        this.professionalExperienceForm.reset();
                        this.error = false;
                        this.professionalExperience = true;
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }.bind(this), this.ms.actions.CONFIRM_DELETE)


    }


    onSubmit() {
        this.submitted = true;
        if (this.professionalExperienceForm.invalid) {
            return;
        } else {
            this.loading = true;
            let params = this.superService.toFormData(this.professionalExperienceForm.value);
            if (params.get("current") === "true")
                params.set("current", "1")
            else
                params.set("current", "0")
            this.seekerService.professional_experiences(params)
                .pipe(first())
                .subscribe(
                    data => {
                        this.loading = false;
                        this.data = data.payload;
                        this.professionalExperienceForm.reset();
                        this.error = false;
                        this.professionalExperience = true;
                    },
                    error => {
                        console.log(error)
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }
    }

}
