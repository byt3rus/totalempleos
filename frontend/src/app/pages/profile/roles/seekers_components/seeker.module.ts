import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicComponent } from './basic.component';
import { LocationComponent } from './location.component';
import {  AcademicComponent} from './academic.component';
import {  CategoryComponent} from './category.component';
import {  SkillComponent} from './skills.component';
import {  LanguageComponent} from './languages.component';
import {  ProfessionalExperienceComponent} from './professional_experience.component';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'


@NgModule({
  imports: [
    CommonModule, 
    FormsModule, 
    ReactiveFormsModule,
    NgxLoadingModule, 
    NgbAlertModule, 
    NgSelectModule, 
    NgxMaskModule.forRoot(),
    NgbDatepickerModule
  ],
  declarations: [ 
      BasicComponent,
      LocationComponent,
      AcademicComponent,
      CategoryComponent,
      SkillComponent,
      LanguageComponent,
      ProfessionalExperienceComponent,
    ],
  exports: [ 
      BasicComponent,
      LocationComponent,
      AcademicComponent,
      CategoryComponent,
      SkillComponent,
      LanguageComponent,
      ProfessionalExperienceComponent,
    ],
})
export class SeekerModule { }