import { Component, Input, OnInit } from '@angular/core';

import * as ms from '../../../../core/constants/messages';
import { AlertService } from '../../../../core/services/alert.service';
import { SeekerService } from '../../../../core/services/seeeker.service';
import { SuperService } from '../../../../core/services/super.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Skill} from '../../../../core/models/seeker.model';

@Component({
    selector: 'skills-seeker',
    templateUrl: './skills.component.html',
    styleUrls: ['../seeker.scss']
})
export class SkillComponent implements OnInit {
    @Input() skill: boolean;
    @Input() data = {} as any;
    ms;
    seeker;
    skillForm: FormGroup;
    submitted = false;
    loading = false;
    errors: any = false;
    error: any = false;
    skills: [Skill];
    constructor(
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private seekerService: SeekerService,
        private superService: SuperService,
    ) {
        this.ms = ms;
        this.seeker = ms.seekers_edit;
    }

    ngOnInit() {
        this.skillForm = this.formBuilder.group({
            skills: ['', [Validators.required]],
        });

    }


    get f() { return this.skillForm.controls; }

    onEditBasicClick() {
        this.displaySkills();
    }
    onCloseBasicClick() {
        this.skillForm.reset();
        this.error = false;
        this.skill = true;
    }

    /**
     * 
     * @param id Hace un soft delete al id indicado
     */
    onDeleteSkillClick(id: number) {
        this.alertService.confirm(function () {
            this.loading = true;
            this.seekerService.deleteSkill(id)
                .pipe(first())
                .subscribe(
                    response => {
                        this.loading = false;
                        this.data = response['payload'];
                        
                    },
                    error => {
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }.bind(this), this.ms.actions.CONFIRM_DELETE)


    }

    displaySkills() {
        this.skill = false;
    }


    onSubmit() {
        this.submitted = true;
        if (this.skillForm.invalid) {
            return;
        } else {
            this.loading = true;
            let params = this.superService.toFormData(this.skillForm.value);

            this.seekerService.skills(params)
                .pipe(first())
                .subscribe(
                    data => {
                        this.loading = false;
                        this.data = data.payload;
                        this.skillForm.reset();
                        this.error = false;
                    },
                    error => {
                        console.log(error)
                        this.error = (typeof error === 'string') ? error : error.error;
                        this.loading = false;
                    });
        }
    }

}
