export const model = {
    TITLE: 'Idiomas',
    LABEL: 'Listado de idiomas',
    headers: ['Titúlo','Descripción'],
    fields: ['title','description'],
    formFields: {
        'title': 'Titulo',
        'description': 'Descripción',
        'active': 'Estado actual',
    },
    endpoint: 'resume_languages',
    new: 'Nuevo idioma',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    description: 'Catálogo para idiomas.',
    icon: 'bx bx-minus-back  ',
};