import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResumeLanguageComponent } from './resume-language.component';
import { UpsertComponent } from './upsert.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: ResumeLanguageComponent,
    },
    {
        path: 'resume_languages/new', 
        component: UpsertComponent,
    },
    {
        path: 'resume_languages/:id/edit', 
        component: UpsertComponent,
    },
    {
        path: 'resume_languages/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ResumeLanguageRoutingModule {}
