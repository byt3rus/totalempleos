import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../core/services/crud.service';
import { SendDataService } from '../../core/services/send-data.service';
import { AlertService } from '../../core/services/alert.service';
import { first } from 'rxjs/operators';
import * as ms from '../../core/constants/messages';
import * as config from './config';

@Component({
  selector: 'app-resume-language',
  templateUrl: '../common/list.component.html'
})
export class ResumeLanguageComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 ms;
 model;

  constructor(
    private sendDataService:SendDataService,
    private alertService:AlertService,
    private crudService: CrudService) { 
      this.ms=ms;
      this.model  = config.model;
      this.crudService.setBase(this.model.endpoint);
  }

  ngOnInit() {
    this.breadCrumbItems = [ { label: this.model.LABEL, active: true }];

    this.crudService.list()
      .pipe(first())
      .subscribe(
        response => { 
          this.sendDataService.sendPayload({
            config: this.model,
            response: response.payload,
            endpoint: this.model.endpoint
          });
          this.loading = false;
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
  }

}
