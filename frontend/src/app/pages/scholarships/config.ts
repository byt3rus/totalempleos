export const model = {
    TITLE: 'Escolaridad',
    LABEL: 'Listado de escolaridades',
    headers: ['Titúlo','Descripción'],
    fields: ['title','description'],
    formFields: {
        'title': 'Titulo',
        'description': 'Descripción',
        'active': 'Estado actual',
    },
    endpoint: 'scholarships',
    new: 'Nueva escolaridad',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    description: 'Catálogo para las escolaridades.',
    icon: 'bx bx-book  ',
};