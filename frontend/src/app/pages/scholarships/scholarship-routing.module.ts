import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScholarshipComponent } from './scholarship.component';
import { UpsertComponent } from './upsert.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: ScholarshipComponent,
    },
    {
        path: 'scholarships/new', 
        component: UpsertComponent,
    },
    {
        path: 'scholarships/:id/edit', 
        component: UpsertComponent,
    },
    {
        path: 'scholarships/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ScholarshipeRoutingModule {}
