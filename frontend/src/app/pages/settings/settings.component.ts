import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../core/services/settings.service';

import { first } from 'rxjs/operators';
import { User } from '../../core/models/auth.models';
import { FormBuilder, FormGroup, Validators, FormControl, RequiredValidator } from '@angular/forms';
import { SuperService } from '../../core/services/super.service';
import { AlertService } from '../../core/services/alert.service';
import { AuthfakeauthenticationService } from '../../core/services/authfake.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as ms from '../../core/constants/messages';


@Component({
  selector: 'app-settings',
  templateUrl: 'settings.component.html'
})
export class SettingsComponent implements OnInit {
  breadCrumbItems: Array<{}>;

  profile: User;
  loading = true;
  settingsForm: FormGroup;
  submitted = false;
  ms;
  avatar_file;
  fileName;
  id: any = false;
  currentSession = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authfakeauthenticationService: AuthfakeauthenticationService,
    private alertService: AlertService,
    private superService: SuperService,
    private formBuilder: FormBuilder,
    private settingService: SettingsService) { this.ms = ms; }

  ngOnInit() {

    const passwordValidator = this.isNew ? ['', [Validators.required, Validators.minLength(8)]] : ['', Validators.minLength(8)];
    let controls = {
      username: ['', [Validators.required, Validators.minLength(5)]],
      email: ['', [Validators.required, Validators.minLength(8), Validators.email]],
      first_name: ['', [Validators.required]],
      middle_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      avatar: [''],
      password: passwordValidator,

    };

    this.settingsForm = this.formBuilder.group(controls);
    if (!this.isNew) {
      this.id = this.route.snapshot.paramMap.get('id');
      this.breadCrumbItems = [{ label: 'Configuración', active: true }];

      this.currentSession = !this.id ? true : false;

      this.id = this.currentSession ? this.superService.getId : this.id;

      if (!this.isInt(this.id))
        this.backAdmins();

      this.settingService.getUser(this.id)
        .pipe(first())
        .subscribe(
          data => {
            this.profile = data.payload;
            this.settingsForm.patchValue(this.getControls());
            this.loading = false;
          }, error => {
            this.loading = false;
            this.backAdmins();
          });
    } else
      this.loading = false;

  }

  isInt(value) {
    return !isNaN(value) && (function (x) { return (x | 0) === x; })(parseFloat(value))
  }

  get f() {
    return this.settingsForm.controls;
  }

  get isNew() {
    return this.router.url === "/settings/new";
  }

  /**
   * Limita los controles que van a ser usados en el formulario
   * desde el payload
   */
  getControls() {
    const model = this.profile;
    return {
      email: model.email,
      username: model.username,
      first_name: model.first_name,
      middle_name: model.middle_name,
      last_name: model.last_name,
      password: ''
    };
  }

  onFileChanged(event: any) {
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      this.avatar_file = file;
      this.fileName = file.name;
      this.settingsForm.markAsDirty()
    }
  }

  backAdmins() {
    this.router.navigate(['/admins'])
  }

  onSubmit() {
    this.submitted = true;
    if (this.settingsForm.invalid && this.settingsForm.dirty)
      return;
    else {
      let params = this.superService.toFormData(this.settingsForm.value);
      params.append('avatar_file', this.avatar_file);
      params.append('id', this.id);
      params.append('currentSession', this.currentSession.toString());
      this.loading = true;
      if (!this.isNew) {
        this.settingService.setProfile(params)
          .pipe(first())
          .subscribe(
            data => {
              if (!this.currentSession) {
                this.alertService.notify(data.message, 'success');
                this.backAdmins();
              } else {
                this.authfakeauthenticationService.updateUser(data.payload)

                this.fileName = this.ms.settings.CHOOSE;
                if (data.payload.hasOwnProperty('avatar')) {
                  this.superService.updateAvatar(data);
                } else {
                  this.loading = false;
                  this.alertService.notify(data.message, 'success');
                }
                this.router.navigate(['/profile'])
              }


            }, error => {
              this.loading = false;
              this.alertService.warning(error);
            });
      } else {
        this.settingService.newAdmin(params)
          .pipe(first())
          .subscribe(
            data => {
              this.alertService.notify(data.message, 'success');
              this.backAdmins();

            }, error => {
              this.loading = false;
              this.alertService.warning(error);
            });
      }
    }
  }

}
