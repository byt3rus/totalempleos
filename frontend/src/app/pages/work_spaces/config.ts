export const model = {
    TITLE: 'Espacios de trabajo',
    LABEL: 'Listado de espacios de trabajo',
    headers: ['Titúlo','Descripción'],
    fields: ['title','description'],
    formFields: {
        'title': 'Titulo',
        'description': 'Descripción',
        'active': 'Estado actual',
    },
    endpoint: 'work_spaces',
    new: 'Nuevo espacio de trabajo',
    edit: (value) => `Editar ${value}`,
    hasDelete: false,
    hasActive: true,
    description: 'Catálogo para espacios de trabajo.',
    icon: 'bx bxs-network-chart  ',
};