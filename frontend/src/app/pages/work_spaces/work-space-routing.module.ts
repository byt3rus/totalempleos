import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkSpaceComponent } from './work-space.component';
import { UpsertComponent } from './upsert.component';
import { ShowComponent } from './show.component';

const routes: Routes = [
    {
        path: '',
        component: WorkSpaceComponent,
    },
    {
        path: 'work_spaces/new', 
        component: UpsertComponent,
    },
    {
        path: 'work_spaces/:id/edit', 
        component: UpsertComponent,
    },
    {
        path: 'work_spaces/:id', 
        component: ShowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WorkSpaceRoutingModule {}
