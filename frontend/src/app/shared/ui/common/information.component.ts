import { Component, OnInit, Input,OnDestroy } from '@angular/core';
import * as ms from '../../../core/constants/messages';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-common-information',
    templateUrl: './information.component.html'
})
export class InformationComponent implements OnInit {

    private _data = new BehaviorSubject<any>({});
    @Input()
    set payload(value) {
        // set the latest value for _data BehaviorSubject
        this._data.next(value);
    };

    get payload() {
        // get the latest value from _data BehaviorSubject
        return this._data.getValue();
    }

    ms;
    profile;
    profileImage = ms.defaultImage;
    constructor(
    ) {
        this.ms = ms;
    }

    ngOnInit() {
        this._data
        .subscribe(data => {
            if(data){
                this.profile = data;
             if (data.avatar)
                this.profileImage = data.avatar
            }
        });
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this._data.unsubscribe();
       }

    
}
