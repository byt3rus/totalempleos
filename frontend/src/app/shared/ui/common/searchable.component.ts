import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../../core/services/alert.service';
import { LocationService } from '../../../core/services/resources/localtion.service';
import { first } from 'rxjs/operators';
import { City } from '../../../core/models/location.model';
import * as ms from '../../../core/constants/messages';

@Component({
  selector: 'searchable-common',
  templateUrl: './searchable.component.html',
  styleUrls: ['./searchable.component.scss']
})
export class SearchableComponent implements OnInit {

  // bread crumb items
 breadCrumbItems: Array<{}>;
 loading = true;
 loadingText = false;
 ms;
 q='';
 locations:City[];
 search:City;
  constructor(
    private locationService:LocationService,
    private alertService:AlertService,) { this.ms=ms; }

  ngOnInit() {
    this.breadCrumbItems = [ { label: this.ms.categories.LABEL, active: true }];
  }

  onSearchClick()
  {
    if(this.search)
      console.log(this.search);
  }

  onSearchKey(e)
  {
      this.q = e.term;
      this.load();
  }

  onSearchChange(e:City)
  {
    this.search = e;
    this.locations = [];
  }

  load()
  {
    this.loadingText = true;
    this.locationService.list(this.q)
    .pipe(first())
    .subscribe(
      response => { 
        this.locations = response;
        this.loadingText = false;
      },error => {
        this.loadingText = false;
      });
  }

}
