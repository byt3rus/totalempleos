import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import {CrudModalComponent} from './modal.component';
import { NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from "@angular/router";
import { ExportAsModule } from 'ngx-export-as';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@NgModule({
  imports: [CommonModule,
    NgbTooltipModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    NgbAlertModule,
    RouterModule,
    ExportAsModule,],
  declarations: [ CrudModalComponent ],
  exports:      [ CrudModalComponent ],
})
export class CrudsModule { }