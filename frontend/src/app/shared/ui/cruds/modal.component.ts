import {Component,OnDestroy} from '@angular/core';
import { Router } from '@angular/router';

import * as ms from '../../../core/constants/messages';
import { DashboardService } from '../../../core/services/dashboard.service';
import { AlertService } from '../../../core/services/alert.service';
import { SendDataService } from '../../../core/services/send-data.service';
import { GeneralService } from '../../../core/services/general.service';
import { Payload,Pagination } from '../../../core/models/pagination.model';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { ExportAsService, ExportAsConfig,SupportedExtensions } from 'ngx-export-as';

@Component({
  selector: 'crud-modal',
  templateUrl: './modal.component.html'
})
export class CrudModalComponent implements OnDestroy {

  ms;
  subscription: Subscription;
  payload: Payload;
  hasDelete:boolean=false;
  hasEdit:boolean=true;
  hasActive:boolean=false;
  hasNew:boolean=true;
  actions=true;
  seenAdmin=false
  isAuth=false;
  headers: [];
  data: any;
  fields: [];
  pagination: Pagination;
  pages = [];
  pageActive = 1;
  hasRecords=false;
  loading=false;
  endpoint = '';
  messageRecords='';
  recordsNotFound=ms.actions.SEARCH
  search='';
  query = new FormControl('');
  scopeActives = "";
  totalRecords:any;
  description:string;


  initPages=1;
  limitPages=ms.actions.SEMENGT_PAGINATION;
  leftArrow = true;
  rightArrow=true;

  orderToggle=true;
  order = ms.actions.ORDER_DEFAULT;

  config: ExportAsConfig = {
    type: 'pdf',
    elementIdOrContent: 'crud-modal',
    options: {
      jsPDF: {
        orientation: 'landscape'
      },
      pdfCallbackFn: this.pdfCallbackFn // to add header and footer
    }
  };


  constructor(
    private exportAsService: ExportAsService,
    private generalService: GeneralService,
    private sendDataService:SendDataService,
    private alertService:AlertService,
    private dashboardService:DashboardService,
    private router: Router
  ) {
    this.subscription = this.sendDataService.getPayload().subscribe(payload => {
      if (payload) {
          this.payload= payload;
          this.nextLoad();
      } 
    });
      this.ms = ms;
   }

   exportAs(type: SupportedExtensions, opt?: string) {
    this.config.type = type;
    if (opt) {
      this.config.options.jsPDF.orientation = opt;
    }
    this.exportAsService.save(this.config, this.payload.config.LABEL).subscribe(() => {
      
    });
  }

   pdfCallbackFn (pdf: any) {
    // example to add page number as footer to every page of pdf
    const noOfPages = pdf.internal.getNumberOfPages();
    for (let i = 1; i <= noOfPages; i++) {
      pdf.setPage(i);
      pdf.text('Pagina ' + i + ' de ' + noOfPages, pdf.internal.pageSize.getWidth() - 100, pdf.internal.pageSize.getHeight() - 30);
    }
  }

   nextLoad()
   {
    
    this.headers = this.payload.config.headers;
    this.fillData()
     this.isAuth=true;
   }

   checkRecord(item,field)
   {
     const excludes = this.ms.excludesFormats;
     if(excludes.collections.includes(field))
     {
       return excludes[`get_${field}`](item);
     }
     return item;
   }

   fillData()
   {

    
    this.pagination = this.payload.response;
    this.endpoint = this.payload.endpoint;
    this.data = this.pagination.data;
    this.showTototal()
    this.fields = this.payload.config.fields;
    this.hasDelete = this.payload.config.hasDelete;
    this.hasActive = this.payload.config.hasActive;
    this.description = this.payload.config.description;
    this.especialConfig();
    this.doPaginate();
   }

   especialConfig()
   {
    if(this.payload.config.hasOwnProperty('hasEdit'))
    {
      this.hasEdit=this.payload.config.hasEdit;
    }
    if(this.payload.config.hasOwnProperty('hasNew'))
    {
      this.hasNew=this.payload.config.hasNew;
    }
    if(this.payload.config.hasOwnProperty('actions'))
    {
      this.actions=this.payload.config.actions;
    }
    if(this.payload.config.hasOwnProperty('seenAdmin'))
    {
      this.seenAdmin=this.payload.config.seenAdmin;
    }
   }

   onSeenClick(item)
   {
    this.loading = true;
    this.generalService.seen(this.endpoint,item.id_main)
      .pipe(first())
      .subscribe(
        response => { 
          this.loading = false;
          this.reopen(); 
          this.alertService.notify(this.ms.actions.SUCCESS_ACTIVE,'success');
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
   }

   onViewUserClick(item)
   {
    this.router.navigate([`/profile/${item.seeker_id}`]);
   }


   doPaginate()
   {
     this.pages = [];
     const limit = this.initPages+this.limitPages;
     this.rightArrow = (this.pagination.lastPage>limit);
     this.leftArrow=(this.initPages>1);
     for(let i=this.initPages;i<=this.pagination.lastPage;i++)
     {
        if(i<=limit)
            this.pages.push(i);
     }
   }

   nextSegmentPages()
   {
       this.initPages= this.initPages+this.limitPages+1;
       this.doPaginate();
   }

   backSegmentPages()
   {
       const init = this.limitPages+1;
       this.initPages= this.initPages-init;
       this.doPaginate();
   }


   showTototal()
   {
    if(this.data.length>0)
    {
        this.recordsNotFound=ms.actions.SEARCH;
        this.messageRecords = ms.actions.PAGINATION(this.pagination.page,this.pagination.perPage,this.pagination.total)
        this.hasRecords=true;
    }else{
        this.messageRecords = "";
        this.hasRecords=false;
        this.recordsNotFound=ms.actions.NOT_FOUND;
    }
    
   }

  ngOnDestroy() {
    this.subscription.unsubscribe();
   }

   onInfoClick()
   {
    this.alertService.info(this.description);
   }

   onNewClick()
   {
    this.router.navigate([`/${this.endpoint}/${this.ms.cruds.new}`]);
   }

   onEditClick(id)
   {
    this.router.navigate([`/${this.endpoint}/${this.ms.cruds.edit(id)}`]);
   }

   onOrderClick(e){
      this.orderToggle = !this.orderToggle;
      this.order = this.orderToggle ? ms.actions.ORDER_DEFAULT : ms.actions.ORDER_DESC;
      this.reload();

   }

   onDeleteClick(e,id)
   {
    const ctx = this;
    this.alertService.confirm(()=> ctx.delete(id));
   }
   onActiveClick(id:number,active:any)
   {
     this.active(id);
   /* 
    const ctx = this;
    this.alertService.confirm(()=> ctx.active(id),this.ms.actions.CONFIRM_ACTIVE(active));
    */
   }
   onActiveChange(e)
   {
     this.scopeActives = e.currentTarget.value;
      this.reload();
   }

   onRecodsChange(e)
   {
    this.totalRecords = e.currentTarget.value;
    this.reload();
   }

   active(id:number){
    this.loading = true;
    this.generalService.active(this.endpoint,id)
      .pipe(first())
      .subscribe(
        response => { 
          this.loading = false;
          this.reopen(); 
          this.alertService.notify(this.ms.actions.SUCCESS_ACTIVE,'success');
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
   }

   getActiveValue(active){
     return !active ? this.ms.actions.ACTIVE : this.ms.actions.INACTIVE;
   }

   delete(id:number=0)
   {
    this.loading = true;
    this.generalService.delete(this.endpoint,id)
      .pipe(first())
      .subscribe(
        response => { 
          this.loading = false;
          this.reopen();
          this.alertService.notify(this.ms.actions.SUCCESS_DELETE,'success');
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
   }

   calculateClasses(active) {
    return {
      bx:true,
      'text-success': active,
        'bxs-toggle-right  ': active,
        'text-secondary': !active,
        'bxs-toggle-right': !active
    };
  }

   onNextPageClick(e, page)
   {
       this.pageActive = page;
       if(this.pagination.page!==page)
            this.reload()
   }

   onEnter(e)
   {
    this.searchData(e.currentTarget.value)
   }

   onSearchClick(search)
   {
    this.searchData(search)
   }

   searchData(query)
   {
    this.clean();
    this.search = query;
    this.reload();
   }

   clean()
   {
    this.initPages=1;
    this.limitPages=ms.actions.SEMENGT_PAGINATION;
    this.pageActive=1;

   }

   onReloadClick(e)
   {
    this.reopen();
   }

   reopen()
   {
    this.clean();
    this.reload()
   }


   reload()
   {
       this.data = [];
       this.loading=true;
       this.hasRecords=false;
       this.generalService.search(
         this.endpoint,
         this.pageActive,
         this.order,
         this.search,
         this.scopeActives,
         this.totalRecords
         )
      .pipe(first())
      .subscribe(
        response => { 
          this.pagination = response.payload;
          this.data = this.pagination.data;
          this.doPaginate();
          this.showTototal();
          window.scroll(0,0);
          this.loading = false;
        },error => {
          this.loading = false;
          this.alertService.warning(error);
        });
   }

}
