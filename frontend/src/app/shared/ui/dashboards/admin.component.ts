import {Component,OnDestroy} from '@angular/core';
import { Router } from '@angular/router';

import * as ms from '../../../core/constants/messages';
import * as ind from '../../../core/constants/indicators';

import { DashboardService } from '../../../core/services/dashboard.service';
import { AlertService } from '../../../core/services/alert.service';
import { SendDataService } from '../../../core/services/send-data.service';
import { Subscription } from 'rxjs';

import {Incators} from '../../../core/models/indicators.model';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './admin.component.html'
})
export class AdminComponent implements OnDestroy {

      // bread crumb items
  breadCrumbItems: Array<{}>;
  ms;
  dashboard;
  profileImage: string;
  user: any;
  show = false;
  authValid = [ms.god,ms.admin];
  subscription: Subscription;
  payload:any;
  permission:string;
  indicators: Incators;
  stats:any;


  constructor(
    private sendDataService: SendDataService,
    private alertService:AlertService,
    private dashboardService:DashboardService,
    private router: Router
  ) {

    this.stats = ind.dashboard.admin;
    this.subscription = this.sendDataService.getPayload().subscribe(payload => {
        if (payload) {
            this.payload= payload;
            if(this.authValid.includes(payload.role))
                this.nextLoad();
        } 
      });

   }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
   }

   nextLoad()
  {
        this.profileImage = ms.defaultImage;
        this.ms = ms;
        this.dashboard = ms.dashboard;
        this.breadCrumbItems = [{ label: ms.dashboard.LABEL }];
        this.user = this.payload;
        if(this.user.avatar)
            this.profileImage = this.user.avatar;
        this.indicators = this.payload.indicators;
        this.show = true;
  }

  onProfileClick()
  {
    this.router.navigate(['/profile']);
  }

}
