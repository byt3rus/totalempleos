import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from "@angular/platform-browser";
import * as ms from '../../../../core/constants/messages';
import { AlertService } from '../../../../core/services/alert.service';
import { JobService } from '../../../../core/services/jobs_postulations.service';
import { Job } from '../../../../core/models/job.model';
import { OtherService } from '../../../../core/services/resources/others.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-dashboard-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./job.scss']
})
export class ApplyComponent implements OnInit, OnDestroy {
  job: Job;
  ms;
  id: string;
  lastTitle: string;
  loading = true;
  constructor(
    private titleService: Title,
    private alertService: AlertService,
    private router: Router,
    private resource: OtherService,
    private jobService: JobService,
    private route: ActivatedRoute,
  ) {
    this.lastTitle = this.titleService.getTitle();
    this.ms = ms;
    this.id = this.route.snapshot.paramMap.get('id');
  }

  /**
   * Obtiene la información de la vacante
   */
  getJob() {
    if (!this.isInt(this.id))
      this.hasError();
    else {
      try {
        this.resource.getJob(parseInt(this.id))
          .pipe(first())
          .subscribe(
            job => {
              this.titleService.setTitle(`${ms.brand.SYSTEM} | ${job.title}`);
              this.job = job;
              this.loading = false;
            }, error => {
              this.loading = false;
              this.hasError();
            });
      } catch (error) {
        this.hasError();
      }

    }
  }

  backAuth()
  {
    if(this.alertService.isAdmin)
      return `/${this.ms.jobs.endpoint}`;
    return '/dashboard';
  }

  hasError() {
    this.alertService.notify(this.ms.valid.not_found_url, 'error')
    this.back();
  }

  back() {
    this.router.navigate(['/']);
  }

  isInt(value) {
    return !isNaN(value) && (function (x) { return (x | 0) === x; })(parseFloat(value))
  }

  showSalary(salary) {
    return this.ms.helpers.salaray(salary);
  }
  showLogo(job: Job) {
    if (job?.confidencial || job?.logo === null)
      return '/assets/images/company.png';
    return job?.logo;
  }

  /**
   * Boton para poder postularse a la vacante
   */
  onApplyClick() {
    if(!this.alertService.isAdmin)
    {
      this.alertService.confirm(function () {
        this.loading = true;
        this.jobService.store(parseInt(this.id))
          .pipe(first())
          .subscribe(
            response => {
              this.alertService.success(ms.jobs.apply_success);
              this.back();
            }, error => {
              this.loading = false;
              this.alertService.warning(error);
            });
      }.bind(this), this.ms.actions.CONFIRM_APPLY);
    }else
    {
      this.alertService.warning(this.ms.not_allow)
    }
    
  }
  ngOnDestroy() {
    this.titleService.setTitle(this.lastTitle);
  }
  ngOnInit() {
    this.getJob();
  }

}
