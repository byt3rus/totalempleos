import {Component,OnInit,Input} from '@angular/core';
import { Router } from '@angular/router';

import * as ms from '../../../../core/constants/messages';
import { DashboardService } from '../../../../core/services/dashboard.service';
import { AlertService } from '../../../../core/services/alert.service';
import { Job } from '../../../../core/models/job.model';
import { GeneralService } from '../../../../core/services/general.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-dashboard-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.scss']
})
export class JobComponent implements OnInit {
  @Input() job: Job;
  @Input() session = true;
  @Input() admin = true;
  ms;
  loading =false;
  constructor(
    private alertService:AlertService,
    private dashboardService:DashboardService,
    private generalService:GeneralService,
    private router: Router,
    
  ) {

      this.ms = ms;
     
   }

   showSalary(salary)
   {
     return this.ms.helpers.salaray(salary);
   }
   showLogo(job: Job)
   {
    if(job.confidencial || job.logo===null)
      return '/assets/images/company.png';
    return job.logo;
   }

   onActiveClick(id,active)
   {
      this.loading = true;
      this.generalService.active('jobs',id)
        .pipe(first())
        .subscribe(
          response => { 
            this.loading = false;
            let message = "";
            let typeMessage = "";
            this.job.active = !this.job.active;
            if(!this.job.active){
              message = this.ms.actions.SUCCESS_ACTIVE_LOAD;
              typeMessage =  "info";
            }else
            {
              message = this.ms.actions.SUCCESS_INACTIVE;
              typeMessage =  "success";
            }
            this.alertService.notify(message,typeMessage);
          },error => {
            this.loading = false;
            this.alertService.warning(error);
          });
   }
   onApplyClick(id:number)
   {
    if(this.session)
      this.router.navigate(['/apply/'+id]);
    else
    {
      this.alertService.redirect(this.ms.valid.postulate_guest)
    }
  }
  ngOnInit() {

   }

}
