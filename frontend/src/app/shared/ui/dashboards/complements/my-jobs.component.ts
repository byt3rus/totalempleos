import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {  ActivatedRoute } from '@angular/router';
import * as ms from '../../../../core/constants/messages';
import { JobService } from '../../../../core/services/job.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-dashboard-my-jobs',
  templateUrl: './my-jobs.component.html',
  styleUrls: ['./job.scss']
})
export class MyJobsComponent implements OnInit, OnDestroy {
  total:number;
  ms;
  id: string;
  lastTitle: string;
  loading = true;
  constructor(
    private jobService: JobService,
    private route: ActivatedRoute,
  ) {
    this.ms = ms;
    this.id = this.route.snapshot.paramMap.get('id');
  }

  /**
   * Obtiene la información de la vacante
   */
  getJob() {
    this.jobService.myJobs
    .pipe(first())
    .subscribe(
      response => {
        this.total = response.total;
        this.loading = false;
      }, error => {
        this.loading = false;
      });
  }

 
  ngOnDestroy() {
  }
  ngOnInit() {
    this.getJob();
  }

}
