import {Component,OnInit,Input} from '@angular/core';
import * as ms from '../../../../core/constants/messages';

@Component({
  selector: 'app-dashboard-job-not-found',
  templateUrl: './notfound.component.html',
  styleUrls: ['./job.scss']
})
export class NotFoundComponent implements OnInit {
  @Input() search: string;
  ms;
  constructor(
  ) {
      this.ms = ms;
   }
  ngOnInit() {

   }

}
