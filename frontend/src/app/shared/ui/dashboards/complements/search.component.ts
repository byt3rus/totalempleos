import {Component,OnInit,Input} from '@angular/core';
import { Router } from '@angular/router';

import * as ms from '../../../../core/constants/messages';
import { DashboardService } from '../../../../core/services/dashboard.service';
import { AlertService } from '../../../../core/services/alert.service';
import { OtherService } from '../../../../core/services/resources/others.service';
import { LocationService } from '../../../../core/services/resources/localtion.service';
import { City } from '../../../../core/models/location.model';
import { Job } from '../../../../core/models/job.model';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, FormControl, RequiredValidator } from '@angular/forms';
import { Pagination } from '../../../../core/models/pagination.model';
@Component({
  selector: 'app-dashboard-search',
  templateUrl: './search.component.html',
  styleUrls: ['./job.scss']
})
export class Searchomponent implements OnInit {
  @Input() payload;
  @Input() session;
  @Input() admin = false;
  ms;
  form :FormGroup;
  jobs:[Job];
  loading = false;
  loadJob = true;
  loadingText = false;
  //Pagination 
  pagination: Pagination;
  pages = [];
  pageActive = 1;
  hasRecords=false;
  messageRecords='';
  recordsFound ='';
  recordsNotFound=ms.actions.SEARCH
  totalRecords:any;

  initPages=1;
  limitPages=ms.actions.SEMENGT_PAGINATION;
  leftArrow = true;
  rightArrow=true;

  orderToggle=true;
  order = ms.actions.ORDER_DEFAULT;
  //Filters
  q:string;
  city:number;
  state:number;
  params= {};
  locations:City[];
  model:string;
  notFound= false;

  constructor(
    private alertService:AlertService,
    private dashboardService:DashboardService,
    private router: Router,
    private formBuilder: FormBuilder, 
    private locationService:LocationService,
    private job: OtherService,
  ) {
      this.ms = ms;
   }


   

  ngOnInit() {

    this.form = this.formBuilder.group({
      q: [''],
      location: [''],
      });
      this.form.reset();
      this.search();
   }

   onSubmit()
   {
     this.clean();
     this.search()
   }

   /**
    * Permite cambiar los params GET para realizar búsquedas
    */
   queryParams()
   {
     const form = this.form.getRawValue();
     this.params = {};
     for ( let key of Object.keys(form) ) {
        if(form[key]!=="" && form[key]!==null)
        {
          const _key = key;
          if(key==="location")
            key = this.model.toLowerCase();
          this.params[key] = form[_key];
          
        }else
         delete this.params[key];
     }
    return this.params;
   }

   onSearchKey(e)
   {
       this.q = e.term;
       this.load();
   }

   //Captura el estado o ciudad
   onSearchChange(e)
   {
     if(e){
      this.model= e.model;
     }
     this.locations = [];
   }

   load()
  {
    this.loadingText = true;
    this.locationService.list(this.q)
    .pipe(first())
    .subscribe(
      response => { 
        this.locations = response;
        this.loadingText = false;
      },error => {
        this.loadingText = false;
      });
  }

  //Pagination
  doPaginate()
  {
    this.pages = [];
    const limit = this.initPages+this.limitPages;
    this.rightArrow = (this.pagination.lastPage>limit);
    this.leftArrow=(this.initPages>1);
    for(let i=this.initPages;i<=this.pagination.lastPage;i++)
    {
       if(i<=limit)
           this.pages.push(i);
    }
  }

  nextSegmentPages()
  {
      this.initPages= this.initPages+this.limitPages+1;
      this.doPaginate();
  }

  backSegmentPages()
  {
      const init = this.limitPages+1;
      this.initPages= this.initPages-init;
      this.doPaginate();
  }


  showTototal()
  {
   if(this.jobs.length>0)
   {
       this.recordsNotFound=ms.actions.SEARCH;
       this.messageRecords = ms.actions.PAGINATION(this.pagination.page,this.pagination.perPage,this.pagination.total)
       this.recordsFound = ms.actions.FOUND_PAGINATE(this.pagination.total)
       this.hasRecords=true;
   }else{
       this.messageRecords = "";
       this.recordsFound="";
       this.hasRecords=false;
       this.recordsNotFound=ms.actions.NOT_FOUND;
   }
   
  }
  onRecodsChange(e)
   {
    this.totalRecords = e.currentTarget.value;
    this.search();
   }
   onNextPageClick(e, page)
   {
       this.pageActive = page;
       if(this.pagination.page!==page)
            this.search()
   }

   clean()
   {
    //this.search = "";
    this.initPages=1;
    this.limitPages=ms.actions.SEMENGT_PAGINATION;
    this.pageActive=1;

   }
  //Endpagination
  /**
   * Función realizar búsquedas
   */
   search()
   {
    this.loadJob = true;
    this.hasRecords=false;
    this.queryParams();
    window.scroll(0,0);
    this.job.search(
         this.pageActive,
         this.totalRecords,
         this.params,
         this.admin)
    .pipe(first())
    .subscribe(
      response => { 
        this.pagination = response;
        this.jobs = response.data;
        this.doPaginate();
        this.showTototal()
        this.notFound= response.data.length>0 ? false : true;
        this.loadJob = false;
      },error => {
        this.loadJob = false;
        this.alertService.warning(error);
      });
   }

}
