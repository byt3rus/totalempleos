import {Component,OnInit,Input} from '@angular/core';
import * as ms from '../../../../core/constants/messages';

@Component({
  selector: 'app-dashboard-job-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./job.scss']
})
export class SkeletonComponent implements OnInit {
  @Input() search: string;
  ms;
  skeleton = [0,1,2,3];
  constructor(
  ) {
      this.ms = ms;
   }
  ngOnInit() {

   }

}
