import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as ms from '../../../../core/constants/messages';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../../core/services/alert.service';
import { DashboardService } from '../../../../core/services/dashboard.service';
import { first } from 'rxjs/operators';
@Component({
    selector: 'app-complete-modal',
    templateUrl: './complete.modal.html',
})
export class CompleteComponent implements OnInit {

    @Input() skip;
    loading = false;
    ms: any;

    constructor(public modal: NgbActiveModal,
        private route: ActivatedRoute,
        private router: Router,
        private alertService: AlertService,
        private dashboardService: DashboardService,
    ) {
        this.ms = ms;
    }


    onSkipClick() {
        this.dashboardService.skip()
            .pipe(first())
            .subscribe(
                data => {
                    this.modal.close();
                }, error => {
                    this.loading = false;
                    this.alertService.warning(error);
                });
    }

    onProfileClick() {
        this.router.navigate(['/profile']);
        this.modal.close();
    }

    ngOnInit(): void {
    }

}