import { Component, OnDestroy, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

import * as ms from '../../../core/constants/messages';
import { DashboardService } from '../../../core/services/dashboard.service';
import { AlertService } from '../../../core/services/alert.service';
import { UserRegister } from '../../../core/models/user_register';
import { SendDataService } from '../../../core/services/send-data.service';
import { Subscription, Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompleteComponent } from './modals/complete.modal';
@Component({
  selector: 'app-dashboard-seeker',
  templateUrl: './seeker.component.html'
})
export class SeekerComponent implements OnDestroy {

  @ViewChild("completeProfile", { read: ElementRef }) completeProfile: ElementRef;
  // bread crumb items
  breadCrumbItems: Array<{}>;
  ms;
  dashboard;
  profileImage: string;
  user: any;
  show = false;
  authValid = [ms.seeker];
  subscription: Subscription;
  payload;
  permission: string;
  userRegister: UserRegister;
  confirmation = false;
  complete = false;
  skip = false;


  constructor(
    private sendDataService: SendDataService,
    private alertService: AlertService,
    private dashboardService: DashboardService,
    private router: Router,
    private modalService: NgbModal,
  ) {
    this.subscription = this.sendDataService.getPayload().subscribe(payload => {
      if (payload) {
        this.payload = payload;
        this.userRegister = payload.userRegister || null;
        if (this.userRegister) {
          this.confirmation = this.userRegister.confirmation;
          this.centerModal();
        }

        if (this.authValid.includes(payload.role))
          this.nextLoad();
      }
    });

  }


  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  /**
  * Open center modal
  * @param centerDataModal center modal data
  */
  centerModal() {
    if (!this.userRegister.complete && !this.userRegister.skip)
      this.modalService.open(CompleteComponent, { centered: true, windowClass: 'modal-holder' });
  }

  nextLoad() {
    this.ms = ms;
    this.dashboard = ms.seeker_component;
    this.breadCrumbItems = [{ label: ms.seeker_component.LABEL }];
    this.show = true;
  }
}
