import {Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import * as data from './menudata';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {
 @Input() permission: string;
 menuList: any;
 isOpen = false;
 currentItemMenu:string = "";

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.menuList = data.menu;
  }

  show(permissions){
      const items =  permissions.includes(this.permission);
      return items;
  }

  onMenuClick(event: any,itemMenu: string,route:any) {
    if(route!==false)
    {
      this.router.navigate([route]);
      return true;
    }
      
    event.preventDefault();
    const nextEl = event.target.nextSibling;
    if(this.currentItemMenu==="")
      this.currentItemMenu = itemMenu;
    if(this.currentItemMenu!==itemMenu)
    {
      this.isOpen = true;
      this.currentItemMenu = itemMenu;
    }else 
      this.isOpen = !this.isOpen;
    
    const toggleItem = this.isOpen ? 'add' : 'remove';
      nextEl.parentElement.classList[toggleItem]('show');
      if (nextEl.nextSibling) {
        if(nextEl.nextSibling.classList)
          nextEl.nextSibling.classList[toggleItem]('show');
        else 
          this.router.navigate(['/']);
        return true;
      }else
        return false;
  }
  goUrl(route: string) {
    this.router.navigate(['/'+route]);
  }

}
