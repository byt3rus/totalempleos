export const menu = [
    {
        permissions: ['god','admin'],
        items: [
            {
                title: "Inicio",
                route: false,
                icon: 'bx bx-home-alt',
                active: false,
                childs: []
            },
            
            {
                title: "Administrar",
                route: false,
                icon: 'bx bx-grid-alt',
                active: true,
                childs: [
                    {
                        title: " Área de especialidad",
                        route: '/categories',
                       },
                       {
                        title: "Dominio idiomas",
                        route: '/language_proficiencies',
                       },
                       {
                        title: "Idiomas",
                        route: '/resume_languages',
                       },
                       {
                        title: "Escolaridades",
                        route: '/scholarships',
                       },
                       {
                        title: "Tipos de contratación",
                        route: '/job_types',
                       },
                       {
                        title: "Espacios de trabajo",
                        route: '/work_spaces',
                       },
                           
                ]
            },

            {
                title: "Usuarios",
                route: false,
                icon: 'bx bx-user',
                active: true,
                childs: [
                    {
                        title: "Administradores",
                        route: '/admins',
                    },
                    {
                        title: "Empresa contratante",
                        route: '/employers',
                    },
                    {
                        title: "Candidatos",
                        route: '/job_seekers',
                    }
                ]
            },

            {
                title: "Empleos",
                route: false,
                icon: 'bx bxs-city',
                active: true,
                childs: [
                    {
                        title: "Vacantes",
                        route: '/jobs',
                    },
                    {
                        title: "Postulados",
                        route: '/job_postulations',
                    },
                   
                ]
            },

        ]
    },

    {
        permissions: ['employer'],
        items: [
            {
                title: "Inicio",
                route: '/dashboard',
                icon: 'bx bx-home-alt',
                active: false,
                childs: []
            },
            {
                title: "Empleos",
                route: false,
                icon: 'bx bxs-city',
                active: true,
                childs: [
                    {
                        title: "Vacantes",
                        route: '/jobs',
                    },
                    {
                        title: "Postulados",
                        route: '/job_postulations',
                    },
                   
                ]
            },

        ]
    },
    {
        permissions: ['seeker'],
        items: [
            {
                title: "Inicio",
                route: '/',
                icon: 'bx bx-home-alt',
                active: false,
                childs: []
            },
            {
                title: "Perfil",
                route: '/profile',
                icon: 'bx bxs-user-rectangle ',
                active: false,
                childs: []
            },
            {
                title: "Mis postulaciones",
                route: '/my_postulations',
                icon: 'bx bx-briefcase-alt ',
                active: false,
                childs: []
            },

        ]
    },
     
];