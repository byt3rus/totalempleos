import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgbCollapseModule, NgbDatepickerModule, NgbTimepickerModule,NgbTooltipModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideModule } from 'ng-click-outside';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { PagetitleComponent } from './pagetitle/pagetitle.component';
import { LoaderComponent } from './loader/loader.component';
import { MenuComponent } from './menus/menu.component';
import { AdminComponent } from './dashboards/admin.component';
import { Searchomponent } from './dashboards/complements/search.component';
import { Routes, RouterModule } from '@angular/router';
import { SeekerComponent } from './dashboards/seeker.component';

//Commons
import { InformationComponent } from './common/information.component';
import { JobComponent } from './dashboards/complements/job.component';
import { NotFoundComponent} from './dashboards/complements/notfound.component';
import { ApplyComponent} from './dashboards/complements/apply.component';
import { MyJobsComponent} from './dashboards/complements/my-jobs.component';
import { SkeletonComponent} from './dashboards/complements/skeleton.component';
import {CrudsModule} from './cruds/crud.module';
import {SearchableComponent} from './common/searchable.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import {WidgetModule} from '../widget/widget.module'

import { NgxLoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    PagetitleComponent, 
    LoaderComponent,
    MenuComponent,
    AdminComponent,
    SeekerComponent,
    Searchomponent,
    InformationComponent,
    SearchableComponent,
    JobComponent,
    NotFoundComponent,
    ApplyComponent,
    MyJobsComponent,
    SkeletonComponent,
  ],
  imports: [
    WidgetModule,
    CrudsModule,
    CommonModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    FormsModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    NgSelectModule,
    NgbAlertModule,
    NgbCollapseModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbTooltipModule,
    NgbDropdownModule,
    RouterModule,
  ],
  exports: [PagetitleComponent,
    LoaderComponent,MenuComponent,
    InformationComponent,
    SearchableComponent,
    AdminComponent,
    CrudsModule,
    Searchomponent,
    NgSelectModule,
    SeekerComponent]
})
export class UIModule { }
