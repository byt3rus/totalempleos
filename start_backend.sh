docker start totalempleosdb
cd backend
cp .env.testing .env
npm i &&  adonis migration:run
adonis seed --files='UserSeeder.js'
docker cp database/sql/resources.sql totalempleosdb:.
touch seed.sh
echo "mysql -utotalempleos -psoelpmelatot totalempleos < resources.sql" > seed.sh
docker cp seed.sh totalempleosdb:.
rm seed.sh
docker exec -it totalempleosdb sh seed.sh
docker exec -it totalempleosdb rm seed.sh
adonis serve --dev